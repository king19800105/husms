<?php

return [
    'common' => [
        'available' => 'available',
        'disable'   => 'disable',
        'errors'    => 'Sorry, something went wrong.',
        'success'   => 'Operate successfully',
        'fail'      => 'operation failure',
    ],
];