<?php
/**
 * Created by PhpStorm.
 * User: king
 * Date: 2018/2/19
 * Time: 下午8:47
 */
return [
    'no_permission'  => 'no permission',
    'authentication' => 'unauthenticated.',
    'authorization'  => 'unauthorised.',
    'not_find'       => 'not find',
    'validation'     => 'parameter validation failure',
    'not_allow'      => 'method not allow',
    'tpl_limit'      => 'template arrive limit',
];