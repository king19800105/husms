<?php
/**
 * Created by PhpStorm.
 * User: king
 * Date: 2018/2/19
 * Time: 下午8:47
 */
return [
    'no_permission'  => '没有权限访问异常',
    'authentication' => '未经身份验证的访问',
    'authorization'  => '未经授权的访问',
    'not_find'       => '没有找到',
    'validation'     => '请求参数验证失败',
    'not_allow'      => '不允许的请求方式',
    'tpl_limit'      => '个人模板到达上限，无法创建',
];