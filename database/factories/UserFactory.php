<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Models\User::class, function (Faker $faker) {
    return [
        'name'           => $faker->name,
        'email'          => $faker->unique()->safeEmail,
        'password'       => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret
        'mobile'         => $faker->numberBetween(13100000000, 13188888888),
        'remember_token' => str_random(10),
        'level'          => $faker->numberBetween(0, 3),
        'score'          => $faker->numberBetween(0, 1000),
        'source'         => $faker->randomElement(['mini', 'open', 'app']),
    ];
});
