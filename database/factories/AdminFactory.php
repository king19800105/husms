<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\Admin::class, function (Faker $faker) {
    return [
        'name'           => $faker->userName,
        'email'          => $faker->freeEmail,
        'mobile'         => $faker->numberBetween(13100000000, 13188888888),
        'password'       => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret,
        'producer'       => $faker->userName,
        'remark'         => $faker->sentence(10),
        'state'          => $faker->numberBetween(0, 1),
        'last_logged_ip' => $faker->ipv4,
    ];
});
