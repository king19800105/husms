/*
 Navicat Premium Data Transfer

 Source Server         : project-aliy
 Source Server Type    : MySQL
 Source Server Version : 80003
 Source Host           : 101.132.79.47:3306
 Source Schema         : husms

 Target Server Type    : MySQL
 Target Server Version : 80003
 File Encoding         : 65001

 Date: 16/08/2018 22:29:42
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for hu_admins
-- ----------------------------
DROP TABLE IF EXISTS `hu_admins`;
CREATE TABLE `hu_admins` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) NOT NULL DEFAULT '' COMMENT '用户名',
  `email` varchar(191) NOT NULL DEFAULT '' COMMENT '邮箱',
  `mobile` varchar(191) NOT NULL DEFAULT '' COMMENT '手机号码',
  `password` varchar(191) NOT NULL DEFAULT '' COMMENT '管理员密码',
  `producer` varchar(191) NOT NULL DEFAULT '' COMMENT '创建人',
  `remark` varchar(191) NOT NULL DEFAULT '' COMMENT '备注信息',
  `state` tinyint(4) NOT NULL DEFAULT '1' COMMENT '用户状态，1：可用，2：冻结',
  `last_logged_ip` char(15) NOT NULL DEFAULT '',
  `remember_token` varchar(100) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `admins_name_unique` (`name`),
  UNIQUE KEY `admins_email_unique` (`email`),
  UNIQUE KEY `admins_mobile_unique` (`mobile`)
) ENGINE=InnoDB AUTO_INCREMENT=102 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of hu_admins
-- ----------------------------
BEGIN;
INSERT INTO `hu_admins` VALUES (1, 'tavares98', 'khalid.willms@yahoo.com', '13120983541', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'astoltenberg', 'Recusandae dolorem ducimus cupiditate culpa est non nisi earum quae aut adipisci id officiis.', 1, '186.111.30.113', NULL, NULL, '2018-05-13 01:24:55', '2018-05-13 01:24:55');
INSERT INTO `hu_admins` VALUES (2, 'elnora42', 'dickinson.jaqueline@hotmail.com', '13145879313', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'jerrold.predovic', 'Velit eum tempore voluptatem rem id vitae asperiores.', 1, '57.221.254.23', NULL, NULL, '2018-05-13 01:24:55', '2018-05-13 01:24:55');
INSERT INTO `hu_admins` VALUES (3, 'sonya35', 'louvenia.ruecker@yahoo.com', '13175838236', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'dcollins', 'Distinctio ullam dicta aut et earum facilis et numquam sed magnam atque amet.', 0, '219.44.137.67', NULL, NULL, '2018-05-13 01:24:55', '2018-05-13 01:24:55');
INSERT INTO `hu_admins` VALUES (4, 'kaci18', 'aglae68@yahoo.com', '13141505101', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'jensen.ratke', 'Libero in odit voluptatem temporibus ea exercitationem quaerat atque tempora labore.', 0, '21.109.37.146', NULL, NULL, '2018-05-13 01:24:55', '2018-05-13 01:24:55');
INSERT INTO `hu_admins` VALUES (5, 'king19800105', 'king19800105@163.com', '13564535304', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'casper.schuster', 'Tenetur rerum nesciunt nostrum minima qui explicabo soluta.', 1, '175.8.253.174', NULL, NULL, '2018-05-13 01:24:55', '2018-08-12 16:34:28');
INSERT INTO `hu_admins` VALUES (6, 'lkrajcik', 'lebsack.timmothy@yahoo.com', '13182132679', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'maurine09', 'Qui quos sed iste ipsum qui aut qui est sint aperiam alias.', 0, '196.223.113.114', NULL, NULL, '2018-05-13 01:24:55', '2018-05-13 01:24:55');
INSERT INTO `hu_admins` VALUES (7, 'wilfrid55', 'bgleichner@gmail.com', '13103682692', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'mcglynn.esther', 'Eaque laudantium at blanditiis pariatur tempora atque sapiente culpa nesciunt voluptatem.', 0, '254.227.215.38', NULL, NULL, '2018-05-13 01:24:55', '2018-05-13 01:24:55');
INSERT INTO `hu_admins` VALUES (8, 'michelle40', 'nikolaus.pete@gmail.com', '13141915067', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'conor87', 'Quia non et eum ex in nihil deleniti saepe.', 1, '89.217.26.26', NULL, NULL, '2018-05-13 01:24:55', '2018-05-13 01:24:55');
INSERT INTO `hu_admins` VALUES (9, 'shawn65', 'mrohan@yahoo.com', '13110560384', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'lazaro22', 'Deserunt iure debitis amet assumenda dolor commodi adipisci inventore.', 0, '84.136.191.40', NULL, NULL, '2018-05-13 01:24:55', '2018-05-13 01:24:55');
INSERT INTO `hu_admins` VALUES (10, 'corkery.cecil', 'minnie94@hotmail.com', '13142025822', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'cchamplin', 'Beatae illo modi quaerat vitae cum atque.', 1, '130.98.132.159', NULL, NULL, '2018-05-13 01:24:55', '2018-05-13 01:24:55');
INSERT INTO `hu_admins` VALUES (11, 'kade97', 'may.cummerata@hotmail.com', '13135527643', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'botsford.elissa', 'Quisquam et quasi commodi illo non eos et.', 1, '213.124.110.97', NULL, NULL, '2018-05-13 01:24:55', '2018-05-13 01:24:55');
INSERT INTO `hu_admins` VALUES (12, 'jesse00', 'ethan.corkery@hotmail.com', '13186772099', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'maynard85', 'Animi molestiae culpa enim adipisci optio neque quae doloribus sit enim.', 1, '30.69.133.89', NULL, NULL, '2018-05-13 01:24:55', '2018-05-13 01:24:55');
INSERT INTO `hu_admins` VALUES (13, 'waldo.grady', 'katlyn64@yahoo.com', '13123181847', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'aiyana.raynor', 'Itaque molestias fugit recusandae aut nesciunt accusantium ut fugiat.', 1, '143.14.160.235', NULL, NULL, '2018-05-13 01:24:55', '2018-05-13 01:24:55');
INSERT INTO `hu_admins` VALUES (14, 'schuster.kassandra', 'eden95@yahoo.com', '13132804573', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'leda.stokes', 'Et saepe facere qui nisi autem labore deleniti porro voluptas.', 1, '232.219.179.116', NULL, NULL, '2018-05-13 01:24:55', '2018-05-13 01:24:55');
INSERT INTO `hu_admins` VALUES (15, 'rippin.linda', 'rosamond22@gmail.com', '13122255295', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'barrows.aubrey', 'Est maxime sit dolorem optio assumenda sint laborum suscipit eum voluptates quia commodi.', 0, '73.211.194.232', NULL, NULL, '2018-05-13 01:24:55', '2018-05-13 01:24:55');
INSERT INTO `hu_admins` VALUES (16, 'trevor.veum', 'alexis.dicki@yahoo.com', '13117220625', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'austyn03', 'Eos excepturi eum eum saepe tempora aut a.', 1, '99.196.241.102', NULL, NULL, '2018-05-13 01:24:55', '2018-05-13 01:24:55');
INSERT INTO `hu_admins` VALUES (17, 'alangworth', 'kling.deonte@yahoo.com', '13125460252', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'montana62', 'Quia itaque voluptas aut velit totam quia quae.', 1, '139.131.211.193', NULL, NULL, '2018-05-13 01:24:55', '2018-05-13 01:24:55');
INSERT INTO `hu_admins` VALUES (18, 'aswaniawski', 'zelda.williamson@gmail.com', '13183485443', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'windler.abby', 'Ea alias voluptates voluptas aut quaerat ratione reprehenderit explicabo porro consequatur in velit.', 1, '71.178.59.80', NULL, NULL, '2018-05-13 01:24:55', '2018-05-13 01:24:55');
INSERT INTO `hu_admins` VALUES (19, 'gabernathy', 'alverta.schaden@gmail.com', '13147386143', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'ratke.jevon', 'Labore in ex id quaerat est blanditiis reprehenderit asperiores ut non.', 1, '204.182.181.90', NULL, NULL, '2018-05-13 01:24:55', '2018-05-13 01:24:55');
INSERT INTO `hu_admins` VALUES (20, 'beatrice.beahan', 'hanna.walker@gmail.com', '13102014518', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'frosenbaum', 'Architecto sequi id quod provident et dignissimos dolores ratione eligendi odit reprehenderit.', 1, '90.249.43.114', NULL, NULL, '2018-05-13 01:24:55', '2018-05-13 01:24:55');
INSERT INTO `hu_admins` VALUES (21, 'eldred.barrows', 'jarrell94@yahoo.com', '13184724461', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'gaylord.myrtice', 'Ut molestiae ab consequuntur sint qui quasi aperiam voluptatum recusandae.', 0, '106.61.33.215', NULL, NULL, '2018-05-13 01:24:55', '2018-05-13 01:24:55');
INSERT INTO `hu_admins` VALUES (22, 'zbaumbach', 'wilderman.conrad@hotmail.com', '13149520923', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'kohler.alyce', 'Sit sed magnam impedit nostrum laboriosam placeat et molestiae ut commodi.', 0, '67.9.52.94', NULL, NULL, '2018-05-13 01:24:55', '2018-05-13 01:24:55');
INSERT INTO `hu_admins` VALUES (23, 'raphaelle.anderson', 'sjerde@gmail.com', '13109043652', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'reynolds.katlynn', 'Et sit et architecto magnam veritatis tempora vel quam saepe.', 0, '149.108.11.165', NULL, NULL, '2018-05-13 01:24:55', '2018-05-13 01:24:55');
INSERT INTO `hu_admins` VALUES (24, 'barton.ava', 'moore.vesta@gmail.com', '13133527316', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'evangeline83', 'Temporibus sequi fugit perspiciatis saepe quisquam dolore consequuntur voluptatem eveniet in veniam quia.', 1, '129.169.250.107', NULL, NULL, '2018-05-13 01:24:55', '2018-05-13 01:24:55');
INSERT INTO `hu_admins` VALUES (25, 'greenfelder.cielo', 'brant31@yahoo.com', '13176985962', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'zbailey', 'Et aperiam sit earum ex ipsa veniam sed tenetur.', 0, '66.81.167.107', NULL, NULL, '2018-05-13 01:24:55', '2018-05-13 01:24:55');
INSERT INTO `hu_admins` VALUES (26, 'harber.fredrick', 'zemlak.gage@gmail.com', '13153830920', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'edward20', 'Quo nihil delectus doloremque eum sed rem voluptatum.', 0, '9.59.102.37', NULL, NULL, '2018-05-13 01:24:55', '2018-05-13 01:24:55');
INSERT INTO `hu_admins` VALUES (27, 'lockman.mario', 'pollich.elaina@yahoo.com', '13136966968', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'leopoldo.leffler', 'Eveniet molestiae quos non ex ut occaecati perspiciatis omnis a ab.', 0, '72.104.40.235', NULL, NULL, '2018-05-13 01:24:55', '2018-05-13 01:24:55');
INSERT INTO `hu_admins` VALUES (28, 'gutmann.rafaela', 'fboehm@yahoo.com', '13154352779', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'champlin.gianni', 'Et voluptas ut dicta similique aliquam eos dolor aliquam voluptas explicabo hic.', 1, '206.17.46.233', NULL, NULL, '2018-05-13 01:24:55', '2018-05-13 01:24:55');
INSERT INTO `hu_admins` VALUES (29, 'isai.weimann', 'joel57@gmail.com', '13152745941', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'lesley80', 'Earum non sit nostrum eaque ducimus molestiae et debitis facere eum molestiae rerum expedita.', 1, '242.42.84.104', NULL, NULL, '2018-05-13 01:24:55', '2018-05-13 01:24:55');
INSERT INTO `hu_admins` VALUES (30, 'lakin.andreanne', 'rgislason@yahoo.com', '13121637255', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'brooke39', 'Voluptas officia et doloremque recusandae voluptate iusto ut sapiente aut natus vel voluptatem officiis.', 1, '160.170.229.82', NULL, NULL, '2018-05-13 01:24:55', '2018-05-13 01:24:55');
INSERT INTO `hu_admins` VALUES (31, 'madelynn83', 'dortha.flatley@gmail.com', '13151607868', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'keebler.else', 'Nisi et harum vitae sint et occaecati harum aut vel aut dolores reprehenderit.', 1, '186.195.30.49', NULL, NULL, '2018-05-13 01:24:55', '2018-05-13 01:24:55');
INSERT INTO `hu_admins` VALUES (32, 'aniya32', 'dariana.torphy@hotmail.com', '13140144391', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'raymond.lebsack', 'Consequuntur dolor necessitatibus illo laborum ullam sit cumque rerum dolores.', 1, '147.165.81.13', NULL, NULL, '2018-05-13 01:24:55', '2018-05-13 01:24:55');
INSERT INTO `hu_admins` VALUES (33, 'norma13', 'mann.darrick@gmail.com', '13182819353', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'joshuah51', 'Est molestias veritatis quia aliquid minus dolorem eligendi dolor tempore.', 0, '247.244.231.158', NULL, NULL, '2018-05-13 01:24:55', '2018-05-13 01:24:55');
INSERT INTO `hu_admins` VALUES (34, 'qnitzsche', 'jakubowski.jacky@hotmail.com', '13141370143', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'sidney.runolfsson', 'Voluptatem nulla ut et cupiditate dolor omnis quas odio.', 1, '184.43.17.156', NULL, NULL, '2018-05-13 01:24:55', '2018-05-13 01:24:55');
INSERT INTO `hu_admins` VALUES (35, 'sjaskolski', 'eldon11@hotmail.com', '13185540116', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'victoria.mraz', 'Nulla expedita quo vel qui aliquid ea cum blanditiis.', 1, '182.95.38.61', NULL, NULL, '2018-05-13 01:24:55', '2018-05-13 01:24:55');
INSERT INTO `hu_admins` VALUES (36, 'thea65', 'ruth.brekke@gmail.com', '13186170971', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'tessie.walker', 'Ipsa ea rerum architecto aliquid ut asperiores et soluta quia.', 1, '146.154.176.210', NULL, NULL, '2018-05-13 01:24:55', '2018-05-13 01:24:55');
INSERT INTO `hu_admins` VALUES (37, 'gilda.marks', 'purdy.llewellyn@gmail.com', '13180011974', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'bergnaum.shad', 'Sed eos quisquam nihil et quae minus porro quia.', 0, '97.172.229.50', NULL, NULL, '2018-05-13 01:24:55', '2018-05-13 01:24:55');
INSERT INTO `hu_admins` VALUES (38, 'clifton.ohara', 'bergstrom.hilario@gmail.com', '13148729533', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'jacobson.makenna', 'Voluptas officiis natus placeat modi odio repudiandae tenetur qui aperiam voluptatem.', 1, '145.111.127.29', NULL, NULL, '2018-05-13 01:24:55', '2018-05-13 01:24:55');
INSERT INTO `hu_admins` VALUES (39, 'kris.hickle', 'klabadie@yahoo.com', '13163351669', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'mckenzie.manley', 'Ipsam saepe natus ipsa magni et id omnis aut quam incidunt.', 0, '166.91.187.70', NULL, NULL, '2018-05-13 01:24:55', '2018-05-13 01:24:55');
INSERT INTO `hu_admins` VALUES (40, 'halvorson.ansel', 'fabiola79@hotmail.com', '13118063153', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'dejah.gleason', 'Ab sed et est dolorem magnam omnis tenetur alias voluptatem hic magni enim.', 0, '255.184.99.219', NULL, NULL, '2018-05-13 01:24:55', '2018-05-13 01:24:55');
INSERT INTO `hu_admins` VALUES (41, 'adriana.dibbert', 'gavin46@gmail.com', '13170068661', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'xsanford', 'Quia ex et qui et ex autem cupiditate minima occaecati ut reiciendis.', 0, '96.40.245.194', NULL, NULL, '2018-05-13 01:24:55', '2018-05-13 01:24:55');
INSERT INTO `hu_admins` VALUES (42, 'ena19', 'satterfield.deion@hotmail.com', '13143585528', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'nbogan', 'Provident quis natus enim odio repellendus officiis officia sint enim deserunt quo.', 1, '8.39.124.156', NULL, NULL, '2018-05-13 01:24:55', '2018-05-13 01:24:55');
INSERT INTO `hu_admins` VALUES (43, 'equitzon', 'daugherty.alisha@hotmail.com', '13151489089', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'ametz', 'Quam et qui voluptatem quia perferendis ea.', 0, '163.39.174.154', NULL, NULL, '2018-05-13 01:24:55', '2018-05-13 01:24:55');
INSERT INTO `hu_admins` VALUES (44, 'aharber', 'yrunte@yahoo.com', '13181962067', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'lawson.heaney', 'Soluta ullam velit ad qui aut quia unde ipsam rerum blanditiis est et eveniet.', 0, '67.140.29.51', NULL, NULL, '2018-05-13 01:24:55', '2018-05-13 01:24:55');
INSERT INTO `hu_admins` VALUES (45, 'olockman', 'chase.bayer@gmail.com', '13168740308', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'noemie80', 'Delectus animi velit qui quae voluptatibus mollitia.', 0, '59.34.20.150', NULL, NULL, '2018-05-13 01:24:55', '2018-05-13 01:24:55');
INSERT INTO `hu_admins` VALUES (46, 'jaqueline96', 'khuels@hotmail.com', '13130951835', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'tstracke', 'Aut laboriosam distinctio autem sunt magnam recusandae minima excepturi molestiae saepe ab molestiae repellendus.', 0, '222.114.144.234', NULL, NULL, '2018-05-13 01:24:55', '2018-05-13 01:24:55');
INSERT INTO `hu_admins` VALUES (47, 'ppowlowski', 'gilda.leannon@gmail.com', '13150388869', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'melba60', 'Id esse quis eos et aliquid ratione tempore dolores voluptatem delectus odio nulla.', 1, '144.225.20.94', NULL, NULL, '2018-05-13 01:24:55', '2018-05-13 01:24:55');
INSERT INTO `hu_admins` VALUES (48, 'kaia.ankunding', 'stevie.wisozk@gmail.com', '13129774144', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'kenneth17', 'Hic quia et quod sunt aut sint beatae asperiores quia perspiciatis eaque consequatur ipsam.', 0, '215.85.238.70', NULL, NULL, '2018-05-13 01:24:55', '2018-05-13 01:24:55');
INSERT INTO `hu_admins` VALUES (49, 'lora.jaskolski', 'sigrid.hermiston@gmail.com', '13152340654', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'barrows.dianna', 'Omnis corrupti voluptas odit vel velit aperiam quia exercitationem praesentium et.', 1, '215.88.170.200', NULL, NULL, '2018-05-13 01:24:55', '2018-05-13 01:24:55');
INSERT INTO `hu_admins` VALUES (50, 'darius53', 'ekohler@gmail.com', '13146976885', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'josue.grant', 'Magni et aut laudantium ab quibusdam qui sunt odit ducimus cupiditate.', 1, '194.121.85.227', NULL, NULL, '2018-05-13 01:24:55', '2018-05-13 01:24:55');
INSERT INTO `hu_admins` VALUES (51, 'nannie.bernhard', 'americo.rice@yahoo.com', '13116808944', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'sauer.saige', 'Perspiciatis et dolor cum perspiciatis aspernatur dolor voluptate sunt quaerat aut.', 1, '214.195.76.40', NULL, NULL, '2018-05-13 01:24:55', '2018-05-13 01:24:55');
INSERT INTO `hu_admins` VALUES (52, 'alda.murphy', 'courtney45@yahoo.com', '13117683585', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'darren.adams', 'Quos quibusdam illum labore nobis distinctio qui voluptatem praesentium nemo minima ab earum distinctio.', 0, '188.30.69.105', NULL, NULL, '2018-05-13 01:24:56', '2018-05-13 01:24:56');
INSERT INTO `hu_admins` VALUES (53, 'lesch.mac', 'mhickle@hotmail.com', '13150875198', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'electa84', 'Soluta laboriosam omnis amet voluptatem libero maiores totam eligendi in pariatur cumque incidunt.', 1, '211.186.233.8', NULL, NULL, '2018-05-13 01:24:56', '2018-05-13 01:24:56');
INSERT INTO `hu_admins` VALUES (54, 'oberbrunner.devyn', 'holly.brekke@yahoo.com', '13173970494', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'leatha66', 'Maiores nulla quibusdam explicabo qui quod quis tenetur sed aut.', 1, '149.159.51.154', NULL, NULL, '2018-05-13 01:24:56', '2018-05-13 01:24:56');
INSERT INTO `hu_admins` VALUES (55, 'crona.lesley', 'mckenzie29@hotmail.com', '13167302787', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'lucienne27', 'Numquam quia ipsa voluptatibus eligendi et dignissimos similique.', 0, '238.49.159.206', NULL, NULL, '2018-05-13 01:24:56', '2018-05-13 01:24:56');
INSERT INTO `hu_admins` VALUES (56, 'dferry', 'hailie56@yahoo.com', '13124380407', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'jmills', 'Quis rerum reiciendis voluptate ipsa maiores perferendis consectetur deleniti quasi ut incidunt nobis.', 1, '67.188.254.21', NULL, NULL, '2018-05-13 01:24:56', '2018-05-13 01:24:56');
INSERT INTO `hu_admins` VALUES (57, 'bartoletti.guiseppe', 'ylebsack@yahoo.com', '13171038933', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'loy76', 'Libero perferendis qui repudiandae minima et reprehenderit possimus.', 1, '90.31.166.176', NULL, NULL, '2018-05-13 01:24:56', '2018-05-13 01:24:56');
INSERT INTO `hu_admins` VALUES (58, 'braun.hilma', 'koelpin.efrain@hotmail.com', '13132041810', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'akeem57', 'Qui omnis excepturi est quo quaerat in et quod.', 0, '52.227.90.2', NULL, NULL, '2018-05-13 01:24:56', '2018-05-13 01:24:56');
INSERT INTO `hu_admins` VALUES (59, 'randall.rowe', 'wilderman.berneice@gmail.com', '13185461410', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'xolson', 'Repudiandae libero aliquid nam explicabo dolores dolor quisquam modi.', 0, '206.131.182.205', NULL, NULL, '2018-05-13 01:24:56', '2018-05-13 01:24:56');
INSERT INTO `hu_admins` VALUES (60, 'kaleigh.zulauf', 'stark.adriana@yahoo.com', '13172611433', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'gturner', 'Sit distinctio explicabo voluptatem consequatur qui maiores distinctio fuga perferendis et eveniet illo.', 1, '189.141.136.87', NULL, NULL, '2018-05-13 01:24:56', '2018-05-13 01:24:56');
INSERT INTO `hu_admins` VALUES (61, 'ibruen', 'leonora.mcdermott@gmail.com', '13173654610', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'brice36', 'Eaque quis qui ipsum veniam velit autem.', 0, '168.95.65.151', NULL, NULL, '2018-05-13 01:24:56', '2018-05-13 01:24:56');
INSERT INTO `hu_admins` VALUES (62, 'dylan61', 'koss.nikita@hotmail.com', '13187684081', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'orn.giovanni', 'Enim voluptatem accusantium quasi explicabo ducimus qui laborum nihil.', 1, '132.43.80.225', NULL, NULL, '2018-05-13 01:24:56', '2018-05-13 01:24:56');
INSERT INTO `hu_admins` VALUES (63, 'chasity.bernhard', 'angel45@hotmail.com', '13125655604', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'gina18', 'Voluptate voluptatum et quia modi voluptatum earum alias omnis consequatur fugit.', 0, '255.15.32.158', NULL, NULL, '2018-05-13 01:24:56', '2018-05-13 01:24:56');
INSERT INTO `hu_admins` VALUES (64, 'sage.jast', 'ullrich.jean@hotmail.com', '13178459869', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'dayna07', 'Accusantium rerum aut est consequuntur nihil vero vel animi consequatur.', 0, '95.85.93.38', NULL, NULL, '2018-05-13 01:24:56', '2018-05-13 01:24:56');
INSERT INTO `hu_admins` VALUES (65, 'lera27', 'odicki@gmail.com', '13122506937', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'preston.herzog', 'Expedita eos voluptatum tempora eaque recusandae quia illo.', 0, '200.4.196.118', NULL, NULL, '2018-05-13 01:24:56', '2018-05-13 01:24:56');
INSERT INTO `hu_admins` VALUES (66, 'romaine.bode', 'gay.schulist@yahoo.com', '13139261669', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'ezequiel.beatty', 'Fugiat alias dolorem omnis sint in dolorem consequatur ut.', 0, '195.107.51.248', NULL, NULL, '2018-05-13 01:24:56', '2018-05-13 01:24:56');
INSERT INTO `hu_admins` VALUES (67, 'schroeder.wilton', 'alison16@yahoo.com', '13185458281', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'maudie.russel', 'Quis itaque harum est excepturi est quia tempore.', 0, '205.137.63.147', NULL, NULL, '2018-05-13 01:24:56', '2018-05-13 01:24:56');
INSERT INTO `hu_admins` VALUES (68, 'vkoepp', 'fritsch.fae@gmail.com', '13132353701', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'elmo05', 'Et in similique sunt nemo nisi architecto quo optio in neque tempora expedita et.', 1, '95.87.204.119', NULL, NULL, '2018-05-13 01:24:56', '2018-05-13 01:24:56');
INSERT INTO `hu_admins` VALUES (69, 'hirthe.jade', 'pietro.beahan@hotmail.com', '13111132299', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'karlie13', 'Tenetur quia sapiente maiores sunt debitis ratione quis nam repudiandae.', 1, '30.167.113.200', NULL, NULL, '2018-05-13 01:24:56', '2018-05-13 01:24:56');
INSERT INTO `hu_admins` VALUES (70, 'gianni52', 'melba16@gmail.com', '13135959613', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'drunte', 'Porro illum ipsa ipsum adipisci odio perspiciatis sit exercitationem non alias exercitationem dolorem laboriosam.', 0, '249.249.226.206', NULL, NULL, '2018-05-13 01:24:56', '2018-05-13 01:24:56');
INSERT INTO `hu_admins` VALUES (71, 'blaze.schiller', 'dokon@yahoo.com', '13146395248', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'mabel.olson', 'Qui ea voluptas cum delectus aut hic odit voluptatem non optio.', 1, '27.58.208.0', NULL, NULL, '2018-05-13 01:24:56', '2018-05-13 01:24:56');
INSERT INTO `hu_admins` VALUES (72, 'auer.gerald', 'amelia.conn@yahoo.com', '13172395959', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'blaise51', 'Doloribus sint placeat quia ducimus optio deleniti maiores soluta consequatur voluptatem maxime.', 1, '2.5.242.80', NULL, NULL, '2018-05-13 01:24:56', '2018-05-13 01:24:56');
INSERT INTO `hu_admins` VALUES (73, 'cwisoky', 'alfonso.schinner@yahoo.com', '13149777819', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'kristopher.gibson', 'Qui eligendi fuga aperiam sequi qui unde qui necessitatibus porro totam assumenda.', 0, '243.98.252.154', NULL, NULL, '2018-05-13 01:24:56', '2018-05-13 01:24:56');
INSERT INTO `hu_admins` VALUES (74, 'payton.rohan', 'rohan.amanda@hotmail.com', '13186034429', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'vswaniawski', 'Quaerat doloremque pariatur molestiae occaecati et quas quasi nisi debitis quia.', 0, '198.146.11.24', NULL, NULL, '2018-05-13 01:24:56', '2018-05-13 01:24:56');
INSERT INTO `hu_admins` VALUES (75, 'clinton08', 'leanna.ankunding@gmail.com', '13111363366', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'marvin.elvera', 'Sunt tenetur sit dolores exercitationem consequuntur culpa nobis a.', 1, '50.141.62.88', NULL, NULL, '2018-05-13 01:24:56', '2018-05-13 01:24:56');
INSERT INTO `hu_admins` VALUES (76, 'stracke.lesly', 'maritza42@hotmail.com', '13114687416', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'beer.aniyah', 'Delectus hic non officia et est vitae.', 0, '37.148.19.221', NULL, NULL, '2018-05-13 01:24:56', '2018-05-13 01:24:56');
INSERT INTO `hu_admins` VALUES (77, 'xgoldner', 'bdach@yahoo.com', '13124649895', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'schuster.leon', 'Molestiae quod voluptatum nesciunt odit labore voluptatum adipisci dicta.', 0, '54.115.218.32', NULL, NULL, '2018-05-13 01:24:56', '2018-05-13 01:24:56');
INSERT INTO `hu_admins` VALUES (78, 'cristina65', 'vmcglynn@hotmail.com', '13106576955', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'lambert.jones', 'Voluptate magni veritatis et voluptates corporis omnis.', 1, '7.143.107.82', NULL, NULL, '2018-05-13 01:24:56', '2018-05-13 01:24:56');
INSERT INTO `hu_admins` VALUES (79, 'jacinto37', 'angela06@gmail.com', '13121455320', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'rosenbaum.jairo', 'Non vel unde esse ea doloribus repellendus.', 1, '3.15.145.38', NULL, NULL, '2018-05-13 01:24:56', '2018-05-13 01:24:56');
INSERT INTO `hu_admins` VALUES (80, 'nnienow', 'braun.fatima@gmail.com', '13101191642', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'kilback.brain', 'Ut et voluptas qui esse et sed ut quibusdam molestiae.', 0, '87.120.10.86', NULL, NULL, '2018-05-13 01:24:56', '2018-05-13 01:24:56');
INSERT INTO `hu_admins` VALUES (81, 'sporer.nolan', 'kariane.bechtelar@gmail.com', '13168908957', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'omer.pagac', 'Ex quidem voluptatem voluptatem dolores omnis totam quae in aperiam ipsum ut ut.', 0, '154.85.121.44', NULL, NULL, '2018-05-13 01:24:56', '2018-05-13 01:24:56');
INSERT INTO `hu_admins` VALUES (82, 'frida56', 'koss.oswald@hotmail.com', '13148886360', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'kiana.abernathy', 'Ut assumenda et voluptatem voluptatem est consectetur corrupti sint omnis quia explicabo illum.', 1, '78.219.90.199', NULL, NULL, '2018-05-13 01:24:56', '2018-05-13 01:24:56');
INSERT INTO `hu_admins` VALUES (83, 'caleb.robel', 'cecelia51@yahoo.com', '13133661756', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'miller.gerlach', 'Eius et amet molestias dicta quo alias reiciendis id.', 0, '142.218.190.153', NULL, NULL, '2018-05-13 01:24:56', '2018-05-13 01:24:56');
INSERT INTO `hu_admins` VALUES (84, 'vhintz', 'lkub@hotmail.com', '13100508914', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'bogan.helena', 'Eos iure enim dolorum hic ad eum fugit.', 1, '23.189.116.121', NULL, NULL, '2018-05-13 01:24:56', '2018-05-13 01:24:56');
INSERT INTO `hu_admins` VALUES (85, 'zakary53', 'kulas.casandra@gmail.com', '13150349485', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'frenner', 'Et voluptatum et voluptate quidem aliquid deserunt voluptas dolore minima et aut.', 1, '155.184.37.232', NULL, NULL, '2018-05-13 01:24:56', '2018-05-13 01:24:56');
INSERT INTO `hu_admins` VALUES (86, 'ibarrows', 'francis90@gmail.com', '13129984389', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'everette.harber', 'Quia velit reprehenderit eos et non sunt asperiores itaque voluptatibus voluptas maiores.', 1, '213.41.54.100', NULL, NULL, '2018-05-13 01:24:56', '2018-05-13 01:24:56');
INSERT INTO `hu_admins` VALUES (87, 'parisian.kadin', 'gerlach.sammie@hotmail.com', '13153373102', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'edooley', 'Ab sint sunt alias exercitationem earum cum quos modi.', 0, '196.120.187.95', NULL, NULL, '2018-05-13 01:24:56', '2018-05-13 01:24:56');
INSERT INTO `hu_admins` VALUES (88, 'mmueller', 'brakus.lera@hotmail.com', '13140637044', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'ulangworth', 'Ipsum nihil ipsa beatae minus at laudantium consequatur est eum officia.', 1, '74.24.67.231', NULL, NULL, '2018-05-13 01:24:56', '2018-05-13 01:24:56');
INSERT INTO `hu_admins` VALUES (89, 'braun.lazaro', 'botsford.viviane@gmail.com', '13142893783', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'ivah.ferry', 'Ut magni ipsam consequuntur repellat quod quo velit ut voluptates perspiciatis omnis.', 0, '108.47.2.197', NULL, NULL, '2018-05-13 01:24:56', '2018-05-13 01:24:56');
INSERT INTO `hu_admins` VALUES (90, 'serena.maggio', 'vwehner@yahoo.com', '13136681719', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'williamson.dawn', 'Aspernatur fugit beatae nihil dolor quia praesentium qui.', 0, '68.75.211.60', NULL, NULL, '2018-05-13 01:24:56', '2018-05-13 01:24:56');
INSERT INTO `hu_admins` VALUES (91, 'elta.anderson', 'jeffrey.mosciski@gmail.com', '13111986125', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'asmith', 'Odio atque asperiores sunt veniam dicta ipsam.', 0, '169.201.66.38', NULL, NULL, '2018-05-13 01:24:56', '2018-05-13 01:24:56');
INSERT INTO `hu_admins` VALUES (92, 'wgorczany', 'marie01@hotmail.com', '13161365939', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'omorissette', 'Nihil eius ab quisquam aut fugit id consequatur amet quidem consectetur consectetur unde odio.', 0, '182.94.171.188', NULL, NULL, '2018-05-13 01:24:56', '2018-05-13 01:24:56');
INSERT INTO `hu_admins` VALUES (93, 'destini92', 'joana.hickle@hotmail.com', '13141560955', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'giuseppe.barrows', 'Suscipit cupiditate nihil alias quis dolorem itaque dolores aspernatur asperiores.', 0, '87.162.67.102', NULL, NULL, '2018-05-13 01:24:56', '2018-05-13 01:24:56');
INSERT INTO `hu_admins` VALUES (94, 'frederik75', 'herbert00@gmail.com', '13170843035', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'elinor81', 'Sit quidem quis dolor et dolore quia hic omnis.', 0, '124.30.102.65', NULL, NULL, '2018-05-13 01:24:56', '2018-05-13 01:24:56');
INSERT INTO `hu_admins` VALUES (95, 'lbailey', 'daija.herzog@hotmail.com', '13139019103', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'gust.mann', 'Itaque nulla eius incidunt nisi esse omnis quas laborum explicabo reiciendis voluptatem.', 1, '193.3.136.2', NULL, NULL, '2018-05-13 01:24:56', '2018-05-13 01:24:56');
INSERT INTO `hu_admins` VALUES (96, 'ahmad.swaniawski', 'thelma.wiegand@hotmail.com', '13104309879', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'joyce79', 'Ex voluptatem voluptatem temporibus itaque repellat necessitatibus minima dignissimos ut.', 1, '100.14.13.55', NULL, NULL, '2018-05-13 01:24:56', '2018-05-13 01:24:56');
INSERT INTO `hu_admins` VALUES (97, 'mrussel', 'uriel57@yahoo.com', '13144040961', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'pkuhic', 'Voluptatem rerum quia eaque ipsum excepturi sint.', 0, '147.184.200.94', NULL, NULL, '2018-05-13 01:24:56', '2018-05-13 01:24:56');
INSERT INTO `hu_admins` VALUES (98, 'heathcote.chesley', 'tanderson@yahoo.com', '13136165644', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'rhalvorson', 'Eum amet aspernatur eveniet impedit sint error.', 1, '176.158.1.80', NULL, NULL, '2018-05-13 01:24:56', '2018-05-13 01:24:56');
INSERT INTO `hu_admins` VALUES (99, 'spinka.mario', 'rlarkin@hotmail.com', '13127226388', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'justyn65', 'Magni exercitationem nihil dolorem illo laborum omnis.', 1, '222.251.93.125', NULL, NULL, '2018-05-13 01:24:56', '2018-05-13 01:24:56');
INSERT INTO `hu_admins` VALUES (100, 'grant.minerva', 'gstamm@hotmail.com', '13140950373', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'ritchie.alejandrin', 'Commodi nesciunt soluta ut veniam omnis mollitia quaerat exercitationem ea.', 1, '105.179.240.251', NULL, NULL, '2018-05-13 01:24:56', '2018-05-13 01:24:56');
INSERT INTO `hu_admins` VALUES (101, 'anthoney11', 'anthony12@163.com', '13555555555', '$2y$10$FGfS6n/s8KGFlj4vNMGRW.d2JzSlV5XOo346B4WLA/5cgtqyQisW2', 'king19800105', '', 1, '', NULL, NULL, '2018-05-25 18:18:36', '2018-05-25 18:18:36');
COMMIT;

-- ----------------------------
-- Table structure for hu_article_categories
-- ----------------------------
DROP TABLE IF EXISTS `hu_article_categories`;
CREATE TABLE `hu_article_categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) NOT NULL COMMENT '文章分类名称',
  `alias_name` varchar(191) NOT NULL COMMENT '文章分类别名',
  `parent_id` int(11) NOT NULL DEFAULT '0' COMMENT '父级id',
  `seo_keywords` varchar(191) NOT NULL DEFAULT '' COMMENT 'seo关键词',
  `seo_title` varchar(191) NOT NULL DEFAULT '' COMMENT 'seo标签',
  `seo_description` varchar(191) NOT NULL DEFAULT '' COMMENT 'seo描述',
  `weight` tinyint(4) NOT NULL DEFAULT '0' COMMENT '排序权重',
  `img` varchar(191) NOT NULL DEFAULT '' COMMENT '分类图片地址',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `article_categories_name_unique` (`name`),
  UNIQUE KEY `article_categories_alias_name_unique` (`alias_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for hu_articles
-- ----------------------------
DROP TABLE IF EXISTS `hu_articles`;
CREATE TABLE `hu_articles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(191) NOT NULL,
  `author` varchar(191) NOT NULL DEFAULT '',
  `content_markdown` text NOT NULL,
  `content_html` text NOT NULL,
  `intro` varchar(191) NOT NULL DEFAULT '',
  `category_id` int(10) unsigned NOT NULL,
  `img` varchar(191) NOT NULL DEFAULT '',
  `weight` tinyint(4) NOT NULL DEFAULT '0',
  `is_top` tinyint(4) NOT NULL DEFAULT '0',
  `is_recommend` tinyint(4) NOT NULL DEFAULT '0',
  `is_hot` tinyint(4) NOT NULL DEFAULT '0',
  `state` tinyint(4) NOT NULL DEFAULT '1',
  `seo_title` varchar(191) NOT NULL DEFAULT '',
  `seo_keywords` varchar(191) NOT NULL DEFAULT '',
  `seo_description` varchar(191) NOT NULL DEFAULT '',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `articles_title_unique` (`title`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for hu_channel_channel_group
-- ----------------------------
DROP TABLE IF EXISTS `hu_channel_channel_group`;
CREATE TABLE `hu_channel_channel_group` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `channel_id` int(11) NOT NULL COMMENT '通道id',
  `channel_group` int(11) NOT NULL COMMENT '通道组id',
  `rate` tinyint(4) NOT NULL COMMENT '0：不可用，先被过滤掉。1~100,百分比分配比率',
  `price` decimal(6,5) NOT NULL DEFAULT '0.00000' COMMENT '该通道发送此类短信的价格',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `channel_channel_group_channel_id_index` (`channel_id`),
  KEY `channel_channel_group_channel_group_index` (`channel_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Table structure for hu_channel_groups
-- ----------------------------
DROP TABLE IF EXISTS `hu_channel_groups`;
CREATE TABLE `hu_channel_groups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `group_name` varchar(191) COLLATE utf8mb4_general_ci NOT NULL COMMENT '分组名称',
  `parent_id` int(11) NOT NULL COMMENT '上级分组id',
  `state` tinyint(4) NOT NULL COMMENT '当前状态（是否可用）',
  `weight` tinyint(4) NOT NULL COMMENT '显示排序权重值',
  `remark` varchar(191) COLLATE utf8mb4_general_ci NOT NULL COMMENT '备注',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `group_name` (`group_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Table structure for hu_channel_model_sets
-- ----------------------------
DROP TABLE IF EXISTS `hu_channel_model_sets`;
CREATE TABLE `hu_channel_model_sets` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `model_name` varchar(191) COLLATE utf8mb4_general_ci NOT NULL DEFAULT '0' COMMENT '模型名称',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Table structure for hu_channel_sign_report
-- ----------------------------
DROP TABLE IF EXISTS `hu_channel_sign_report`;
CREATE TABLE `hu_channel_sign_report` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sign_report_id` int(11) NOT NULL COMMENT '签名报备表id',
  `channel_id` int(11) NOT NULL COMMENT '所属通道id',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Table structure for hu_channels
-- ----------------------------
DROP TABLE IF EXISTS `hu_channels`;
CREATE TABLE `hu_channels` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '通道名称',
  `group_id` int(11) NOT NULL DEFAULT '0' COMMENT '通道组关联id',
  `access_type` varchar(50) COLLATE utf8mb4_general_ci NOT NULL COMMENT '对接类型, cmpp，http等',
  `access_domain` varchar(191) COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '接入域名',
  `access_ip` varchar(191) COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '接入ip地址',
  `access_port` mediumint(9) NOT NULL DEFAULT '0' COMMENT '接入端口',
  `channel_model_id` int(11) NOT NULL DEFAULT '0' COMMENT '通道模型id',
  `price` decimal(6,5) NOT NULL DEFAULT '0.00000' COMMENT '通道单价，同级别的多个通道，根据价格优先原则选择。',
  `is_sign_limit` tinyint(4) NOT NULL DEFAULT '0' COMMENT '是否受到签名限制（0：无限制，1：受限）',
  `state` tinyint(4) NOT NULL DEFAULT '0' COMMENT '当前状态（是否可用）',
  `weight` tinyint(4) NOT NULL DEFAULT '0' COMMENT '显示的权重，根据权重来排名',
  `remark` varchar(191) COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '通道备注信息',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `channels_name_unique` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Table structure for hu_companies
-- ----------------------------
DROP TABLE IF EXISTS `hu_companies`;
CREATE TABLE `hu_companies` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0' COMMENT '用户id',
  `company_name` varchar(191) COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '公司名称',
  `qualification_state` tinyint(4) NOT NULL DEFAULT '0' COMMENT '审核状态',
  `license_uri` varchar(191) COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '营业执照地址',
  `organization_uri` varchar(191) COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '组织机构代码证',
  `id_card_front_uri` varchar(191) COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '经办人身份证正面',
  `id_card_back_uri` varchar(191) COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '经办人身份证反面',
  `tax_register_uri` varchar(191) COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '税务登记证',
  `icp_uri` varchar(191) COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '备案截图',
  `sms_app_uri` varchar(191) COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '短信应用截图',
  `site_url` varchar(191) COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '官网地址',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `companies_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Table structure for hu_configs
-- ----------------------------
DROP TABLE IF EXISTS `hu_configs`;
CREATE TABLE `hu_configs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) NOT NULL COMMENT '配置类型，如：common',
  `show_name` varchar(191) NOT NULL DEFAULT '' COMMENT '配置名称，如：公共配置',
  `info` text NOT NULL COMMENT '配置内容，以json格式存储',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `configs_name_unique` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for hu_goods
-- ----------------------------
DROP TABLE IF EXISTS `hu_goods`;
CREATE TABLE `hu_goods` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) NOT NULL,
  `alias_name` varchar(191) NOT NULL DEFAULT '',
  `goods_category_id` int(11) NOT NULL DEFAULT '0',
  `goods_model_id` int(11) NOT NULL DEFAULT '0',
  `img` varchar(191) NOT NULL DEFAULT '',
  `goods_no` varchar(191) NOT NULL DEFAULT '',
  `shop_price` decimal(8,2) NOT NULL DEFAULT '0.00',
  `market_price` decimal(8,2) NOT NULL DEFAULT '0.00',
  `inventory` int(11) NOT NULL DEFAULT '1',
  `seo_title` varchar(191) NOT NULL DEFAULT '',
  `seo_keywords` varchar(191) NOT NULL DEFAULT '',
  `seo_description` varchar(191) NOT NULL DEFAULT '',
  `intro` varchar(191) NOT NULL DEFAULT '',
  `content_html` text NOT NULL,
  `content_markdown` text NOT NULL,
  `is_hot` tinyint(4) NOT NULL DEFAULT '0',
  `is_new` tinyint(4) NOT NULL DEFAULT '0',
  `is_recommend` tinyint(4) NOT NULL DEFAULT '0',
  `is_discount` tinyint(4) NOT NULL DEFAULT '0',
  `state` tinyint(4) NOT NULL DEFAULT '1',
  `weight` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `goods_name_unique` (`name`),
  UNIQUE KEY `goods_goods_no_unique` (`goods_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for hu_goods_categories
-- ----------------------------
DROP TABLE IF EXISTS `hu_goods_categories`;
CREATE TABLE `hu_goods_categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) NOT NULL COMMENT '商品分类名称',
  `alias_name` varchar(191) NOT NULL DEFAULT '' COMMENT '商品分类别名',
  `parent_id` int(11) NOT NULL DEFAULT '0' COMMENT '分类父级id，顶级为0',
  `show_type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '显示类型，1：全部端都显示，2:仅支持pc端，3：支持手机端',
  `weight` tinyint(4) NOT NULL DEFAULT '1' COMMENT '权重',
  `state` tinyint(4) NOT NULL DEFAULT '1' COMMENT '分类状态，1显示，0不显示',
  `img` varchar(191) NOT NULL DEFAULT '' COMMENT '分类图片url',
  `seo_title` varchar(191) NOT NULL DEFAULT '',
  `seo_keywords` varchar(191) NOT NULL DEFAULT '',
  `seo_description` varchar(191) NOT NULL DEFAULT '',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `goods_categories_name_unique` (`name`),
  KEY `goods_categories_alias_name_index` (`alias_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for hu_inside_message_contents
-- ----------------------------
DROP TABLE IF EXISTS `hu_inside_message_contents`;
CREATE TABLE `hu_inside_message_contents` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(191) NOT NULL DEFAULT '' COMMENT '标题',
  `contact_way` varchar(191) NOT NULL DEFAULT '' COMMENT '联系方式',
  `content` text NOT NULL COMMENT '内容',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `inside_message_contents_title_index` (`title`),
  KEY `inside_message_contents_contact_way_index` (`contact_way`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of hu_inside_message_contents
-- ----------------------------
BEGIN;
INSERT INTO `hu_inside_message_contents` VALUES (1, 'sdfsfafsaf', '13333333', 'sfafafaf', '2018-06-08 10:36:52', '2018-06-08 10:36:52');
INSERT INTO `hu_inside_message_contents` VALUES (2, 'sdfsfafsaf', '13333333', 'sfafafaf', '2018-06-08 10:38:05', '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_contents` VALUES (3, 'sdfsfafsaf111', '13333333', 'sfafafaf', '2018-06-08 10:40:23', '2018-06-08 10:40:23');
INSERT INTO `hu_inside_message_contents` VALUES (4, 'sdfsfafsaf', '13333333', 'sfafafaf', '2018-06-08 10:41:00', '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_contents` VALUES (5, 'sdfsfafsaf', '13333333', 'sfafafaf', '2018-06-27 15:55:09', '2018-06-27 15:55:09');
INSERT INTO `hu_inside_message_contents` VALUES (6, 'sdfsfafsaf', '13333333', 'sfafafaf', '2018-06-27 15:56:52', '2018-06-27 15:56:52');
INSERT INTO `hu_inside_message_contents` VALUES (7, 'sdfsfafsaf', '13333333', 'sfafafaf', '2018-06-27 16:00:46', '2018-06-27 16:00:46');
INSERT INTO `hu_inside_message_contents` VALUES (8, 'sdfsfafsaf', '13333333', 'sfafafaf', '2018-06-27 16:01:09', '2018-06-27 16:01:09');
INSERT INTO `hu_inside_message_contents` VALUES (9, 'sdfsfafsaf', '13333333', 'sfafafaf', '2018-06-27 17:08:13', '2018-06-27 17:08:13');
INSERT INTO `hu_inside_message_contents` VALUES (10, 'sdfsfafsaf', '13333333', 'sfafafaf', '2018-06-27 17:18:34', '2018-06-27 17:18:34');
INSERT INTO `hu_inside_message_contents` VALUES (11, 'sdfsfafsaf', '13333333', 'sfafafaf', '2018-06-27 17:34:07', '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_contents` VALUES (12, 'sdfsfafsaf', '13333333', 'sfafafaf', '2018-06-27 17:34:53', '2018-06-27 17:34:53');
COMMIT;

-- ----------------------------
-- Table structure for hu_inside_message_systems
-- ----------------------------
DROP TABLE IF EXISTS `hu_inside_message_systems`;
CREATE TABLE `hu_inside_message_systems` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NOT NULL COMMENT '推送给的客户id',
  `message_id` int(11) NOT NULL DEFAULT '0' COMMENT '信息表id',
  `state` tinyint(4) NOT NULL DEFAULT '0' COMMENT '未读。已读',
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `inside_message_systems_customer_id_index` (`customer_id`),
  KEY `inside_message_systems_created_at_index` (`created_at`)
) ENGINE=InnoDB AUTO_INCREMENT=1501 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of hu_inside_message_systems
-- ----------------------------
BEGIN;
INSERT INTO `hu_inside_message_systems` VALUES (1, 1, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (2, 2, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (3, 3, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (4, 4, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (5, 5, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (6, 6, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (7, 7, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (8, 8, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (9, 9, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (10, 10, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (11, 11, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (12, 12, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (13, 13, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (14, 14, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (15, 15, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (16, 16, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (17, 17, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (18, 18, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (19, 19, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (20, 20, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (21, 21, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (22, 22, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (23, 23, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (24, 24, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (25, 25, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (26, 26, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (27, 27, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (28, 28, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (29, 29, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (30, 30, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (31, 31, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (32, 32, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (33, 33, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (34, 34, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (35, 35, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (36, 36, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (37, 37, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (38, 38, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (39, 39, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (40, 40, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (41, 41, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (42, 42, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (43, 43, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (44, 44, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (45, 45, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (46, 46, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (47, 47, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (48, 48, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (49, 49, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (50, 50, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (51, 51, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (52, 52, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (53, 53, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (54, 54, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (55, 55, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (56, 56, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (57, 57, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (58, 58, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (59, 59, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (60, 60, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (61, 61, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (62, 62, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (63, 63, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (64, 64, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (65, 65, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (66, 66, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (67, 67, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (68, 68, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (69, 69, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (70, 70, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (71, 71, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (72, 72, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (73, 73, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (74, 74, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (75, 75, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (76, 76, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (77, 77, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (78, 78, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (79, 79, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (80, 80, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (81, 81, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (82, 82, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (83, 83, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (84, 84, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (85, 85, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (86, 86, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (87, 87, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (88, 88, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (89, 89, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (90, 90, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (91, 91, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (92, 92, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (93, 93, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (94, 94, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (95, 95, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (96, 96, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (97, 97, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (98, 98, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (99, 99, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (100, 100, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (101, 101, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (102, 102, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (103, 103, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (104, 104, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (105, 105, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (106, 106, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (107, 107, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (108, 108, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (109, 109, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (110, 110, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (111, 111, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (112, 112, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (113, 113, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (114, 114, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (115, 115, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (116, 116, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (117, 117, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (118, 118, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (119, 119, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (120, 120, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (121, 121, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (122, 122, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (123, 123, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (124, 124, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (125, 125, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (126, 126, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (127, 127, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (128, 128, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (129, 129, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (130, 130, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (131, 131, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (132, 132, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (133, 133, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (134, 134, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (135, 135, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (136, 136, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (137, 137, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (138, 138, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (139, 139, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (140, 140, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (141, 141, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (142, 142, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (143, 143, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (144, 144, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (145, 145, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (146, 146, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (147, 147, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (148, 148, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (149, 149, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (150, 150, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (151, 151, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (152, 152, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (153, 153, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (154, 154, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (155, 155, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (156, 156, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (157, 157, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (158, 158, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (159, 159, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (160, 160, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (161, 161, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (162, 162, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (163, 163, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (164, 164, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (165, 165, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (166, 166, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (167, 167, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (168, 168, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (169, 169, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (170, 170, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (171, 171, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (172, 172, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (173, 173, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (174, 174, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (175, 175, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (176, 176, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (177, 177, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (178, 178, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (179, 179, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (180, 180, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (181, 181, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (182, 182, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (183, 183, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (184, 184, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (185, 185, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (186, 186, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (187, 187, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (188, 188, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (189, 189, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (190, 190, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (191, 191, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (192, 192, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (193, 193, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (194, 194, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (195, 195, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (196, 196, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (197, 197, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (198, 198, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (199, 199, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (200, 200, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (201, 201, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (202, 202, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (203, 203, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (204, 204, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (205, 205, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (206, 206, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (207, 207, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (208, 208, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (209, 209, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (210, 210, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (211, 211, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (212, 212, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (213, 213, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (214, 214, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (215, 215, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (216, 216, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (217, 217, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (218, 218, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (219, 219, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (220, 220, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (221, 221, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (222, 222, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (223, 223, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (224, 224, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (225, 225, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (226, 226, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (227, 227, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (228, 228, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (229, 229, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (230, 230, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (231, 231, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (232, 232, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (233, 233, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (234, 234, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (235, 235, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (236, 236, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (237, 237, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (238, 238, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (239, 239, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (240, 240, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (241, 241, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (242, 242, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (243, 243, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (244, 244, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (245, 245, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (246, 246, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (247, 247, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (248, 248, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (249, 249, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (250, 250, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (251, 251, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (252, 252, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (253, 253, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (254, 254, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (255, 255, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (256, 256, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (257, 257, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (258, 258, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (259, 259, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (260, 260, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (261, 261, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (262, 262, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (263, 263, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (264, 264, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (265, 265, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (266, 266, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (267, 267, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (268, 268, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (269, 269, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (270, 270, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (271, 271, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (272, 272, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (273, 273, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (274, 274, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (275, 275, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (276, 276, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (277, 277, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (278, 278, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (279, 279, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (280, 280, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (281, 281, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (282, 282, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (283, 283, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (284, 284, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (285, 285, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (286, 286, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (287, 287, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (288, 288, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (289, 289, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (290, 290, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (291, 291, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (292, 292, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (293, 293, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (294, 294, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (295, 295, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (296, 296, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (297, 297, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (298, 298, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (299, 299, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (300, 300, 1, 0, NULL, '2018-06-08 10:36:53');
INSERT INTO `hu_inside_message_systems` VALUES (301, 1, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (302, 2, 2, 1, '2018-06-08 16:54:04', '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (303, 3, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (304, 4, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (305, 5, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (306, 6, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (307, 7, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (308, 8, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (309, 9, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (310, 10, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (311, 11, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (312, 12, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (313, 13, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (314, 14, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (315, 15, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (316, 16, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (317, 17, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (318, 18, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (319, 19, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (320, 20, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (321, 21, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (322, 22, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (323, 23, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (324, 24, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (325, 25, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (326, 26, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (327, 27, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (328, 28, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (329, 29, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (330, 30, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (331, 31, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (332, 32, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (333, 33, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (334, 34, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (335, 35, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (336, 36, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (337, 37, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (338, 38, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (339, 39, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (340, 40, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (341, 41, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (342, 42, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (343, 43, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (344, 44, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (345, 45, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (346, 46, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (347, 47, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (348, 48, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (349, 49, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (350, 50, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (351, 51, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (352, 52, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (353, 53, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (354, 54, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (355, 55, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (356, 56, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (357, 57, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (358, 58, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (359, 59, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (360, 60, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (361, 61, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (362, 62, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (363, 63, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (364, 64, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (365, 65, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (366, 66, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (367, 67, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (368, 68, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (369, 69, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (370, 70, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (371, 71, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (372, 72, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (373, 73, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (374, 74, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (375, 75, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (376, 76, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (377, 77, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (378, 78, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (379, 79, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (380, 80, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (381, 81, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (382, 82, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (383, 83, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (384, 84, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (385, 85, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (386, 86, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (387, 87, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (388, 88, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (389, 89, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (390, 90, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (391, 91, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (392, 92, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (393, 93, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (394, 94, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (395, 95, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (396, 96, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (397, 97, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (398, 98, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (399, 99, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (400, 100, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (401, 101, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (402, 102, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (403, 103, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (404, 104, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (405, 105, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (406, 106, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (407, 107, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (408, 108, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (409, 109, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (410, 110, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (411, 111, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (412, 112, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (413, 113, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (414, 114, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (415, 115, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (416, 116, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (417, 117, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (418, 118, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (419, 119, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (420, 120, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (421, 121, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (422, 122, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (423, 123, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (424, 124, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (425, 125, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (426, 126, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (427, 127, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (428, 128, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (429, 129, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (430, 130, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (431, 131, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (432, 132, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (433, 133, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (434, 134, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (435, 135, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (436, 136, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (437, 137, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (438, 138, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (439, 139, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (440, 140, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (441, 141, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (442, 142, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (443, 143, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (444, 144, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (445, 145, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (446, 146, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (447, 147, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (448, 148, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (449, 149, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (450, 150, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (451, 151, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (452, 152, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (453, 153, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (454, 154, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (455, 155, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (456, 156, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (457, 157, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (458, 158, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (459, 159, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (460, 160, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (461, 161, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (462, 162, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (463, 163, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (464, 164, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (465, 165, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (466, 166, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (467, 167, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (468, 168, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (469, 169, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (470, 170, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (471, 171, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (472, 172, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (473, 173, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (474, 174, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (475, 175, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (476, 176, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (477, 177, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (478, 178, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (479, 179, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (480, 180, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (481, 181, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (482, 182, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (483, 183, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (484, 184, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (485, 185, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (486, 186, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (487, 187, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (488, 188, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (489, 189, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (490, 190, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (491, 191, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (492, 192, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (493, 193, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (494, 194, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (495, 195, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (496, 196, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (497, 197, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (498, 198, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (499, 199, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (500, 200, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (501, 201, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (502, 202, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (503, 203, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (504, 204, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (505, 205, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (506, 206, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (507, 207, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (508, 208, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (509, 209, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (510, 210, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (511, 211, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (512, 212, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (513, 213, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (514, 214, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (515, 215, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (516, 216, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (517, 217, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (518, 218, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (519, 219, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (520, 220, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (521, 221, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (522, 222, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (523, 223, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (524, 224, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (525, 225, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (526, 226, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (527, 227, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (528, 228, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (529, 229, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (530, 230, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (531, 231, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (532, 232, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (533, 233, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (534, 234, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (535, 235, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (536, 236, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (537, 237, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (538, 238, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (539, 239, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (540, 240, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (541, 241, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (542, 242, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (543, 243, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (544, 244, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (545, 245, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (546, 246, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (547, 247, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (548, 248, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (549, 249, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (550, 250, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (551, 251, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (552, 252, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (553, 253, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (554, 254, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (555, 255, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (556, 256, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (557, 257, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (558, 258, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (559, 259, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (560, 260, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (561, 261, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (562, 262, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (563, 263, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (564, 264, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (565, 265, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (566, 266, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (567, 267, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (568, 268, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (569, 269, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (570, 270, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (571, 271, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (572, 272, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (573, 273, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (574, 274, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (575, 275, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (576, 276, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (577, 277, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (578, 278, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (579, 279, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (580, 280, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (581, 281, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (582, 282, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (583, 283, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (584, 284, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (585, 285, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (586, 286, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (587, 287, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (588, 288, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (589, 289, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (590, 290, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (591, 291, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (592, 292, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (593, 293, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (594, 294, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (595, 295, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (596, 296, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (597, 297, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (598, 298, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (599, 299, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (600, 300, 2, 0, NULL, '2018-06-08 10:38:05');
INSERT INTO `hu_inside_message_systems` VALUES (601, 1, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (602, 2, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (603, 3, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (604, 4, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (605, 5, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (606, 6, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (607, 7, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (608, 8, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (609, 9, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (610, 10, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (611, 11, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (612, 12, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (613, 13, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (614, 14, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (615, 15, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (616, 16, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (617, 17, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (618, 18, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (619, 19, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (620, 20, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (621, 21, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (622, 22, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (623, 23, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (624, 24, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (625, 25, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (626, 26, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (627, 27, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (628, 28, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (629, 29, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (630, 30, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (631, 31, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (632, 32, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (633, 33, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (634, 34, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (635, 35, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (636, 36, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (637, 37, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (638, 38, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (639, 39, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (640, 40, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (641, 41, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (642, 42, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (643, 43, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (644, 44, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (645, 45, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (646, 46, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (647, 47, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (648, 48, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (649, 49, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (650, 50, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (651, 51, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (652, 52, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (653, 53, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (654, 54, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (655, 55, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (656, 56, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (657, 57, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (658, 58, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (659, 59, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (660, 60, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (661, 61, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (662, 62, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (663, 63, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (664, 64, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (665, 65, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (666, 66, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (667, 67, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (668, 68, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (669, 69, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (670, 70, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (671, 71, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (672, 72, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (673, 73, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (674, 74, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (675, 75, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (676, 76, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (677, 77, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (678, 78, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (679, 79, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (680, 80, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (681, 81, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (682, 82, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (683, 83, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (684, 84, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (685, 85, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (686, 86, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (687, 87, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (688, 88, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (689, 89, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (690, 90, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (691, 91, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (692, 92, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (693, 93, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (694, 94, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (695, 95, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (696, 96, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (697, 97, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (698, 98, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (699, 99, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (700, 100, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (701, 101, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (702, 102, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (703, 103, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (704, 104, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (705, 105, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (706, 106, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (707, 107, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (708, 108, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (709, 109, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (710, 110, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (711, 111, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (712, 112, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (713, 113, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (714, 114, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (715, 115, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (716, 116, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (717, 117, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (718, 118, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (719, 119, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (720, 120, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (721, 121, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (722, 122, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (723, 123, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (724, 124, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (725, 125, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (726, 126, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (727, 127, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (728, 128, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (729, 129, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (730, 130, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (731, 131, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (732, 132, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (733, 133, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (734, 134, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (735, 135, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (736, 136, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (737, 137, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (738, 138, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (739, 139, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (740, 140, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (741, 141, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (742, 142, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (743, 143, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (744, 144, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (745, 145, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (746, 146, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (747, 147, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (748, 148, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (749, 149, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (750, 150, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (751, 151, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (752, 152, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (753, 153, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (754, 154, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (755, 155, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (756, 156, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (757, 157, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (758, 158, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (759, 159, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (760, 160, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (761, 161, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (762, 162, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (763, 163, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (764, 164, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (765, 165, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (766, 166, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (767, 167, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (768, 168, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (769, 169, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (770, 170, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (771, 171, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (772, 172, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (773, 173, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (774, 174, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (775, 175, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (776, 176, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (777, 177, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (778, 178, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (779, 179, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (780, 180, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (781, 181, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (782, 182, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (783, 183, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (784, 184, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (785, 185, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (786, 186, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (787, 187, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (788, 188, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (789, 189, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (790, 190, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (791, 191, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (792, 192, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (793, 193, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (794, 194, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (795, 195, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (796, 196, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (797, 197, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (798, 198, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (799, 199, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (800, 200, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (801, 201, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (802, 202, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (803, 203, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (804, 204, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (805, 205, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (806, 206, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (807, 207, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (808, 208, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (809, 209, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (810, 210, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (811, 211, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (812, 212, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (813, 213, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (814, 214, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (815, 215, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (816, 216, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (817, 217, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (818, 218, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (819, 219, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (820, 220, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (821, 221, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (822, 222, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (823, 223, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (824, 224, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (825, 225, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (826, 226, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (827, 227, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (828, 228, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (829, 229, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (830, 230, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (831, 231, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (832, 232, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (833, 233, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (834, 234, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (835, 235, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (836, 236, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (837, 237, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (838, 238, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (839, 239, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (840, 240, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (841, 241, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (842, 242, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (843, 243, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (844, 244, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (845, 245, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (846, 246, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (847, 247, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (848, 248, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (849, 249, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (850, 250, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (851, 251, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (852, 252, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (853, 253, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (854, 254, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (855, 255, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (856, 256, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (857, 257, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (858, 258, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (859, 259, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (860, 260, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (861, 261, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (862, 262, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (863, 263, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (864, 264, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (865, 265, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (866, 266, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (867, 267, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (868, 268, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (869, 269, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (870, 270, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (871, 271, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (872, 272, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (873, 273, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (874, 274, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (875, 275, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (876, 276, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (877, 277, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (878, 278, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (879, 279, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (880, 280, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (881, 281, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (882, 282, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (883, 283, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (884, 284, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (885, 285, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (886, 286, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (887, 287, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (888, 288, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (889, 289, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (890, 290, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (891, 291, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (892, 292, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (893, 293, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (894, 294, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (895, 295, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (896, 296, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (897, 297, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (898, 298, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (899, 299, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (900, 300, 4, 0, NULL, '2018-06-08 10:41:00');
INSERT INTO `hu_inside_message_systems` VALUES (901, 1, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (902, 2, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (903, 3, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (904, 4, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (905, 5, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (906, 6, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (907, 7, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (908, 8, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (909, 9, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (910, 10, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (911, 11, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (912, 12, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (913, 13, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (914, 14, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (915, 15, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (916, 16, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (917, 17, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (918, 18, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (919, 19, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (920, 20, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (921, 21, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (922, 22, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (923, 23, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (924, 24, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (925, 25, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (926, 26, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (927, 27, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (928, 28, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (929, 29, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (930, 30, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (931, 31, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (932, 32, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (933, 33, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (934, 34, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (935, 35, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (936, 36, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (937, 37, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (938, 38, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (939, 39, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (940, 40, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (941, 41, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (942, 42, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (943, 43, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (944, 44, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (945, 45, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (946, 46, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (947, 47, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (948, 48, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (949, 49, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (950, 50, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (951, 51, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (952, 52, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (953, 53, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (954, 54, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (955, 55, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (956, 56, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (957, 57, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (958, 58, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (959, 59, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (960, 60, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (961, 61, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (962, 62, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (963, 63, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (964, 64, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (965, 65, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (966, 66, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (967, 67, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (968, 68, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (969, 69, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (970, 70, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (971, 71, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (972, 72, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (973, 73, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (974, 74, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (975, 75, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (976, 76, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (977, 77, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (978, 78, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (979, 79, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (980, 80, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (981, 81, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (982, 82, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (983, 83, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (984, 84, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (985, 85, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (986, 86, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (987, 87, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (988, 88, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (989, 89, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (990, 90, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (991, 91, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (992, 92, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (993, 93, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (994, 94, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (995, 95, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (996, 96, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (997, 97, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (998, 98, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (999, 99, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1000, 100, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1001, 101, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1002, 102, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1003, 103, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1004, 104, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1005, 105, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1006, 106, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1007, 107, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1008, 108, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1009, 109, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1010, 110, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1011, 111, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1012, 112, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1013, 113, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1014, 114, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1015, 115, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1016, 116, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1017, 117, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1018, 118, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1019, 119, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1020, 120, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1021, 121, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1022, 122, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1023, 123, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1024, 124, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1025, 125, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1026, 126, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1027, 127, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1028, 128, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1029, 129, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1030, 130, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1031, 131, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1032, 132, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1033, 133, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1034, 134, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1035, 135, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1036, 136, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1037, 137, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1038, 138, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1039, 139, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1040, 140, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1041, 141, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1042, 142, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1043, 143, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1044, 144, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1045, 145, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1046, 146, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1047, 147, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1048, 148, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1049, 149, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1050, 150, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1051, 151, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1052, 152, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1053, 153, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1054, 154, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1055, 155, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1056, 156, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1057, 157, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1058, 158, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1059, 159, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1060, 160, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1061, 161, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1062, 162, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1063, 163, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1064, 164, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1065, 165, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1066, 166, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1067, 167, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1068, 168, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1069, 169, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1070, 170, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1071, 171, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1072, 172, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1073, 173, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1074, 174, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1075, 175, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1076, 176, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1077, 177, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1078, 178, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1079, 179, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1080, 180, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1081, 181, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1082, 182, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1083, 183, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1084, 184, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1085, 185, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1086, 186, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1087, 187, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1088, 188, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1089, 189, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1090, 190, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1091, 191, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1092, 192, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1093, 193, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1094, 194, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1095, 195, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1096, 196, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1097, 197, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1098, 198, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1099, 199, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1100, 200, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1101, 201, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1102, 202, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1103, 203, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1104, 204, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1105, 205, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1106, 206, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1107, 207, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1108, 208, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1109, 209, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1110, 210, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1111, 211, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1112, 212, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1113, 213, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1114, 214, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1115, 215, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1116, 216, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1117, 217, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1118, 218, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1119, 219, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1120, 220, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1121, 221, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1122, 222, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1123, 223, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1124, 224, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1125, 225, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1126, 226, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1127, 227, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1128, 228, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1129, 229, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1130, 230, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1131, 231, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1132, 232, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1133, 233, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1134, 234, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1135, 235, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1136, 236, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1137, 237, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1138, 238, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1139, 239, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1140, 240, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1141, 241, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1142, 242, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1143, 243, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1144, 244, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1145, 245, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1146, 246, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1147, 247, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1148, 248, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1149, 249, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1150, 250, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1151, 251, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1152, 252, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1153, 253, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1154, 254, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1155, 255, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1156, 256, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1157, 257, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1158, 258, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1159, 259, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1160, 260, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1161, 261, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1162, 262, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1163, 263, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1164, 264, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1165, 265, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1166, 266, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1167, 267, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1168, 268, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1169, 269, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1170, 270, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1171, 271, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1172, 272, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1173, 273, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1174, 274, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1175, 275, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1176, 276, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1177, 277, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1178, 278, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1179, 279, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1180, 280, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1181, 281, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1182, 282, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1183, 283, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1184, 284, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1185, 285, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1186, 286, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1187, 287, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1188, 288, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1189, 289, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1190, 290, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1191, 291, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1192, 292, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1193, 293, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1194, 294, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1195, 295, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1196, 296, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1197, 297, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1198, 298, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1199, 299, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1200, 300, 11, 0, NULL, '2018-06-27 17:34:07');
INSERT INTO `hu_inside_message_systems` VALUES (1201, 1, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1202, 2, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1203, 3, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1204, 4, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1205, 5, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1206, 6, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1207, 7, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1208, 8, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1209, 9, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1210, 10, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1211, 11, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1212, 12, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1213, 13, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1214, 14, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1215, 15, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1216, 16, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1217, 17, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1218, 18, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1219, 19, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1220, 20, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1221, 21, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1222, 22, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1223, 23, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1224, 24, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1225, 25, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1226, 26, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1227, 27, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1228, 28, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1229, 29, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1230, 30, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1231, 31, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1232, 32, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1233, 33, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1234, 34, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1235, 35, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1236, 36, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1237, 37, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1238, 38, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1239, 39, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1240, 40, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1241, 41, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1242, 42, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1243, 43, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1244, 44, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1245, 45, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1246, 46, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1247, 47, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1248, 48, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1249, 49, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1250, 50, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1251, 51, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1252, 52, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1253, 53, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1254, 54, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1255, 55, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1256, 56, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1257, 57, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1258, 58, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1259, 59, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1260, 60, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1261, 61, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1262, 62, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1263, 63, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1264, 64, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1265, 65, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1266, 66, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1267, 67, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1268, 68, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1269, 69, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1270, 70, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1271, 71, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1272, 72, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1273, 73, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1274, 74, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1275, 75, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1276, 76, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1277, 77, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1278, 78, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1279, 79, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1280, 80, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1281, 81, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1282, 82, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1283, 83, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1284, 84, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1285, 85, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1286, 86, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1287, 87, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1288, 88, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1289, 89, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1290, 90, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1291, 91, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1292, 92, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1293, 93, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1294, 94, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1295, 95, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1296, 96, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1297, 97, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1298, 98, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1299, 99, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1300, 100, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1301, 101, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1302, 102, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1303, 103, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1304, 104, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1305, 105, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1306, 106, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1307, 107, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1308, 108, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1309, 109, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1310, 110, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1311, 111, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1312, 112, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1313, 113, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1314, 114, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1315, 115, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1316, 116, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1317, 117, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1318, 118, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1319, 119, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1320, 120, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1321, 121, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1322, 122, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1323, 123, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1324, 124, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1325, 125, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1326, 126, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1327, 127, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1328, 128, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1329, 129, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1330, 130, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1331, 131, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1332, 132, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1333, 133, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1334, 134, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1335, 135, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1336, 136, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1337, 137, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1338, 138, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1339, 139, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1340, 140, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1341, 141, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1342, 142, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1343, 143, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1344, 144, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1345, 145, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1346, 146, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1347, 147, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1348, 148, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1349, 149, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1350, 150, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1351, 151, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1352, 152, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1353, 153, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1354, 154, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1355, 155, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1356, 156, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1357, 157, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1358, 158, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1359, 159, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1360, 160, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1361, 161, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1362, 162, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1363, 163, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1364, 164, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1365, 165, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1366, 166, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1367, 167, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1368, 168, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1369, 169, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1370, 170, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1371, 171, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1372, 172, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1373, 173, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1374, 174, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1375, 175, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1376, 176, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1377, 177, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1378, 178, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1379, 179, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1380, 180, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1381, 181, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1382, 182, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1383, 183, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1384, 184, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1385, 185, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1386, 186, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1387, 187, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1388, 188, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1389, 189, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1390, 190, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1391, 191, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1392, 192, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1393, 193, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1394, 194, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1395, 195, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1396, 196, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1397, 197, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1398, 198, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1399, 199, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1400, 200, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1401, 201, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1402, 202, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1403, 203, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1404, 204, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1405, 205, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1406, 206, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1407, 207, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1408, 208, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1409, 209, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1410, 210, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1411, 211, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1412, 212, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1413, 213, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1414, 214, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1415, 215, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1416, 216, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1417, 217, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1418, 218, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1419, 219, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1420, 220, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1421, 221, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1422, 222, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1423, 223, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1424, 224, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1425, 225, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1426, 226, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1427, 227, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1428, 228, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1429, 229, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1430, 230, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1431, 231, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1432, 232, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1433, 233, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1434, 234, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1435, 235, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1436, 236, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1437, 237, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1438, 238, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1439, 239, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1440, 240, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1441, 241, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1442, 242, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1443, 243, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1444, 244, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1445, 245, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1446, 246, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1447, 247, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1448, 248, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1449, 249, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1450, 250, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1451, 251, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1452, 252, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1453, 253, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1454, 254, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1455, 255, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1456, 256, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1457, 257, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1458, 258, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1459, 259, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1460, 260, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1461, 261, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1462, 262, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1463, 263, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1464, 264, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1465, 265, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1466, 266, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1467, 267, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1468, 268, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1469, 269, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1470, 270, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1471, 271, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1472, 272, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1473, 273, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1474, 274, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1475, 275, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1476, 276, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1477, 277, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1478, 278, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1479, 279, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1480, 280, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1481, 281, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1482, 282, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1483, 283, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1484, 284, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1485, 285, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1486, 286, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1487, 287, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1488, 288, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1489, 289, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1490, 290, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1491, 291, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1492, 292, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1493, 293, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1494, 294, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1495, 295, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1496, 296, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1497, 297, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1498, 298, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1499, 299, 12, 0, NULL, '2018-06-27 17:34:53');
INSERT INTO `hu_inside_message_systems` VALUES (1500, 300, 12, 0, NULL, '2018-06-27 17:34:53');
COMMIT;

-- ----------------------------
-- Table structure for hu_inside_messages
-- ----------------------------
DROP TABLE IF EXISTS `hu_inside_messages`;
CREATE TABLE `hu_inside_messages` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `sender_id` int(11) NOT NULL DEFAULT '0' COMMENT '发起人id',
  `recipient_id` int(11) NOT NULL DEFAULT '0' COMMENT '接收人id，如果是系统群发，则为0',
  `sender_model` varchar(191) NOT NULL DEFAULT '' COMMENT '发送者模型',
  `recipient_model` varchar(191) NOT NULL DEFAULT '' COMMENT '接收者模型',
  `content_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '内部消息表id',
  `state` tinyint(4) NOT NULL DEFAULT '0' COMMENT '当前信息的状态。0：未查看，1：已查看，2：已撤回',
  `type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1.站内系统通知(单向。管理员->会员)。2.业务沟通（双向。管理员和会员）。',
  `created_at` timestamp NOT NULL,
  `updated_at` timestamp NOT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `inside_messages_sender_id_index` (`sender_id`),
  KEY `inside_messages_recipient_id_index` (`recipient_id`),
  KEY `inside_messages_created_at` (`created_at`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of hu_inside_messages
-- ----------------------------
BEGIN;
INSERT INTO `hu_inside_messages` VALUES (1, 5, 0, 'App\\Models\\Admin', 'App\\Models\\User', 1, 0, 2, '2018-06-08 10:36:52', '2018-06-08 10:36:52');
INSERT INTO `hu_inside_messages` VALUES (2, 5, 0, 'App\\Models\\Admin', 'App\\Models\\User', 3, 0, 2, '2018-06-08 10:38:05', '2018-06-12 16:31:50');
INSERT INTO `hu_inside_messages` VALUES (3, 5, 5, 'App\\Models\\Admin', 'App\\Models\\User', 3, 0, 1, '2018-06-08 10:40:23', '2018-06-12 16:12:43');
INSERT INTO `hu_inside_messages` VALUES (4, 5, 0, 'App\\Models\\Admin', 'App\\Models\\User', 4, 0, 2, '2018-06-08 10:41:00', '2018-06-08 10:41:00');
INSERT INTO `hu_inside_messages` VALUES (5, 5, 0, 'App\\Models\\Admin', 'App\\Models\\User', 5, 0, 1, '2018-06-27 15:55:09', '2018-06-27 15:55:09');
INSERT INTO `hu_inside_messages` VALUES (6, 5, 0, 'App\\Models\\Admin', 'App\\Models\\User', 6, 0, 1, '2018-06-27 15:56:52', '2018-06-27 15:56:52');
INSERT INTO `hu_inside_messages` VALUES (7, 5, 0, 'App\\Models\\Admin', 'App\\Models\\User', 7, 0, 1, '2018-06-27 16:00:46', '2018-06-27 16:00:46');
INSERT INTO `hu_inside_messages` VALUES (8, 5, 0, 'App\\Models\\Admin', 'App\\Models\\User', 8, 0, 1, '2018-06-27 16:01:09', '2018-06-27 16:01:09');
INSERT INTO `hu_inside_messages` VALUES (9, 5, 0, 'App\\Models\\Admin', 'App\\Models\\User', 9, 0, 1, '2018-06-27 17:08:14', '2018-06-27 17:08:14');
INSERT INTO `hu_inside_messages` VALUES (10, 5, 0, 'App\\Models\\Admin', 'App\\Models\\User', 10, 0, 1, '2018-06-27 17:18:35', '2018-06-27 17:18:35');
INSERT INTO `hu_inside_messages` VALUES (11, 5, 0, 'App\\Models\\Admin', 'App\\Models\\User', 11, 0, 1, '2018-06-27 17:34:07', '2018-06-27 17:34:07');
INSERT INTO `hu_inside_messages` VALUES (12, 5, 0, 'App\\Models\\Admin', 'App\\Models\\User', 12, 0, 1, '2018-06-27 17:34:53', '2018-06-27 17:34:53');
COMMIT;

-- ----------------------------
-- Table structure for hu_menus
-- ----------------------------
DROP TABLE IF EXISTS `hu_menus`;
CREATE TABLE `hu_menus` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) NOT NULL COMMENT '菜单名称',
  `slug` varchar(191) NOT NULL DEFAULT '' COMMENT '菜单权限',
  `parent_id` int(11) NOT NULL DEFAULT '0' COMMENT '父级id',
  `url` varchar(191) NOT NULL DEFAULT '' COMMENT '请求地址，如：/admin',
  `url_alias` varchar(191) NOT NULL DEFAULT '' COMMENT '请求地址的别名，如：admin.index',
  `weight` tinyint(4) NOT NULL DEFAULT '0' COMMENT '权重排序值。0~255',
  `type` varchar(191) NOT NULL DEFAULT 'backend' COMMENT '菜单类型',
  `icon` varchar(191) NOT NULL DEFAULT '' COMMENT '图标',
  `light` varchar(191) NOT NULL DEFAULT '' COMMENT '高亮名称',
  `description` varchar(191) NOT NULL DEFAULT '' COMMENT '路由描述',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_type` (`name`,`type`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of hu_menus
-- ----------------------------
BEGIN;
INSERT INTO `hu_menus` VALUES (1, '管理员模块', 'Admin@index', 0, '', '', 1, 'backend', 'aaa', '', '管理员顶级导航', '2018-07-29 21:28:59', '2018-07-29 21:28:59');
INSERT INTO `hu_menus` VALUES (2, '管理员列表', 'Admin@index', 1, 'admin', 'admin.index', 9, 'backend', 'aaa', '', '管理员顶级导航', '2018-07-29 21:30:06', '2018-07-29 21:30:06');
INSERT INTO `hu_menus` VALUES (3, '权限节点列表', 'Permission@index', 1, 'permission', 'permission.index', 1, 'backend', 'bbb', '', '', '2018-07-29 21:33:40', '2018-07-29 21:33:40');
INSERT INTO `hu_menus` VALUES (4, '角色列表', 'Role@index', 1, 'role', 'role.index', 6, 'backend', 'bbb', '', '', '2018-07-29 21:37:01', '2018-07-29 21:37:01');
INSERT INTO `hu_menus` VALUES (5, '系统模块', 'Menu@index', 0, '', '', 2, 'backend', 'bbb', '', '', '2018-07-29 21:42:59', '2018-07-29 21:42:59');
INSERT INTO `hu_menus` VALUES (6, '菜单列表', 'Menu@index', 5, 'menu', 'menu.index', 1, 'backend', 'ttt', '', '', '2018-07-29 21:47:34', '2018-07-29 21:47:34');
COMMIT;

-- ----------------------------
-- Table structure for hu_migrations
-- ----------------------------
DROP TABLE IF EXISTS `hu_migrations`;
CREATE TABLE `hu_migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of hu_migrations
-- ----------------------------
BEGIN;
INSERT INTO `hu_migrations` VALUES (1, '2014_10_12_000000_create_users_table', 1);
INSERT INTO `hu_migrations` VALUES (2, '2014_10_12_100000_create_password_resets_table', 1);
INSERT INTO `hu_migrations` VALUES (3, '2016_06_01_000001_create_oauth_auth_codes_table', 1);
INSERT INTO `hu_migrations` VALUES (4, '2016_06_01_000002_create_oauth_access_tokens_table', 1);
INSERT INTO `hu_migrations` VALUES (5, '2016_06_01_000003_create_oauth_refresh_tokens_table', 1);
INSERT INTO `hu_migrations` VALUES (6, '2016_06_01_000004_create_oauth_clients_table', 1);
INSERT INTO `hu_migrations` VALUES (7, '2016_06_01_000005_create_oauth_personal_access_clients_table', 1);
INSERT INTO `hu_migrations` VALUES (8, '2018_05_09_084716_create_admins_table', 1);
INSERT INTO `hu_migrations` VALUES (9, '2018_05_10_151756_create_permission_tables', 1);
INSERT INTO `hu_migrations` VALUES (10, '2018_05_12_183105_create_menus_table', 1);
INSERT INTO `hu_migrations` VALUES (11, '2018_05_12_200024_create_configs_table', 1);
INSERT INTO `hu_migrations` VALUES (12, '2018_05_12_215208_create_article_categories_table', 1);
INSERT INTO `hu_migrations` VALUES (13, '2018_05_13_001043_create_articles_table', 1);
INSERT INTO `hu_migrations` VALUES (14, '2018_05_13_005100_create_tags_table', 1);
INSERT INTO `hu_migrations` VALUES (15, '2018_05_23_131746_create_goods_categories_table', 2);
INSERT INTO `hu_migrations` VALUES (16, '2018_05_23_160130_create_goods_table', 3);
INSERT INTO `hu_migrations` VALUES (17, '2018_05_24_130044_create_operation_logs_table', 4);
INSERT INTO `hu_migrations` VALUES (18, '2018_05_24_133931_create_slow_query_logs_table', 5);
INSERT INTO `hu_migrations` VALUES (20, '2018_05_25_173228_create_inside_messages_table', 6);
INSERT INTO `hu_migrations` VALUES (21, '2018_05_31_174609_create_inside_message_contents_table', 7);
INSERT INTO `hu_migrations` VALUES (22, '2018_05_31_175222_create_inside_message_systems_table', 8);
INSERT INTO `hu_migrations` VALUES (23, '2018_06_29_152556_create_review_templates_table', 9);
INSERT INTO `hu_migrations` VALUES (24, '2018_07_17_144958_create_channel_groups_table', 10);
INSERT INTO `hu_migrations` VALUES (25, '2018_08_03_173445_create_channel_model_sets_table', 10);
INSERT INTO `hu_migrations` VALUES (26, '2018_08_06_140641_create_channels_table', 11);
INSERT INTO `hu_migrations` VALUES (27, '2018_08_08_122832_create_channel_channel_group_table', 12);
INSERT INTO `hu_migrations` VALUES (28, '2018_08_08_125530_create_sign_reports_table', 13);
INSERT INTO `hu_migrations` VALUES (29, '2018_08_08_142809_create_channel_sign_report_table', 14);
INSERT INTO `hu_migrations` VALUES (30, '2018_08_08_143938_create_sms_send_records_table', 15);
INSERT INTO `hu_migrations` VALUES (31, '2018_08_08_162022_create_sms_replies_table', 16);
INSERT INTO `hu_migrations` VALUES (32, '2018_08_13_162304_create_users_table', 17);
INSERT INTO `hu_migrations` VALUES (33, '2018_08_14_165520_create_companies_table', 18);
INSERT INTO `hu_migrations` VALUES (34, '2018_08_15_154550_create_user_funds_table', 19);
INSERT INTO `hu_migrations` VALUES (35, '2018_08_15_165228_create_orders_table', 20);
INSERT INTO `hu_migrations` VALUES (36, '2018_08_16_163432_create_sms_send_limits_table', 21);
INSERT INTO `hu_migrations` VALUES (37, '2018_08_16_215856_create_sms_user_lists_table', 22);
COMMIT;

-- ----------------------------
-- Table structure for hu_model_has_permissions
-- ----------------------------
DROP TABLE IF EXISTS `hu_model_has_permissions`;
CREATE TABLE `hu_model_has_permissions` (
  `permission_id` int(10) unsigned NOT NULL,
  `model_type` varchar(191) NOT NULL,
  `model_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  KEY `model_has_permissions_model_type_model_id_index` (`model_type`,`model_id`),
  CONSTRAINT `model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `hu_permissions` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for hu_model_has_roles
-- ----------------------------
DROP TABLE IF EXISTS `hu_model_has_roles`;
CREATE TABLE `hu_model_has_roles` (
  `role_id` int(10) unsigned NOT NULL,
  `model_type` varchar(191) NOT NULL,
  `model_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  KEY `model_has_roles_model_type_model_id_index` (`model_type`,`model_id`),
  CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `hu_roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of hu_model_has_roles
-- ----------------------------
BEGIN;
INSERT INTO `hu_model_has_roles` VALUES (1, 'App\\Models\\Admin', 5);
COMMIT;

-- ----------------------------
-- Table structure for hu_oauth_access_tokens
-- ----------------------------
DROP TABLE IF EXISTS `hu_oauth_access_tokens`;
CREATE TABLE `hu_oauth_access_tokens` (
  `id` varchar(100) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `client_id` int(11) NOT NULL,
  `name` varchar(191) DEFAULT NULL,
  `scopes` text,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_access_tokens_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of hu_oauth_access_tokens
-- ----------------------------
BEGIN;
INSERT INTO `hu_oauth_access_tokens` VALUES ('2aaa81c4a876f620c0ad710b3f7e665f0a40bcc2402a881e18c2102d87f1e74eccaa77a8b0101990', 5, 2, NULL, '[\"client-backend\"]', 0, '2018-07-28 16:46:28', '2018-07-28 16:46:28', '2018-08-27 16:46:28');
INSERT INTO `hu_oauth_access_tokens` VALUES ('53c84e59f5131448043f0fcc1608a6561eca013231081c6b8736477742e56099aadaa6d6071c350b', 5, 2, NULL, '[\"client-backend\"]', 0, '2018-07-28 17:16:36', '2018-07-28 17:16:36', '2018-08-27 17:16:36');
INSERT INTO `hu_oauth_access_tokens` VALUES ('675504ddb6cdc077b551e6ebf89e2e896bb5749df6bc48aea6fcd558c63e2e10d9484efe178712f2', 5, 2, NULL, '[\"client-backend\"]', 0, '2018-07-29 23:44:45', '2018-07-29 23:44:45', '2018-08-28 23:44:45');
INSERT INTO `hu_oauth_access_tokens` VALUES ('87768a64a890019740c85f66e2d53ec3f4e0d5db35565716cfd2a2a7593cfb9184ef1cf6484edb6e', 5, 2, NULL, '[\"client-backend\"]', 0, '2018-07-28 18:51:54', '2018-07-28 18:51:54', '2018-08-27 18:51:53');
INSERT INTO `hu_oauth_access_tokens` VALUES ('8ec98af59c15bbc87cef75ffab06234a4851bd4355fb6840f335d1914930c8e072c42fbe06130569', 5, 2, NULL, '[\"client-backend\"]', 0, '2018-07-28 18:28:43', '2018-07-28 18:28:43', '2018-08-27 18:28:43');
INSERT INTO `hu_oauth_access_tokens` VALUES ('9969eb61269cfddf487e17a71d99d6708bc1bd02ced2a3cee3178e3971850a23489fa7c71b239b98', 5, 2, NULL, '[\"client-backend\"]', 0, '2018-08-12 17:13:59', '2018-08-12 17:13:59', '2018-09-11 17:13:59');
INSERT INTO `hu_oauth_access_tokens` VALUES ('b6ef410eaa1c640f5454ba39e16e688943c696933145ef23aa6e5d304dfc919582f0d1dea7614ef1', 5, 2, NULL, '[\"client-backend\"]', 0, '2018-07-28 19:12:19', '2018-07-28 19:12:19', '2018-08-27 19:12:19');
INSERT INTO `hu_oauth_access_tokens` VALUES ('ded618ba8e407e99697ede6f67b96984291d17a85f13a6780d0a32c4866c6d7384785a9cd3943000', 5, 2, NULL, '[\"client-backend\"]', 0, '2018-07-29 23:39:34', '2018-07-29 23:39:34', '2018-08-28 23:39:34');
INSERT INTO `hu_oauth_access_tokens` VALUES ('e63ae43b12c5c6660bed64a968b0b03a071f05a6b00eab6b353b232a5a193c5bb5c8d6bd21f24bea', 5, 2, NULL, '[\"client-backend\"]', 0, '2018-07-29 16:00:36', '2018-07-29 16:00:36', '2018-08-28 16:00:35');
INSERT INTO `hu_oauth_access_tokens` VALUES ('fef11fbeb27d1681a632279b2ecf967820aa365c473ed8e7672673b5ffd1c341e583189a8368fb5d', 5, 2, NULL, '[\"client-backend\"]', 0, '2018-08-12 16:34:27', '2018-08-12 16:34:27', '2018-09-11 16:34:27');
COMMIT;

-- ----------------------------
-- Table structure for hu_oauth_auth_codes
-- ----------------------------
DROP TABLE IF EXISTS `hu_oauth_auth_codes`;
CREATE TABLE `hu_oauth_auth_codes` (
  `id` varchar(100) NOT NULL,
  `user_id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `scopes` text,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for hu_oauth_clients
-- ----------------------------
DROP TABLE IF EXISTS `hu_oauth_clients`;
CREATE TABLE `hu_oauth_clients` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(191) NOT NULL,
  `secret` varchar(100) NOT NULL,
  `redirect` text NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_clients_user_id_index` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of hu_oauth_clients
-- ----------------------------
BEGIN;
INSERT INTO `hu_oauth_clients` VALUES (1, NULL, 'husms Personal Access Client', 'ts8AeMt6PRdeCTuB7PzJ7x4cL17WzXXXSmfRgfbh', 'http://localhost', 1, 0, 0, '2018-05-13 01:27:46', '2018-05-13 01:27:46');
INSERT INTO `hu_oauth_clients` VALUES (2, NULL, 'husms Password Grant Client', 'IRjbX0rJkziAXEW4vQ2TpkfeyUNFzS814cSmiWBd', 'http://localhost', 0, 1, 0, '2018-05-13 01:27:46', '2018-05-13 01:27:46');
INSERT INTO `hu_oauth_clients` VALUES (3, NULL, 'husms Personal Access Client', 'N03NDfYSg0WZeL8tQ2MF615WTsN6p98GTIHVDcVp', 'http://localhost', 1, 0, 0, '2018-05-23 11:27:30', '2018-05-23 11:27:30');
INSERT INTO `hu_oauth_clients` VALUES (4, NULL, 'husms Password Grant Client', 'nMF1ZKePY8JdAeJsA5ZMcllp5ZmfKxHeHOV2okdF', 'http://localhost', 0, 1, 0, '2018-05-23 11:27:30', '2018-05-23 11:27:30');
COMMIT;

-- ----------------------------
-- Table structure for hu_oauth_personal_access_clients
-- ----------------------------
DROP TABLE IF EXISTS `hu_oauth_personal_access_clients`;
CREATE TABLE `hu_oauth_personal_access_clients` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `client_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_personal_access_clients_client_id_index` (`client_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of hu_oauth_personal_access_clients
-- ----------------------------
BEGIN;
INSERT INTO `hu_oauth_personal_access_clients` VALUES (1, 1, '2018-05-13 01:27:46', '2018-05-13 01:27:46');
INSERT INTO `hu_oauth_personal_access_clients` VALUES (2, 3, '2018-05-23 11:27:30', '2018-05-23 11:27:30');
COMMIT;

-- ----------------------------
-- Table structure for hu_oauth_refresh_tokens
-- ----------------------------
DROP TABLE IF EXISTS `hu_oauth_refresh_tokens`;
CREATE TABLE `hu_oauth_refresh_tokens` (
  `id` varchar(100) NOT NULL,
  `access_token_id` varchar(100) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of hu_oauth_refresh_tokens
-- ----------------------------
BEGIN;
INSERT INTO `hu_oauth_refresh_tokens` VALUES ('078c3db20ec47d0812fdb9cc0e8e2ebbf09cc5b129f1b0d3bebbde9cea09f47300f855f074296cfe', '05c6269ae216c92963a6fe594c192f24e17bc5784a1cf71e6383b56337995743ff869ba24d67db5a', 0, '2018-07-12 18:40:51');
INSERT INTO `hu_oauth_refresh_tokens` VALUES ('07aa811430a1f2d43e2fd932447de2312541e401b999e6696f812075a386d05d11b600ce3a1382de', '5ae162e69f54c1d3ad2e6d347184f803867f030884811f2aaee63d5b12aea83e664f34ae953140ca', 0, '2018-07-14 23:51:47');
INSERT INTO `hu_oauth_refresh_tokens` VALUES ('0991bb372e70d0fa9e9d4474850f05a7a66755a24b52a1e59aa8eb1eecbdc3ba3f4eec9ee89e4cc9', 'ded618ba8e407e99697ede6f67b96984291d17a85f13a6780d0a32c4866c6d7384785a9cd3943000', 0, '2018-09-27 23:39:34');
INSERT INTO `hu_oauth_refresh_tokens` VALUES ('0a6ddc54f8ce1442b9712a7b0d7312d5cd36fb1a9d31a4cb94c85972c396db791d6e32322f34b32c', 'e812ff028509afdb96d6a10bc5029723897f94c11712b815e2c554c3daad80e15c5abc4c80aff2b6', 0, '2018-07-12 22:59:55');
INSERT INTO `hu_oauth_refresh_tokens` VALUES ('1037a308ac8c1e7a4e2f475e331895ca59ba7ff0cca7e63d130f3c0781fe32a8565221050416b849', '9ae1b2160f5f89adf24a47df28fb8d99ae510e118f9bf58046e1a7e361182c0177c418d41c77ba66', 0, '2018-07-12 22:39:43');
INSERT INTO `hu_oauth_refresh_tokens` VALUES ('1155a56736ae2c60d13694b8ef06615bdb4db684766c96cc680b52f1d35b53245b528ca70e5e9575', '257a1dcfd50d93c6c2696b41996d19fde55d31fd7aa73d2d012fd94ac2224231736adcfbc334fb14', 0, '2018-07-14 21:25:14');
INSERT INTO `hu_oauth_refresh_tokens` VALUES ('11ea61fd7d723194961b8383d5919013017021c838ec6e131a737779efd82444d346fcfb4c33cb9c', 'becf493112bc022b98dfc4fe7308fd8109f217dbcab4c1c08f152889369e916841c38b2bd1465aad', 0, '2018-07-12 22:37:33');
INSERT INTO `hu_oauth_refresh_tokens` VALUES ('189efc62f84c35505022ad7501fd6f34568e98b0be1fb14f4869e04a46a16e3d7235e4585b8ef8da', '5a7e2fc07fbc8c0b39bb8fc9f189b870835419a99ebddf293d78c0973e9f2b49c9d3b0d6226b0e44', 0, '2018-07-14 22:58:39');
INSERT INTO `hu_oauth_refresh_tokens` VALUES ('1a9637b62bf0dedc0bfddeaac973e2a0a7ac0b84dca66b65c6064f952108f7f5bd5a658662849911', '0711a5ef404ecc2fa9f0ceb1a08f0a6640d3b109839247f6ea9efb23e09f6b356a8cad0ef3f1e254', 0, '2018-07-12 21:36:12');
INSERT INTO `hu_oauth_refresh_tokens` VALUES ('2038bef12255cdbe1ae2c191ff8ecdb30dbec86fac3231b05c931dc0f41e720f97de01ab7cb0ed1b', 'e60882dfa2fb8f08a3493b03cd3bdde73b667ca1df7128644bc4c413a91a8697c3ba48f194cc0679', 0, '2018-07-12 21:38:20');
INSERT INTO `hu_oauth_refresh_tokens` VALUES ('222578db5c043ee54686e147f8421bb9ddb1efda24c4087f12ba797443dd111ad06463d1f8a8f7a2', '1fa8ff1ee114a4cd38713352c61bb41284d011484ad4ce043fd244ca0a7ad9c4df0bbe0f49913582', 0, '2018-07-13 23:11:43');
INSERT INTO `hu_oauth_refresh_tokens` VALUES ('223503ea384a383fcfabf52b1520035e96fcbe5d9232a1d5582307a24f3a6f51ce9f2d7a0c5af534', '8ec98af59c15bbc87cef75ffab06234a4851bd4355fb6840f335d1914930c8e072c42fbe06130569', 0, '2018-09-26 18:28:43');
INSERT INTO `hu_oauth_refresh_tokens` VALUES ('2527d27b7173f7217c7044cb29553674ae17bef137b01d4570222fcc91fa06caa88aa2ced14d368e', '2e5885eb4170e49db3577c2f85f3463881390d61274662144c40e4d14afa06c487b9514287c962b0', 0, '2018-07-12 18:56:17');
INSERT INTO `hu_oauth_refresh_tokens` VALUES ('27460a7936a8822b11850a3884d7b30935f2a79b5eecb223d99eca7142c9fd6d40593091866d9f5e', 'd93f76f4eb9667667761da8b9ea670e82fff358b04845d3f4b25dcc28994c4d802f733e9cde0a2a9', 0, '2018-07-14 23:03:18');
INSERT INTO `hu_oauth_refresh_tokens` VALUES ('2ec81fe574c12f8c65adeb81effbc7cf0388b26cc93b0120196ec561cb26c4d22f48b91ebbc08834', 'af506173c1fe55b0c5964dca6f29951541bb80b038223630f9a9165b6d2820e8d0cd0d8884734b07', 0, '2018-07-12 18:57:39');
INSERT INTO `hu_oauth_refresh_tokens` VALUES ('2ef1c5f3c1ff2a38043c4e2768b96981482fafed6951a0ac7c3d28f45c93e383c3481b16192d400a', 'e63ae43b12c5c6660bed64a968b0b03a071f05a6b00eab6b353b232a5a193c5bb5c8d6bd21f24bea', 0, '2018-09-27 16:00:36');
INSERT INTO `hu_oauth_refresh_tokens` VALUES ('3c398031f4085af4b8f65a3dfb61cd3024225aa681de18533169782c4b081135589639afc47ce8a9', '2aaa81c4a876f620c0ad710b3f7e665f0a40bcc2402a881e18c2102d87f1e74eccaa77a8b0101990', 0, '2018-09-26 16:46:28');
INSERT INTO `hu_oauth_refresh_tokens` VALUES ('3e6f66f7c1fa3a8467589fa3905a8721b1cbc5a04ca1e490d1b5e08c6c8623c66947e6df8814fd6b', '27020336091ebe82d7c38dd5cc67ea8433e740f993def9d2d2a2839d4d1ecf8698e3890b75e2761a', 0, '2018-07-13 22:31:46');
INSERT INTO `hu_oauth_refresh_tokens` VALUES ('42b3009fc1a905b044fd5e47ff35197b54e5e669fd8677b5eb3a5d910a18e94e3affbb8bb593aa95', '486b6e7e3cb43606bab821ed9a67fa81680f5de5f097d0713557d029f8cc9a586c08d0cc35d9fa44', 0, '2018-07-12 18:56:28');
INSERT INTO `hu_oauth_refresh_tokens` VALUES ('43c0a1aa1bfa6953e46ecba4a15d0d8d33dbb2301400575e756093695b99483d6aec7b72b250144b', '9969eb61269cfddf487e17a71d99d6708bc1bd02ced2a3cee3178e3971850a23489fa7c71b239b98', 0, '2018-10-11 17:13:59');
INSERT INTO `hu_oauth_refresh_tokens` VALUES ('43c29d9e43b0b4f45ea884500195305a26fba58545700199b4664b120bc5f1d4ae80ac74fe381442', 'f7048afda09afa5544cf61d49da44492c4b9dec818a0e0674d28dcc8b81e838142b4a771e91390a4', 0, '2018-07-13 23:12:04');
INSERT INTO `hu_oauth_refresh_tokens` VALUES ('4515a9136f6828af9c638e1993ecfd4f9d12c080f71ba2769f657314dfe7babe6dfff1bd5a825ea5', '4b54f43f34368637efb9dacc22860d50d30d9bf4ea6562b00d2d0f87b92ad0b2cd71f08ad5c1b24a', 0, '2018-07-13 23:13:15');
INSERT INTO `hu_oauth_refresh_tokens` VALUES ('4753fa30ec6aa0975acbcc04f26c602daf981950990cb15b24b0621724d54ad23551cac4ea0a970f', 'f889a74ccf0c5880e1150b0d2929721f4d618509b98681bcadf7862e515e77e1f1a6d2f2dc3c7790', 0, '2018-09-01 14:32:42');
INSERT INTO `hu_oauth_refresh_tokens` VALUES ('4bc0e49f0337822ed08b992db34f8023cc4cfa4974cff1d6061360d94438d8e1be88919ec35bdd96', '996f2150cb5ac053ddd2bc390a3676f522a9bdbda36e5c893316bde668aad928c4c7b1a4a59df1ea', 0, '2018-07-12 18:57:28');
INSERT INTO `hu_oauth_refresh_tokens` VALUES ('52d1975bc148a4a56e0e9bed5060bb7ba41009a72653944599c3501ac616061f7ecdfd96d2048819', 'bd66ba4c7309f82670647d7c7d2d30b94bc9466f6e50e7690db78f757f2e37223e43827a32e3a437', 0, '2018-07-12 22:24:50');
INSERT INTO `hu_oauth_refresh_tokens` VALUES ('538740d5820c8014d2152d81c60267314a0d82f0e5b27a573346ba4ad636611433258b23d124b196', '4139492f6a172640f7291ac8b5742cf3fcfdf518fab7a35cea613dff7d20ca445a952651680e2b3d', 0, '2018-07-14 23:02:05');
INSERT INTO `hu_oauth_refresh_tokens` VALUES ('54b36b8f77b24ca19f864ee2edb02c62515faa9929b9f684988b786c6888a1b58cc4e25ec6244969', '686107ed90f9024f4311ab52b6ef54457fc8ff2397cd31d90697160c2d05c6cf79d8f950625bf9bb', 0, '2018-07-12 18:40:34');
INSERT INTO `hu_oauth_refresh_tokens` VALUES ('56464c0e5dd4beae3192c3653904e35cd9d695b415af2ec7dfe6d76128f9023ef69e21e65bddba08', '888c5ff5629d6473aede3904e1514824c1ba095644c74887e5d478e68cd0bf17a287683d20611da1', 0, '2018-07-12 21:35:10');
INSERT INTO `hu_oauth_refresh_tokens` VALUES ('593937c1e812ed17780d68432cfeb2ba6af5fd814cb5d8040467a6620f3b7447ff0f5aa3c6f97bad', 'fc76d78989e07c8162300acce62fb52aebccca371dbdcb975a1baa538ea8f9bd77ab74d489116f48', 0, '2018-07-12 21:37:53');
INSERT INTO `hu_oauth_refresh_tokens` VALUES ('5bd80d0a359a70d83fa435efa04d5241e64cab3e294e89d1409f71ed5163595509fef50d2e24869c', '7aae8b646cd1f3420158d752c02a8ee5d32c0e4ddc2c3bb0e10b7d98778ce7b2b7ba03ed581be01b', 0, '2018-07-19 12:53:11');
INSERT INTO `hu_oauth_refresh_tokens` VALUES ('5fa4328024082fe39d5436ef1d1ee5a66fad937e51683f63a2b7ef2cb3197a740335449f773dc921', 'b17f32199430c64c18884c22a30a54d0045a2ef587ae4efcbd0313fd7bba63e35a66373ade2fcb13', 0, '2018-07-14 22:23:48');
INSERT INTO `hu_oauth_refresh_tokens` VALUES ('5ff333b713b7ba440c7ddc6718c730b935f94b396c2141c6fe95826ed27273979b37ffc014f432f0', '87768a64a890019740c85f66e2d53ec3f4e0d5db35565716cfd2a2a7593cfb9184ef1cf6484edb6e', 0, '2018-09-26 18:51:53');
INSERT INTO `hu_oauth_refresh_tokens` VALUES ('62f757f8b92a970e2d8a8220303b8bee015fde14ae43e644d382513a6b63e5ce1293e020acd84251', '39b035f59224228aec8de74884a9fa0f97acec250da9673ad54536613a80f0f8db21f000fac65beb', 0, '2018-07-12 21:36:05');
INSERT INTO `hu_oauth_refresh_tokens` VALUES ('6ab47c3058a52a691e5dd720eb41ce8dd912fb60b48385068a835b394c79aae72b73168c24a13880', '3c87efb9f8ae24d90e5a7fabf13e619a9f3193dbd9113354c6535032b1e83b2a7d1ace8c05de444a', 0, '2018-07-14 23:13:12');
INSERT INTO `hu_oauth_refresh_tokens` VALUES ('6f3c7b8a5b70115237a0ce40168531e0f38acabb4f4fcf25f9acc253e8b54cae6de568961bb06ec7', '6e93795f98e8a3ead14da601b12af0435ef987cb0c6422ab47efee48d36491ced19f18e5a111f64e', 0, '2018-07-22 11:34:36');
INSERT INTO `hu_oauth_refresh_tokens` VALUES ('71654e0e88a6790c7ddde8050e4e67a54fefdfc802b677efb4b2081e17f28a21dc45c901d4cfc95e', '12dcb039a66dd3c3882f5028b735cd40d4df2cfe6cea6ac10a7d69d18c56378de8164cc230060ad0', 0, '2018-09-01 18:13:12');
INSERT INTO `hu_oauth_refresh_tokens` VALUES ('72598b9feb8090cb59c5e217ed252317e4bd2e510ea89183911aa72757febeea64cd876b98de812b', 'b09bd1239c0ee4fcdfb9bedac5720236e18a38ee3659e700bcaa80fb283995a71fc1a9dfbf15cdbe', 0, '2018-07-12 21:47:23');
INSERT INTO `hu_oauth_refresh_tokens` VALUES ('7771df7d0dbd601b65f2c2cdb031716e2fff355bda323107b610716da8b38cf40f6488c4810866ac', 'b6ef410eaa1c640f5454ba39e16e688943c696933145ef23aa6e5d304dfc919582f0d1dea7614ef1', 0, '2018-09-26 19:12:19');
INSERT INTO `hu_oauth_refresh_tokens` VALUES ('77e973bdadd5f731952a51913fb61b0452ccf63bbabd0458a3028b8eb59237120f68b236d4908d41', 'e01c78c81f59e4bd7e2142cbed9d2c3208557d1f80c20c2b3fb671d8e717ce55790dd9c58786dc0f', 0, '2018-08-26 12:55:28');
INSERT INTO `hu_oauth_refresh_tokens` VALUES ('7c0d70883aa487d06c7031ec4cedc44500d24923c9f4cc4c8b0d4e73ba43dbac676826f389d1ca58', '41698147d70b528e473e8a51b3df806e79c1e5e56b3c984a3e307814b0fc2bfeb493ae6b0be9db67', 0, '2018-07-14 23:10:32');
INSERT INTO `hu_oauth_refresh_tokens` VALUES ('82797b6b60f41f96251bc5c674d19adea58584d1009413a52165772c16c2dec092569a75d134066e', '3e25987d012b44f46c2a69727ad38972a2496e557353855365a196314781012c4985cfa72ea0e0cf', 0, '2018-07-13 22:30:16');
INSERT INTO `hu_oauth_refresh_tokens` VALUES ('836037f147d0ead64aa92ae98845105f5e4307c75177a894dfc3d37b4bad357c6f784825251d223b', '18209567d81536b83b98d697b93b62f04f1b38cdd79b93030090868a873c66ece68fde35b5aeea90', 0, '2018-07-12 21:48:34');
INSERT INTO `hu_oauth_refresh_tokens` VALUES ('83711da19cd8c1563d24d05dfe5cf369c9372a1e3607d3a703d286d96e4a2d1d00ebfd13e7a0ec8e', '1965c64ea1ec6c97f1b13f5b732c6243dbdd9c37dafed555ea97eecd7d51ad50d4871818ea321495', 0, '2018-07-14 22:33:16');
INSERT INTO `hu_oauth_refresh_tokens` VALUES ('8645104da18ae07119d1407c9ed614ca354a02cd4e3e4085a07172bc48eb5edb6f4a6067634eae93', 'ac0f2645549fe8849ddf0736f1583c0083487e73c82ecc89954f21c48835138221b5dd8d16fe93e3', 0, '2018-07-19 12:38:53');
INSERT INTO `hu_oauth_refresh_tokens` VALUES ('956cd72a8d04d36d174b0966360791dabc6ac97a9b52a5b1710d4734853919c399854ccb200e8c9c', '30325ea59e62efdca64e1d3e9b734e3f113cbbbfeec9b8e6914dfa4a3345ef8f8c637f844023521a', 0, '2018-07-14 23:32:58');
INSERT INTO `hu_oauth_refresh_tokens` VALUES ('95756d2539276396470b35d9f363a4931ef1e38853ab843ed701057a3016d8d209ed59ab1ac37322', '17afc36707dbe1a0faab81af516c387fe153436f089e7e04b1ad4c464ecebe8eec5b167395279b51', 0, '2018-07-15 23:33:45');
INSERT INTO `hu_oauth_refresh_tokens` VALUES ('9940722e9fc5754bde68d8b7d4bc150227a1431a97b39ae8e6636ff0464f1dec4ae5086105a467c8', 'cac97a9d628365b27eafc71643f76fca313302e8b71cc12c7a948b36b2458189caa512ce84a95ccc', 0, '2018-07-12 22:38:31');
INSERT INTO `hu_oauth_refresh_tokens` VALUES ('9f1d4f4226351967940fb751a86da3b169b2fc5b155212c47a71b70a488e266ff98bf069ff5ae1fc', '675504ddb6cdc077b551e6ebf89e2e896bb5749df6bc48aea6fcd558c63e2e10d9484efe178712f2', 0, '2018-09-27 23:44:45');
INSERT INTO `hu_oauth_refresh_tokens` VALUES ('a018d8b1e89eb3ceb685f3c8401a588e60999fafcf11442f4305df65bf40df43468ca26d2d07a83f', '2e387ff163a6c7176db83133b4f1328bc9710f44fe0236d42131e87db73df00a2ff67d66d0469211', 0, '2018-07-12 21:35:21');
INSERT INTO `hu_oauth_refresh_tokens` VALUES ('a0d2d57c82d65293cb11b7039ac3dc82328bc9666d45ee398c5d2968cb3aef0831f881e1f430899e', '44868eac48bd1f0e4b1d209c56dbc6721e4fadc55f3d287f6dcecf910685a8e924f937eb0ecf8f46', 0, '2018-07-13 23:13:41');
INSERT INTO `hu_oauth_refresh_tokens` VALUES ('a8c936dcddd997c93983951139c2569d04839026820842b0837c034128094db0b1a73e1700bcff40', 'df3cc7e143dd08ed72c3b0c92ac4423b9a5a7ce9623ba4921e70f89fd5f855cdbc6c2576ca384534', 0, '2018-07-19 11:05:19');
INSERT INTO `hu_oauth_refresh_tokens` VALUES ('af30b3a9d1387443ed0654050e5e96b01691b80d2743f0981701b33a3aba7dca05edae593e45c35b', '164a9e428454eef715a313af37192b1475666615b19464fcde3dfc3633ba9de660220d594c5b412e', 0, '2018-07-23 18:18:03');
INSERT INTO `hu_oauth_refresh_tokens` VALUES ('bb45849b38ebd9ce5f1520680b3cf99d4926915a85967df48328fcc7a9afd525e7d52fdbc8e000cf', '3d4be5163d2ea6d0c4be379e72657c463a777d51b328303c3ea80a35d85dc4551ee3dc94444e9136', 0, '2018-07-12 18:56:33');
INSERT INTO `hu_oauth_refresh_tokens` VALUES ('bd707720973a4c14cd6e6c7ef5059a500e6cd10ef618280ecd7f6740e8cb46c7a5421bebdca901b3', '8603952e15463817edc66d790348d01400a55958e219343e6c81c5445dd7891badfb7722784d19a8', 0, '2018-07-12 21:47:39');
INSERT INTO `hu_oauth_refresh_tokens` VALUES ('c24da08b25e94cc9ee0fda6a6432f9736f164d48a60ba354b6a936989a67b406b593833f00a13148', 'c9c9542229cc42118f933b571b8d25fd4c1f7e4a94f42ba50662238b9e0398ae88e2cb0b365e1399', 0, '2018-07-18 15:48:36');
INSERT INTO `hu_oauth_refresh_tokens` VALUES ('c492c4a3d03b90f9354cdfaf4a31a1367d654a648281a6ec45779a6fd19e6f13ee69281fdc9d5915', 'ceb59c282605b187dec70ab11e10f520aa25899189336110fac2252eccc4dbfd0b2e632eb04fe9e0', 0, '2018-07-12 21:47:08');
INSERT INTO `hu_oauth_refresh_tokens` VALUES ('c61cf55c655a6ca7414caebf4dda0e3bb07d79c4cd0f9f18da6c3a94190bfa1c1be21ad7374a5212', 'ee1b083b86da9d148ee7ce182ab4467385a1d46d66067ca3adc248fec7ece9c92f760d76d21281ec', 0, '2018-07-12 22:51:51');
INSERT INTO `hu_oauth_refresh_tokens` VALUES ('cb5affe87952d58befdbb9616719725e5385ae2d6f0b98433daa47119970da4e92212b77a85d05c3', '9121891e72eeba01d9314b74d389152694d526d2d8bc701c6eb53051f9cf549c9bf879db7dac4031', 0, '2018-07-19 11:05:09');
INSERT INTO `hu_oauth_refresh_tokens` VALUES ('cd25b7e5893e261cf10120368ae3e435319f2686c9573ffeb7963b2f558ce76eb5973c9c782a2fb6', '1ac1f3d1fbb3c390109b4f199ab1b18988c936ae293a14978e536ff13ce01b1c75652829ed14c7fa', 0, '2018-07-19 12:38:41');
INSERT INTO `hu_oauth_refresh_tokens` VALUES ('d7882ef747d976bc4d242d51a45246ecd7d0e5e62071d1ebf29b77461a9f8ba3e01b29bdb7d23888', 'fef11fbeb27d1681a632279b2ecf967820aa365c473ed8e7672673b5ffd1c341e583189a8368fb5d', 0, '2018-10-11 16:34:27');
INSERT INTO `hu_oauth_refresh_tokens` VALUES ('d96b285f554b05fb662c2f0ac13286ec2a79b28a098ae6db5a9c3efb03111721528c8ef2aee3f262', 'f2ade73db1b99a5237ebc24828670a273e174ae4d45a7e7c18f9a608113822453b1613d9943d18ae', 0, '2018-07-12 21:49:43');
INSERT INTO `hu_oauth_refresh_tokens` VALUES ('deb57ff304524d428c8c3ef1c70083b3e563f8d8b13aa6d56c25584cb4bd08bd948db75dbcb71ca5', '128d66b2dd4a41a2bbb93e2fb964868a15a3d25c211c9d861d2bc6960e59d2571853e46e644d2740', 0, '2018-07-12 22:34:33');
INSERT INTO `hu_oauth_refresh_tokens` VALUES ('e0c3b9498272848c490eda6f8ab0202542232ba09d5d650d8d65dbf0b8e0b79d4c49b57ce7f12de7', 'fa518a5a5e57c173ff44a8c6223f3a60034a902c2b9c3155c3dc9ec60d0ccb1e7183777a27ccb7bb', 0, '2018-07-12 18:57:13');
INSERT INTO `hu_oauth_refresh_tokens` VALUES ('e50adadd746754af3ece47a680f9a7998a067ee687443acccbbf452a3aef83c2758952ad91f819bd', 'f952626e1e9916b04808ae330a76fc75a3ed875645bbe991bef1239c021559131065842de06e6b4b', 0, '2018-07-14 00:15:09');
INSERT INTO `hu_oauth_refresh_tokens` VALUES ('e5efbff928d3cfc765e287b2cce906aeb2e4bdd6079d92dcb30a7c1aaa0557775dfaf080d99ba7e1', 'f6dcd4909475fe70d101636b81ee6924cc92c353ade2c59062274d79fb816ea81d0811ac6c3fe4e0', 0, '2018-07-14 22:55:33');
INSERT INTO `hu_oauth_refresh_tokens` VALUES ('e789b49bdca14b203ec0b738ca7c0a2d01e28bed70c84bde7490f33e4143917d444a9801b89545bd', '4bd394bc35fcbe0257e9d3068699dfd92b06932eb86e4e6b71e01f89bf626a8c69193b087dae9a8e', 0, '2018-07-18 15:19:57');
INSERT INTO `hu_oauth_refresh_tokens` VALUES ('ea83e91368d31685a3961432e53a22a405bb35c85f1def53a77b7500e85354ce01f0ac758793145a', '1df17e85c492724e62e55838297d71ca10cfbc6466040571023e6477f31c82339c3f89d99c1d3117', 0, '2018-07-14 23:34:54');
INSERT INTO `hu_oauth_refresh_tokens` VALUES ('edf450c29a73f86566cd1ef7ee89acd386f28818a58a5b58f492a38218a2f7d0dcc19bfb49fff17b', '35042214a5e381d0112e4f21405f76403e43fedcf4975f0089206ce3f47c7dcabc0aefb82dc161a5', 0, '2018-07-12 21:18:53');
INSERT INTO `hu_oauth_refresh_tokens` VALUES ('ef95e844c76587889045d6d865114d011d20bc084f4308bf6e9db006d56d98949c07b502d4387367', 'cd9deced41836c28061cafb833b0d81b20838de7f588ae481c07062188dd433b20b9e4053655f3ff', 0, '2018-07-12 21:38:04');
INSERT INTO `hu_oauth_refresh_tokens` VALUES ('f1bdf4ed5c36947b7751c8cb9ef23802687c30b110cc7a749d0ef6791bd25ba1e891717c298f0fd4', '8f2b59b31f742264883907193952c8712d4e5a46f488dd124595566dae2dcb37fa0c80cd8288ed57', 0, '2018-07-12 18:57:05');
INSERT INTO `hu_oauth_refresh_tokens` VALUES ('f347c6536236cd163efcf49d3061eb9f5e8d119f23f969e95fbd30e3b0a5fbf8bfb9399dd7f4600a', 'ec591618c028862a572877feda485f8ce89e16f26c4f07e61d71eaffd4d5b162a999169334c1c791', 0, '2018-07-12 22:36:03');
INSERT INTO `hu_oauth_refresh_tokens` VALUES ('f9b4f918a20937d86ad8bba185f9274be038451bade66abf5ffdacbd0368d482a534fcff1d1df99c', 'd84a67e2828613a81bac0f762b43dfe0bae59132c46ce32bddab2dcb687e57f2f48949b63d22f901', 0, '2018-07-12 22:32:04');
INSERT INTO `hu_oauth_refresh_tokens` VALUES ('fde66ce7f2c564df16ab800cb64b3efedbf0fcd83a3e2f8365d9e8eced4a07d89f1e83c4fac12096', '53c84e59f5131448043f0fcc1608a6561eca013231081c6b8736477742e56099aadaa6d6071c350b', 0, '2018-09-26 17:16:36');
COMMIT;

-- ----------------------------
-- Table structure for hu_operation_logs
-- ----------------------------
DROP TABLE IF EXISTS `hu_operation_logs`;
CREATE TABLE `hu_operation_logs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(191) NOT NULL DEFAULT '',
  `user_model` varchar(191) NOT NULL DEFAULT '',
  `exec` varchar(191) NOT NULL DEFAULT '',
  `params` text NOT NULL,
  `response_data` text NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `operation_logs_user_id_index` (`user_id`),
  KEY `operation_logs_name_index` (`name`),
  KEY `operation_logs_exec_index` (`exec`)
) ENGINE=InnoDB AUTO_INCREMENT=65 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of hu_operation_logs
-- ----------------------------
BEGIN;
INSERT INTO `hu_operation_logs` VALUES (1, 5, 'king19800105', 'App\\Models\\Admin', 'GoodsController@index', '{\"a\":\"10\",\"b\":\"10\"}', '', '2018-05-24 13:22:51', '2018-05-24 13:22:51');
INSERT INTO `hu_operation_logs` VALUES (2, 5, 'king19800105', 'App\\Models\\Admin', 'AdminController@store', '{\"name\":\"anthoney11\",\"password\":\"111111\",\"email\":\"anthony12@163.com\",\"mobile\":\"13555555555\",\"password_confirmation\":\"111111\"}', '{\"code\":0,\"message\":\"success\",\"result\":{\"name\":\"anthoney11\",\"email\":\"anthony12@163.com\",\"mobile\":\"13555555555\",\"producer\":\"king19800105\",\"updated_at\":\"2018-05-25 18:18:36\",\"created_at\":\"2018-05-25 18:18:36\",\"id\":101}}', '2018-05-25 18:18:37', '2018-05-25 18:18:37');
INSERT INTO `hu_operation_logs` VALUES (3, 5, 'king19800105', 'App\\Models\\Admin', 'MessageController@sendInnerPointByType', '{\"type\":\"123\"}', '[]', '2018-06-01 11:45:10', '2018-06-01 11:45:10');
INSERT INTO `hu_operation_logs` VALUES (4, 5, 'king19800105', 'App\\Models\\Admin', 'MessageController@sendInsidePointByType', '{\"recipient_id\":\"12\",\"title\":\"sdfsfafsaf\",\"content\":\"sfafafaf\"}', '[]', '2018-06-06 15:28:48', '2018-06-06 15:28:48');
INSERT INTO `hu_operation_logs` VALUES (5, 5, 'king19800105', 'App\\Models\\Admin', 'MessageController@sendInsidePointByType', '{\"recipient_id\":\"12\",\"title\":\"sdfsfafsaf\",\"content\":\"sfafafaf\"}', '{\"code\":0,\"message\":\"success\",\"result\":{\"sender_id\":5,\"sender_model\":\"App\\\\Models\\\\Admin\",\"recipient_model\":\"App\\\\Models\\\\User\",\"recipient_id\":12,\"type\":\"2\",\"message_id\":3,\"updated_at\":\"2018-06-06 15:29:28\",\"created_at\":\"2018-06-06 15:29:28\",\"id\":3}}', '2018-06-06 15:29:28', '2018-06-06 15:29:28');
INSERT INTO `hu_operation_logs` VALUES (6, 5, 'king19800105', 'App\\Models\\Admin', 'MessageController@cancelInsideMessage', '{\"recipient_id\":\"12\",\"title\":\"sdfsfafsaf\",\"content\":\"sfafafaf\",\"id\":\"1\"}', '{\"code\":0,\"message\":\"success\",\"result\":true}', '2018-06-06 16:12:52', '2018-06-06 16:12:52');
INSERT INTO `hu_operation_logs` VALUES (7, 5, 'king19800105', 'App\\Models\\Admin', 'MessageController@sendInsidePoint', '{\"recipient_id\":\"12\",\"title\":\"sdfsfafsaf\",\"content\":\"sfafafaf\",\"contact_way\":\"13333333\",\"condition\":\"{}\"}', '{\"code\":0,\"message\":\"success\",\"result\":{\"sender_id\":5,\"sender_model\":\"App\\\\Models\\\\Admin\",\"recipient_model\":\"App\\\\Models\\\\User\",\"recipient_id\":12,\"type\":\"2\",\"message_id\":1,\"updated_at\":\"2018-06-06 18:00:53\",\"created_at\":\"2018-06-06 18:00:53\",\"id\":1}}', '2018-06-06 18:00:53', '2018-06-06 18:00:53');
INSERT INTO `hu_operation_logs` VALUES (8, 5, 'king19800105', 'App\\Models\\Admin', 'MessageController@readInsideMessage', '{\"recipient_id\":\"0\",\"title\":\"sdfsfafsaf\",\"content\":\"sfafafaf\",\"contact_way\":\"13333333\",\"condition\":\"{}\",\"id\":\"2\"}', '{\"code\":0,\"message\":\"success\",\"result\":true}', '2018-06-08 16:54:05', '2018-06-08 16:54:05');
INSERT INTO `hu_operation_logs` VALUES (9, 5, 'king19800105', 'App\\Models\\Admin', 'MessageController@sendInsideSystem', '{\"recipient_id\":\"0\",\"title\":\"sdfsfafsaf\",\"content\":\"sfafafaf\",\"contact_way\":\"13333333\",\"condition\":\"{}\"}', '{\"code\":0,\"message\":\"success\",\"result\":true}', '2018-06-27 17:34:53', '2018-06-27 17:34:53');
INSERT INTO `hu_operation_logs` VALUES (10, 5, 'king19800105', 'App\\Models\\Admin', 'ReviewTemplateController@store', '{\"title\":\"\\u7b2c\\u4e00\\u6b21\\u63d0\\u4ea4\",\"sign\":\"\\u8fc1\\u79fb\",\"content\":\"\\u77ed\\u4fe1\\u6765\\u4e86\"}', '{\"code\":0,\"message\":\"success\",\"result\":{\"title\":\"\\u7b2c\\u4e00\\u6b21\\u63d0\\u4ea4\",\"sign\":\"\\u8fc1\\u79fb\",\"content\":\"\\u77ed\\u4fe1\\u6765\\u4e86\",\"user_id\":5,\"updated_at\":\"2018-07-03 17:02:14\",\"created_at\":\"2018-07-03 17:02:14\",\"id\":1}}', '2018-07-03 17:02:14', '2018-07-03 17:02:14');
INSERT INTO `hu_operation_logs` VALUES (11, 5, 'king19800105', 'App\\Models\\Admin', 'ReviewTemplateController@store', '{\"title\":\"\\u7b2c\\u4e00\\u6b21\\u63d0\\u4ea44\",\"sign\":\"\\u8fc1\\u79fb\",\"content\":\"\\u77ed\\u4fe1\\u6765\\u4e86\"}', '{\"code\":0,\"message\":\"success\",\"result\":{\"title\":\"\\u7b2c\\u4e00\\u6b21\\u63d0\\u4ea44\",\"sign\":\"\\u8fc1\\u79fb\",\"content\":\"\\u77ed\\u4fe1\\u6765\\u4e86\",\"user_id\":5,\"updated_at\":\"2018-07-03 17:14:32\",\"created_at\":\"2018-07-03 17:14:32\",\"id\":4}}', '2018-07-03 17:14:32', '2018-07-03 17:14:32');
INSERT INTO `hu_operation_logs` VALUES (12, 5, 'king19800105', 'App\\Models\\Admin', 'ReviewTemplateController@store', '{\"title\":\"\\u7b2c\\u4e00\\u6b21\\u63d0\\u4ea46\",\"sign\":\"\\u8fc1\\u79fb\",\"content\":\"\\u77ed\\u4fe1\\u6765\\u4e86\"}', '{\"code\":0,\"message\":\"success\",\"result\":{\"title\":\"\\u7b2c\\u4e00\\u6b21\\u63d0\\u4ea46\",\"sign\":\"\\u8fc1\\u79fb\",\"content\":\"\\u77ed\\u4fe1\\u6765\\u4e86\",\"user_id\":5,\"updated_at\":\"2018-07-03 17:38:10\",\"created_at\":\"2018-07-03 17:38:10\",\"id\":5}}', '2018-07-03 17:38:10', '2018-07-03 17:38:10');
INSERT INTO `hu_operation_logs` VALUES (13, 5, 'king19800105', 'App\\Models\\Admin', 'ReviewTemplateController@store', '{\"title\":\"\\u7b2c\\u4e00\\u6b21\\u63d0\\u4ea43\",\"sign\":\"\\u8fc1\\u79fb\",\"content\":\"\\u77ed\\u4fe1\\u6765\\u4e86\"}', '{\"code\":0,\"message\":\"success\",\"result\":{\"title\":\"\\u7b2c\\u4e00\\u6b21\\u63d0\\u4ea43\",\"sign\":\"\\u8fc1\\u79fb\",\"content\":\"\\u77ed\\u4fe1\\u6765\\u4e86\",\"user_id\":5,\"updated_at\":\"2018-07-03 17:38:43\",\"created_at\":\"2018-07-03 17:38:43\",\"id\":6}}', '2018-07-03 17:38:43', '2018-07-03 17:38:43');
INSERT INTO `hu_operation_logs` VALUES (14, 5, 'king19800105', 'App\\Models\\Admin', 'ReviewTemplateController@store', '{\"title\":\"\\u7b2c\\u4e00\\u6b21\\u63d0\\u4ea410000\",\"sign\":\"\\u8fc1\\u79fb\",\"content\":\"\\u77ed\\u4fe1\\u6765\\u4e86\"}', '{\"code\":0,\"message\":\"success\",\"result\":{\"title\":\"\\u7b2c\\u4e00\\u6b21\\u63d0\\u4ea410000\",\"sign\":\"\\u8fc1\\u79fb\",\"content\":\"\\u77ed\\u4fe1\\u6765\\u4e86\",\"user_id\":5,\"updated_at\":\"2018-07-04 12:48:17\",\"created_at\":\"2018-07-04 12:48:17\",\"id\":7}}', '2018-07-04 12:48:17', '2018-07-04 12:48:17');
INSERT INTO `hu_operation_logs` VALUES (15, 5, 'king19800105', 'App\\Models\\Admin', 'ReviewTemplateController@destroy', '{\"title\":\"\\u7b2c\\u4e00\\u6b21\\u63d0\\u4ea410000\",\"sign\":\"\\u8fc1\\u79fb\",\"content\":\"\\u77ed\\u4fe1\\u6765\\u4e86\",\"review_tpl\":\"2\"}', '{\"code\":0,\"message\":\"success\",\"result\":true}', '2018-07-04 12:52:02', '2018-07-04 12:52:02');
INSERT INTO `hu_operation_logs` VALUES (16, 5, 'king19800105', 'App\\Models\\Admin', 'ReviewTemplateController@update', '{\"title\":\"\\u7b2c\\u4e00\\u6b21\\u63d0\\u4ea43\",\"sign\":\"\\u8fc1\\u79fb\",\"content\":\"\\u77ed\\u4fe1\\u6765\\u4e86\",\"id\":\"7\",\"review_tpl\":\"7\"}', '{\"code\":0,\"message\":\"success\",\"result\":true}', '2018-07-04 13:23:38', '2018-07-04 13:23:38');
INSERT INTO `hu_operation_logs` VALUES (17, 5, 'king19800105', 'App\\Models\\Admin', 'ReviewTemplateController@update', '{\"title\":\"\\u7b2c\\u4e00\\u6b21\\u63d0\\u4ea43\",\"sign\":\"\\u8fc1\\u79fb\",\"content\":\"\\u77ed\\u4fe1\\u6765\\u4e86\",\"id\":\"7\",\"review_tpl\":\"7\"}', '{\"code\":0,\"message\":\"success\",\"result\":true}', '2018-07-04 13:23:44', '2018-07-04 13:23:44');
INSERT INTO `hu_operation_logs` VALUES (18, 5, 'king19800105', 'App\\Models\\Admin', 'ReviewTemplateController@update', '{\"title\":\"\\u7b2c\\u4e00\\u6b21\\u63d0\\u4ea43\",\"sign\":\"\\u8fc1\\u79fb\",\"content\":\"\\u77ed\\u4fe1\\u6765\\u4e86\",\"id\":\"7\",\"review_tpl\":\"7\"}', '{\"code\":0,\"message\":\"success\",\"result\":true}', '2018-07-04 13:24:06', '2018-07-04 13:24:06');
INSERT INTO `hu_operation_logs` VALUES (19, 5, 'king19800105', 'App\\Models\\Admin', 'ReviewTemplateController@update', '{\"title\":\"\\u7b2c\\u4e00\\u6b21\\u63d0\\u4ea43\",\"sign\":\"\\u8fc1\\u79fb\",\"content\":\"\\u77ed\\u4fe1\\u6765\\u4e86\",\"id\":\"7\",\"review_tpl\":\"7\"}', '{\"code\":0,\"message\":\"success\",\"result\":true}', '2018-07-04 13:51:58', '2018-07-04 13:51:58');
INSERT INTO `hu_operation_logs` VALUES (20, 5, 'king19800105', 'App\\Models\\Admin', 'ReviewTemplateController@update', '{\"title\":\"\\u7b2c\\u4e00\\u6b21\\u63d0\\u4ea43\",\"sign\":\"\\u8fc1\\u79fb\",\"content\":\"\\u77ed\\u4fe1\\u6765\\u4e86\",\"id\":\"7\",\"review_tpl\":\"7\"}', '{\"code\":0,\"message\":\"success\",\"result\":true}', '2018-07-04 13:52:21', '2018-07-04 13:52:21');
INSERT INTO `hu_operation_logs` VALUES (21, 5, 'king19800105', 'App\\Models\\Admin', 'ReviewTemplateController@update', '{\"title\":\"\\u7b2c\\u4e00\\u6b21\\u63d0\\u4ea43\",\"sign\":\"\\u8fc1\\u79fb\",\"content\":\"\\u77ed\\u4fe1\\u6765\\u4e86\",\"id\":\"7\",\"review_tpl\":\"7\"}', '{\"code\":0,\"message\":\"success\",\"result\":true}', '2018-07-04 13:52:35', '2018-07-04 13:52:35');
INSERT INTO `hu_operation_logs` VALUES (22, 5, 'king19800105', 'App\\Models\\Admin', 'ReviewTemplateController@update', '{\"title\":\"\\u7b2c\\u4e00\\u6b21\\u63d0\\u4ea43\",\"sign\":\"\\u8fc1\\u79fb\",\"content\":\"\\u77ed\\u4fe1\\u6765\\u4e86\",\"id\":\"7\",\"review_tpl\":\"7\"}', '{\"code\":0,\"message\":\"success\",\"result\":true}', '2018-07-04 13:54:24', '2018-07-04 13:54:24');
INSERT INTO `hu_operation_logs` VALUES (23, 5, 'king19800105', 'App\\Models\\Admin', 'ReviewTemplateController@verifyTemplate', '{\"title\":\"\\u7b2c\\u4e00\\u6b21\\u63d0\\u4ea4233\",\"sign\":\"\\u8fc1\\u79fb\",\"content\":\"\\u77ed\\u4fe1\\u6765\\u4e86\",\"id\":\"1\",\"user_id\":\"5\",\"state\":\"1\",\"type\":\"1\",\"level\":\"20\"}', '{\"code\":0,\"message\":\"success\",\"result\":true}', '2018-07-04 15:37:57', '2018-07-04 15:37:57');
INSERT INTO `hu_operation_logs` VALUES (24, 5, 'king19800105', 'App\\Models\\Admin', 'ReviewTemplateController@verifyTemplate', '{\"title\":\"\\u7b2c\\u4e00\\u6b21\\u63d0\\u4ea4233\",\"sign\":\"\\u8fc1\\u79fb\",\"content\":\"\\u77ed\\u4fe1\\u6765\\u4e86\",\"id\":\"1\",\"user_id\":\"5\",\"state\":\"1\",\"type\":\"1\",\"level\":\"20\"}', '{\"code\":0,\"message\":\"success\",\"result\":true}', '2018-07-04 15:39:32', '2018-07-04 15:39:32');
INSERT INTO `hu_operation_logs` VALUES (25, 5, 'king19800105', 'App\\Models\\Admin', 'ReviewTemplateController@verifyTemplate', '{\"title\":\"\\u7b2c\\u4e00\\u6b21\\u63d0\\u4ea4233\",\"sign\":\"\\u8fc1\\u79fb\",\"content\":\"\\u77ed\\u4fe1\\u6765\\u4e86\",\"id\":\"1\",\"user_id\":\"5\",\"state\":\"1\",\"type\":\"2\",\"level\":\"20\"}', '{\"code\":0,\"message\":\"success\",\"result\":true}', '2018-07-04 15:49:49', '2018-07-04 15:49:49');
INSERT INTO `hu_operation_logs` VALUES (26, 5, 'king19800105', 'App\\Models\\Admin', 'PermissionController@store', '{\"name\":\"super\",\"show_name\":\"\\u8d85\\u7ea7\\u7ba1\\u7406\\u5458\"}', '{\"code\":0,\"message\":\"success\",\"result\":{\"name\":\"super\",\"show_name\":\"\\u8d85\\u7ea7\\u7ba1\\u7406\\u5458\",\"guard_name\":\"api_admin\",\"updated_at\":\"2018-07-29 17:45:43\",\"created_at\":\"2018-07-29 17:45:43\",\"id\":1}}', '2018-07-29 17:45:44', '2018-07-29 17:45:44');
INSERT INTO `hu_operation_logs` VALUES (27, 5, 'king19800105', 'App\\Models\\Admin', 'PermissionController@store', '{\"name\":\"admin@update\",\"show_name\":\"\\u8d85\\u7ea7\\u7ba1\\u7406\\u5458\"}', '{\"code\":0,\"message\":\"success\",\"result\":{\"name\":\"Admin@update\",\"show_name\":\"\\u8d85\\u7ea7\\u7ba1\\u7406\\u5458\",\"guard_name\":\"api_admin\",\"module\":\"Admin\",\"updated_at\":\"2018-07-29 18:24:05\",\"created_at\":\"2018-07-29 18:24:05\",\"id\":3}}', '2018-07-29 18:24:05', '2018-07-29 18:24:05');
INSERT INTO `hu_operation_logs` VALUES (28, 5, 'king19800105', 'App\\Models\\Admin', 'PermissionController@store', '{\"name\":\"admin@store\",\"show_name\":\"\\u7ba1\\u7406\\u5458\\u6dfb\\u52a0\"}', '{\"code\":0,\"message\":\"success\",\"result\":{\"name\":\"Admin@store\",\"show_name\":\"\\u7ba1\\u7406\\u5458\\u6dfb\\u52a0\",\"guard_name\":\"api_admin\",\"module\":\"Admin\",\"updated_at\":\"2018-07-29 18:26:23\",\"created_at\":\"2018-07-29 18:26:23\",\"id\":4}}', '2018-07-29 18:26:23', '2018-07-29 18:26:23');
INSERT INTO `hu_operation_logs` VALUES (29, 5, 'king19800105', 'App\\Models\\Admin', 'PermissionController@store', '{\"name\":\"admin@destroy\",\"show_name\":\"\\u7ba1\\u7406\\u5458\\u5220\\u9664\"}', '{\"code\":0,\"message\":\"success\",\"result\":{\"name\":\"Admin@destroy\",\"show_name\":\"\\u7ba1\\u7406\\u5458\\u5220\\u9664\",\"guard_name\":\"api_admin\",\"module\":\"Admin\",\"updated_at\":\"2018-07-29 18:26:50\",\"created_at\":\"2018-07-29 18:26:50\",\"id\":5}}', '2018-07-29 18:26:50', '2018-07-29 18:26:50');
INSERT INTO `hu_operation_logs` VALUES (30, 5, 'king19800105', 'App\\Models\\Admin', 'PermissionController@store', '{\"name\":\"admin@currentAdminInfo\",\"show_name\":\"\\u7ba1\\u7406\\u5458\\u4fe1\\u606f\\u8be6\\u60c5\"}', '{\"code\":0,\"message\":\"success\",\"result\":{\"name\":\"Admin@currentAdminInfo\",\"show_name\":\"\\u7ba1\\u7406\\u5458\\u4fe1\\u606f\\u8be6\\u60c5\",\"guard_name\":\"api_admin\",\"module\":\"Admin\",\"updated_at\":\"2018-07-29 18:30:08\",\"created_at\":\"2018-07-29 18:30:08\",\"id\":6}}', '2018-07-29 18:30:08', '2018-07-29 18:30:08');
INSERT INTO `hu_operation_logs` VALUES (31, 5, 'king19800105', 'App\\Models\\Admin', 'PermissionController@store', '{\"name\":\"menu@index\",\"show_name\":\"\\u83dc\\u5355\\u5217\\u8868\"}', '{\"code\":0,\"message\":\"success\",\"result\":{\"name\":\"Menu@index\",\"show_name\":\"\\u83dc\\u5355\\u5217\\u8868\",\"guard_name\":\"api_admin\",\"module\":\"Menu\",\"updated_at\":\"2018-07-29 18:32:17\",\"created_at\":\"2018-07-29 18:32:17\",\"id\":7}}', '2018-07-29 18:32:18', '2018-07-29 18:32:18');
INSERT INTO `hu_operation_logs` VALUES (32, 5, 'king19800105', 'App\\Models\\Admin', 'PermissionController@store', '{\"name\":\"menu@show\",\"show_name\":\"\\u83dc\\u5355\\u8be6\\u60c5\"}', '{\"code\":0,\"message\":\"success\",\"result\":{\"name\":\"Menu@show\",\"show_name\":\"\\u83dc\\u5355\\u8be6\\u60c5\",\"guard_name\":\"api_admin\",\"module\":\"Menu\",\"updated_at\":\"2018-07-29 18:32:37\",\"created_at\":\"2018-07-29 18:32:37\",\"id\":9}}', '2018-07-29 18:32:38', '2018-07-29 18:32:38');
INSERT INTO `hu_operation_logs` VALUES (33, 5, 'king19800105', 'App\\Models\\Admin', 'PermissionController@store', '{\"name\":\"menu@update\",\"show_name\":\"\\u83dc\\u5355\\u4fee\\u6539\"}', '{\"code\":0,\"message\":\"success\",\"result\":{\"name\":\"Menu@update\",\"show_name\":\"\\u83dc\\u5355\\u4fee\\u6539\",\"guard_name\":\"api_admin\",\"module\":\"Menu\",\"updated_at\":\"2018-07-29 18:32:52\",\"created_at\":\"2018-07-29 18:32:52\",\"id\":10}}', '2018-07-29 18:32:52', '2018-07-29 18:32:52');
INSERT INTO `hu_operation_logs` VALUES (34, 5, 'king19800105', 'App\\Models\\Admin', 'PermissionController@store', '{\"name\":\"menu@store\",\"show_name\":\"\\u83dc\\u5355\\u6dfb\\u52a0\"}', '{\"code\":0,\"message\":\"success\",\"result\":{\"name\":\"Menu@store\",\"show_name\":\"\\u83dc\\u5355\\u6dfb\\u52a0\",\"guard_name\":\"api_admin\",\"module\":\"Menu\",\"updated_at\":\"2018-07-29 18:33:06\",\"created_at\":\"2018-07-29 18:33:06\",\"id\":11}}', '2018-07-29 18:33:06', '2018-07-29 18:33:06');
INSERT INTO `hu_operation_logs` VALUES (35, 5, 'king19800105', 'App\\Models\\Admin', 'PermissionController@store', '{\"name\":\"menu@destroy\",\"show_name\":\"\\u83dc\\u5355\\u5220\\u9664\"}', '{\"code\":0,\"message\":\"success\",\"result\":{\"name\":\"Menu@destroy\",\"show_name\":\"\\u83dc\\u5355\\u5220\\u9664\",\"guard_name\":\"api_admin\",\"module\":\"Menu\",\"updated_at\":\"2018-07-29 18:33:17\",\"created_at\":\"2018-07-29 18:33:17\",\"id\":12}}', '2018-07-29 18:33:18', '2018-07-29 18:33:18');
INSERT INTO `hu_operation_logs` VALUES (36, 5, 'king19800105', 'App\\Models\\Admin', 'PermissionController@store', '{\"name\":\"role@index\",\"show_name\":\"\\u89d2\\u8272\\u5217\\u8868\"}', '{\"code\":0,\"message\":\"success\",\"result\":{\"name\":\"Role@index\",\"show_name\":\"\\u89d2\\u8272\\u5217\\u8868\",\"guard_name\":\"api_admin\",\"module\":\"Role\",\"updated_at\":\"2018-07-29 18:34:05\",\"created_at\":\"2018-07-29 18:34:05\",\"id\":13}}', '2018-07-29 18:34:05', '2018-07-29 18:34:05');
INSERT INTO `hu_operation_logs` VALUES (37, 5, 'king19800105', 'App\\Models\\Admin', 'PermissionController@store', '{\"name\":\"role@show\",\"show_name\":\"\\u89d2\\u8272\\u8be6\\u60c5\"}', '{\"code\":0,\"message\":\"success\",\"result\":{\"name\":\"Role@show\",\"show_name\":\"\\u89d2\\u8272\\u8be6\\u60c5\",\"guard_name\":\"api_admin\",\"module\":\"Role\",\"updated_at\":\"2018-07-29 18:34:14\",\"created_at\":\"2018-07-29 18:34:14\",\"id\":14}}', '2018-07-29 18:34:14', '2018-07-29 18:34:14');
INSERT INTO `hu_operation_logs` VALUES (38, 5, 'king19800105', 'App\\Models\\Admin', 'PermissionController@store', '{\"name\":\"role@update\",\"show_name\":\"\\u89d2\\u8272\\u4fee\\u6539\"}', '{\"code\":0,\"message\":\"success\",\"result\":{\"name\":\"Role@update\",\"show_name\":\"\\u89d2\\u8272\\u4fee\\u6539\",\"guard_name\":\"api_admin\",\"module\":\"Role\",\"updated_at\":\"2018-07-29 18:34:25\",\"created_at\":\"2018-07-29 18:34:25\",\"id\":15}}', '2018-07-29 18:34:25', '2018-07-29 18:34:25');
INSERT INTO `hu_operation_logs` VALUES (39, 5, 'king19800105', 'App\\Models\\Admin', 'PermissionController@store', '{\"name\":\"role@store\",\"show_name\":\"\\u89d2\\u8272\\u6dfb\\u52a0\"}', '{\"code\":0,\"message\":\"success\",\"result\":{\"name\":\"Role@store\",\"show_name\":\"\\u89d2\\u8272\\u6dfb\\u52a0\",\"guard_name\":\"api_admin\",\"module\":\"Role\",\"updated_at\":\"2018-07-29 18:34:38\",\"created_at\":\"2018-07-29 18:34:38\",\"id\":16}}', '2018-07-29 18:34:38', '2018-07-29 18:34:38');
INSERT INTO `hu_operation_logs` VALUES (40, 5, 'king19800105', 'App\\Models\\Admin', 'PermissionController@store', '{\"name\":\"role@destroy\",\"show_name\":\"\\u89d2\\u8272\\u5220\\u9664\"}', '{\"code\":0,\"message\":\"success\",\"result\":{\"name\":\"Role@destroy\",\"show_name\":\"\\u89d2\\u8272\\u5220\\u9664\",\"guard_name\":\"api_admin\",\"module\":\"Role\",\"updated_at\":\"2018-07-29 18:34:49\",\"created_at\":\"2018-07-29 18:34:49\",\"id\":17}}', '2018-07-29 18:34:49', '2018-07-29 18:34:49');
INSERT INTO `hu_operation_logs` VALUES (41, 5, 'king19800105', 'App\\Models\\Admin', 'PermissionController@store', '{\"name\":\"role@assignPermissions\",\"show_name\":\"\\u89d2\\u8272\\u5206\\u914d\\u6743\\u9650\"}', '{\"code\":0,\"message\":\"success\",\"result\":{\"name\":\"Role@assignPermissions\",\"show_name\":\"\\u89d2\\u8272\\u5206\\u914d\\u6743\\u9650\",\"guard_name\":\"api_admin\",\"module\":\"Role\",\"updated_at\":\"2018-07-29 18:35:44\",\"created_at\":\"2018-07-29 18:35:44\",\"id\":18}}', '2018-07-29 18:35:44', '2018-07-29 18:35:44');
INSERT INTO `hu_operation_logs` VALUES (42, 5, 'king19800105', 'App\\Models\\Admin', 'PermissionController@store', '{\"name\":\"admin@assign\",\"show_name\":\"\\u7ba1\\u7406\\u5458\\u5206\\u914d\\u89d2\\u8272\"}', '{\"code\":0,\"message\":\"success\",\"result\":{\"name\":\"Admin@assign\",\"show_name\":\"\\u7ba1\\u7406\\u5458\\u5206\\u914d\\u89d2\\u8272\",\"guard_name\":\"api_admin\",\"module\":\"Admin\",\"updated_at\":\"2018-07-29 18:36:34\",\"created_at\":\"2018-07-29 18:36:34\",\"id\":19}}', '2018-07-29 18:36:35', '2018-07-29 18:36:35');
INSERT INTO `hu_operation_logs` VALUES (43, 5, 'king19800105', 'App\\Models\\Admin', 'PermissionController@store', '{\"name\":\"permission@index\",\"show_name\":\"\\u6743\\u9650\\u8282\\u70b9\\u5217\\u8868\"}', '{\"code\":0,\"message\":\"success\",\"result\":{\"name\":\"Permission@index\",\"show_name\":\"\\u6743\\u9650\\u8282\\u70b9\\u5217\\u8868\",\"guard_name\":\"api_admin\",\"module\":\"Permission\",\"updated_at\":\"2018-07-29 18:37:39\",\"created_at\":\"2018-07-29 18:37:39\",\"id\":20}}', '2018-07-29 18:37:40', '2018-07-29 18:37:40');
INSERT INTO `hu_operation_logs` VALUES (44, 5, 'king19800105', 'App\\Models\\Admin', 'PermissionController@store', '{\"name\":\"permission@show\",\"show_name\":\"\\u6743\\u9650\\u8282\\u70b9\\u8be6\\u60c5\"}', '{\"code\":0,\"message\":\"success\",\"result\":{\"name\":\"Permission@show\",\"show_name\":\"\\u6743\\u9650\\u8282\\u70b9\\u8be6\\u60c5\",\"guard_name\":\"api_admin\",\"module\":\"Permission\",\"updated_at\":\"2018-07-29 18:37:55\",\"created_at\":\"2018-07-29 18:37:55\",\"id\":21}}', '2018-07-29 18:37:56', '2018-07-29 18:37:56');
INSERT INTO `hu_operation_logs` VALUES (45, 5, 'king19800105', 'App\\Models\\Admin', 'PermissionController@store', '{\"name\":\"permission@store\",\"show_name\":\"\\u6743\\u9650\\u8282\\u70b9\\u6dfb\\u52a0\"}', '{\"code\":0,\"message\":\"success\",\"result\":{\"name\":\"Permission@store\",\"show_name\":\"\\u6743\\u9650\\u8282\\u70b9\\u6dfb\\u52a0\",\"guard_name\":\"api_admin\",\"module\":\"Permission\",\"updated_at\":\"2018-07-29 18:38:06\",\"created_at\":\"2018-07-29 18:38:06\",\"id\":22}}', '2018-07-29 18:38:06', '2018-07-29 18:38:06');
INSERT INTO `hu_operation_logs` VALUES (46, 5, 'king19800105', 'App\\Models\\Admin', 'PermissionController@store', '{\"name\":\"permission@update\",\"show_name\":\"\\u6743\\u9650\\u8282\\u70b9\\u4fee\\u6539\"}', '{\"code\":0,\"message\":\"success\",\"result\":{\"name\":\"Permission@update\",\"show_name\":\"\\u6743\\u9650\\u8282\\u70b9\\u4fee\\u6539\",\"guard_name\":\"api_admin\",\"module\":\"Permission\",\"updated_at\":\"2018-07-29 18:38:15\",\"created_at\":\"2018-07-29 18:38:15\",\"id\":23}}', '2018-07-29 18:38:15', '2018-07-29 18:38:15');
INSERT INTO `hu_operation_logs` VALUES (47, 5, 'king19800105', 'App\\Models\\Admin', 'PermissionController@store', '{\"name\":\"permission@destroy\",\"show_name\":\"\\u6743\\u9650\\u8282\\u70b9\\u5220\\u9664\"}', '{\"code\":0,\"message\":\"success\",\"result\":{\"name\":\"Permission@destroy\",\"show_name\":\"\\u6743\\u9650\\u8282\\u70b9\\u5220\\u9664\",\"guard_name\":\"api_admin\",\"module\":\"Permission\",\"updated_at\":\"2018-07-29 18:38:34\",\"created_at\":\"2018-07-29 18:38:34\",\"id\":24}}', '2018-07-29 18:38:34', '2018-07-29 18:38:34');
INSERT INTO `hu_operation_logs` VALUES (48, 5, 'king19800105', 'App\\Models\\Admin', 'RoleController@store', '{\"name\":\"super_admin\",\"show_name\":\"\\u540e\\u53f0\\u8d85\\u7ea7\\u7ba1\\u7406\\u5458\"}', '{\"code\":0,\"message\":\"success\",\"result\":{\"name\":\"super_admin\",\"show_name\":\"\\u540e\\u53f0\\u8d85\\u7ea7\\u7ba1\\u7406\\u5458\",\"guard_name\":\"api_admin\",\"updated_at\":\"2018-07-29 18:43:01\",\"created_at\":\"2018-07-29 18:43:01\",\"id\":1}}', '2018-07-29 18:43:01', '2018-07-29 18:43:01');
INSERT INTO `hu_operation_logs` VALUES (49, 5, 'king19800105', 'App\\Models\\Admin', 'RoleController@store', '{\"name\":\"admin\",\"show_name\":\"\\u540e\\u53f0\\u7ba1\\u7406\\u5458\"}', '{\"code\":0,\"message\":\"success\",\"result\":{\"name\":\"admin\",\"show_name\":\"\\u540e\\u53f0\\u7ba1\\u7406\\u5458\",\"guard_name\":\"api_admin\",\"updated_at\":\"2018-07-29 18:51:36\",\"created_at\":\"2018-07-29 18:51:36\",\"id\":2}}', '2018-07-29 18:51:36', '2018-07-29 18:51:36');
INSERT INTO `hu_operation_logs` VALUES (50, 5, 'king19800105', 'App\\Models\\Admin', 'RoleController@assign', '{\"id\":\"2\",\"permission_ids\":\"[1,2,3]\"}', '{\"code\":0,\"message\":\"success\",\"result\":{\"id\":2,\"name\":\"admin\",\"guard_name\":\"api_admin\",\"show_name\":\"\\u540e\\u53f0\\u7ba1\\u7406\\u5458\",\"created_at\":\"2018-07-29 18:51:36\",\"updated_at\":\"2018-07-29 18:51:36\"}}', '2018-07-29 19:16:30', '2018-07-29 19:16:30');
INSERT INTO `hu_operation_logs` VALUES (51, 5, 'king19800105', 'App\\Models\\Admin', 'RoleController@assign', '{\"id\":\"2\",\"permission_ids\":\"[1,2,3]\"}', '{\"code\":0,\"message\":\"success\",\"result\":{\"id\":2,\"name\":\"admin\",\"guard_name\":\"api_admin\",\"show_name\":\"\\u540e\\u53f0\\u7ba1\\u7406\\u5458\",\"created_at\":\"2018-07-29 18:51:36\",\"updated_at\":\"2018-07-29 18:51:36\"}}', '2018-07-29 19:16:56', '2018-07-29 19:16:56');
INSERT INTO `hu_operation_logs` VALUES (52, 5, 'king19800105', 'App\\Models\\Admin', 'RoleController@assign', '{\"id\":\"2\",\"permission_ids\":\"[6,7,9,10,11,12,13,14,20,21]\"}', '{\"code\":0,\"message\":\"success\",\"result\":{\"id\":2,\"name\":\"admin\",\"guard_name\":\"api_admin\",\"show_name\":\"\\u540e\\u53f0\\u7ba1\\u7406\\u5458\",\"created_at\":\"2018-07-29 18:51:36\",\"updated_at\":\"2018-07-29 18:51:36\"}}', '2018-07-29 19:23:16', '2018-07-29 19:23:16');
INSERT INTO `hu_operation_logs` VALUES (53, 5, 'king19800105', 'App\\Models\\Admin', 'RoleController@assign', '{\"id\":\"2\",\"permission_ids\":\"[6,7,9,10,11,12,13,14,20,21]\"}', '{\"code\":0,\"message\":\"success\",\"result\":{\"id\":2,\"name\":\"admin\",\"guard_name\":\"api_admin\",\"show_name\":\"\\u540e\\u53f0\\u7ba1\\u7406\\u5458\",\"created_at\":\"2018-07-29 18:51:36\",\"updated_at\":\"2018-07-29 18:51:36\"}}', '2018-07-29 20:21:51', '2018-07-29 20:21:51');
INSERT INTO `hu_operation_logs` VALUES (54, 5, 'king19800105', 'App\\Models\\Admin', 'RoleController@assign', '{\"id\":\"2\",\"permission_ids\":\"[1,2,3,6,7,9,10,11,12,13,14,20,21]\"}', '{\"code\":0,\"message\":\"success\",\"result\":{\"id\":2,\"name\":\"admin\",\"guard_name\":\"api_admin\",\"show_name\":\"\\u540e\\u53f0\\u7ba1\\u7406\\u5458\",\"created_at\":\"2018-07-29 18:51:36\",\"updated_at\":\"2018-07-29 18:51:36\"}}', '2018-07-29 20:22:53', '2018-07-29 20:22:53');
INSERT INTO `hu_operation_logs` VALUES (55, 5, 'king19800105', 'App\\Models\\Admin', 'AdminController@assign', '{\"id\":\"5\",\"id_or_ids\":\"[1]\"}', '{\"code\":0,\"message\":\"success\",\"result\":{\"id\":5,\"name\":\"king19800105\",\"email\":\"king19800105@163.com\",\"mobile\":\"13564535304\",\"producer\":\"casper.schuster\",\"remark\":\"Tenetur rerum nesciunt nostrum minima qui explicabo soluta.\",\"state\":1,\"last_logged_ip\":\"172.19.0.1\",\"remember_token\":null,\"deleted_at\":null,\"created_at\":\"2018-05-13 01:24:55\",\"updated_at\":\"2018-07-28 16:46:28\"}}', '2018-07-29 20:31:18', '2018-07-29 20:31:18');
INSERT INTO `hu_operation_logs` VALUES (56, 5, 'king19800105', 'App\\Models\\Admin', 'PermissionController@store', '{\"name\":\"admin@resetPassword\",\"show_name\":\"\\u7ba1\\u7406\\u5458\\u5bc6\\u7801\\u4fee\\u6539\"}', '{\"code\":0,\"message\":\"success\",\"result\":{\"name\":\"Admin@resetPassword\",\"show_name\":\"\\u7ba1\\u7406\\u5458\\u5bc6\\u7801\\u4fee\\u6539\",\"guard_name\":\"api_admin\",\"module\":\"Admin\",\"updated_at\":\"2018-07-29 20:44:31\",\"created_at\":\"2018-07-29 20:44:31\",\"id\":25}}', '2018-07-29 20:44:31', '2018-07-29 20:44:31');
INSERT INTO `hu_operation_logs` VALUES (57, 5, 'king19800105', 'App\\Models\\Admin', 'MenuController@store', '{\"name\":\"admin@resetPassword\",\"show_name\":\"\\u7ba1\\u7406\\u5458\\u5bc6\\u7801\\u4fee\\u6539\"}', '{\"code\":0,\"message\":\"success\",\"result\":{\"name\":\"admin@resetPassword\",\"updated_at\":\"2018-07-29 21:19:10\",\"created_at\":\"2018-07-29 21:19:10\",\"id\":1}}', '2018-07-29 21:19:11', '2018-07-29 21:19:11');
INSERT INTO `hu_operation_logs` VALUES (58, 5, 'king19800105', 'App\\Models\\Admin', 'MenuController@store', '{\"name\":\"admin@resetPassword\",\"show_name\":\"\\u7ba1\\u7406\\u5458\\u5bc6\\u7801\\u4fee\\u6539\"}', '{\"code\":0,\"message\":\"success\",\"result\":{\"name\":\"admin@resetPassword\",\"updated_at\":\"2018-07-29 21:19:20\",\"created_at\":\"2018-07-29 21:19:20\",\"id\":2}}', '2018-07-29 21:19:20', '2018-07-29 21:19:20');
INSERT INTO `hu_operation_logs` VALUES (59, 5, 'king19800105', 'App\\Models\\Admin', 'MenuController@store', '{\"name\":\"\\u7ba1\\u7406\\u5458\\u6a21\\u5757\",\"slug\":\"Admin@index\",\"parent_id\":\"0\",\"url\":\"admin\",\"url_alias\":\"admin.index\",\"weight\":\"1\",\"icon\":\"aaa\",\"description\":\"\\u7ba1\\u7406\\u5458\\u9876\\u7ea7\\u5bfc\\u822a\"}', '{\"code\":0,\"message\":\"success\",\"result\":{\"name\":\"\\u7ba1\\u7406\\u5458\\u6a21\\u5757\",\"slug\":\"Admin@index\",\"parent_id\":\"0\",\"url\":\"admin\",\"url_alias\":\"admin.index\",\"weight\":\"1\",\"icon\":\"aaa\",\"description\":\"\\u7ba1\\u7406\\u5458\\u9876\\u7ea7\\u5bfc\\u822a\",\"updated_at\":\"2018-07-29 21:28:59\",\"created_at\":\"2018-07-29 21:28:59\",\"id\":1}}', '2018-07-29 21:29:00', '2018-07-29 21:29:00');
INSERT INTO `hu_operation_logs` VALUES (60, 5, 'king19800105', 'App\\Models\\Admin', 'MenuController@store', '{\"name\":\"\\u7ba1\\u7406\\u5458\\u5217\\u8868\",\"slug\":\"Admin@index\",\"parent_id\":\"1\",\"url\":\"admin\",\"url_alias\":\"admin.index\",\"weight\":\"1\",\"icon\":\"aaa\",\"description\":\"\\u7ba1\\u7406\\u5458\\u9876\\u7ea7\\u5bfc\\u822a\"}', '{\"code\":0,\"message\":\"success\",\"result\":{\"name\":\"\\u7ba1\\u7406\\u5458\\u5217\\u8868\",\"slug\":\"Admin@index\",\"parent_id\":\"1\",\"url\":\"admin\",\"url_alias\":\"admin.index\",\"weight\":\"1\",\"icon\":\"aaa\",\"description\":\"\\u7ba1\\u7406\\u5458\\u9876\\u7ea7\\u5bfc\\u822a\",\"updated_at\":\"2018-07-29 21:30:06\",\"created_at\":\"2018-07-29 21:30:06\",\"id\":2}}', '2018-07-29 21:30:06', '2018-07-29 21:30:06');
INSERT INTO `hu_operation_logs` VALUES (61, 5, 'king19800105', 'App\\Models\\Admin', 'MenuController@store', '{\"name\":\"\\u6743\\u9650\\u5217\\u8868\",\"slug\":\"Permission@index\",\"parent_id\":\"0\",\"weight\":\"1\",\"icon\":\"bbb\"}', '{\"code\":0,\"message\":\"success\",\"result\":{\"name\":\"\\u6743\\u9650\\u5217\\u8868\",\"slug\":\"Permission@index\",\"parent_id\":\"0\",\"weight\":\"1\",\"icon\":\"bbb\",\"updated_at\":\"2018-07-29 21:33:40\",\"created_at\":\"2018-07-29 21:33:40\",\"id\":3}}', '2018-07-29 21:33:40', '2018-07-29 21:33:40');
INSERT INTO `hu_operation_logs` VALUES (62, 5, 'king19800105', 'App\\Models\\Admin', 'MenuController@store', '{\"name\":\"\\u89d2\\u8272\\u5217\\u8868\",\"slug\":\"Role@index\",\"parent_id\":\"1\",\"url\":\"role\",\"url_alias\":\"role.index\",\"weight\":\"6\",\"icon\":\"bbb\"}', '{\"code\":0,\"message\":\"success\",\"result\":{\"name\":\"\\u89d2\\u8272\\u5217\\u8868\",\"slug\":\"Role@index\",\"parent_id\":\"1\",\"url\":\"role\",\"url_alias\":\"role.index\",\"weight\":\"6\",\"icon\":\"bbb\",\"updated_at\":\"2018-07-29 21:37:01\",\"created_at\":\"2018-07-29 21:37:01\",\"id\":4}}', '2018-07-29 21:37:01', '2018-07-29 21:37:01');
INSERT INTO `hu_operation_logs` VALUES (63, 5, 'king19800105', 'App\\Models\\Admin', 'MenuController@store', '{\"name\":\"\\u7cfb\\u7edf\\u6a21\\u5757\",\"slug\":\"system\",\"parent_id\":\"0\",\"weight\":\"6\",\"icon\":\"bbb\"}', '{\"code\":0,\"message\":\"success\",\"result\":{\"name\":\"\\u7cfb\\u7edf\\u6a21\\u5757\",\"slug\":\"system\",\"parent_id\":\"0\",\"weight\":\"6\",\"icon\":\"bbb\",\"updated_at\":\"2018-07-29 21:42:59\",\"created_at\":\"2018-07-29 21:42:59\",\"id\":5}}', '2018-07-29 21:42:59', '2018-07-29 21:42:59');
INSERT INTO `hu_operation_logs` VALUES (64, 5, 'king19800105', 'App\\Models\\Admin', 'MenuController@store', '{\"name\":\"\\u83dc\\u5355\\u5217\\u8868\",\"slug\":\"Menu@index\",\"parent_id\":\"5\",\"url\":\"menu\",\"url_alias\":\"menu.index\",\"weight\":\"1\",\"icon\":\"ttt\"}', '{\"code\":0,\"message\":\"success\",\"result\":{\"name\":\"\\u83dc\\u5355\\u5217\\u8868\",\"slug\":\"Menu@index\",\"parent_id\":\"5\",\"url\":\"menu\",\"url_alias\":\"menu.index\",\"weight\":\"1\",\"icon\":\"ttt\",\"updated_at\":\"2018-07-29 21:47:34\",\"created_at\":\"2018-07-29 21:47:34\",\"id\":7}}', '2018-07-29 21:47:34', '2018-07-29 21:47:34');
COMMIT;

-- ----------------------------
-- Table structure for hu_orders
-- ----------------------------
DROP TABLE IF EXISTS `hu_orders`;
CREATE TABLE `hu_orders` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `order_id` bigint(20) NOT NULL COMMENT '订单编号',
  `user_id` int(11) NOT NULL COMMENT '用户id',
  `goods_id` int(11) NOT NULL COMMENT '商品id',
  `goods_name` varchar(191) COLLATE utf8mb4_general_ci NOT NULL COMMENT '商品名称',
  `goods_number` int(11) NOT NULL DEFAULT '1' COMMENT '购买个数，套餐个数',
  `order_type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '订单类型，1：购买，2：赠送',
  `state` tinyint(4) NOT NULL DEFAULT '0' COMMENT '订单状态(0:未支付，1:已支付，2:申请退款，3:已退款, 4:已过期)，如果申请退款失败，则返回已支付状态',
  `pay_type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '支付方式, 1.支付宝，2.微信，3.银行转账，4...',
  `ori_price` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '原价',
  `discount_price` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '折扣金额',
  `actual_price` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '实际支付金额，赠送的话为0',
  `pay_time` timestamp NOT NULL COMMENT '支付时间',
  `decision_time` timestamp NOT NULL COMMENT '申请退款时间',
  `refund_time` timestamp NOT NULL COMMENT '退款时间',
  `remark` varchar(191) COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '备注信息',
  `source` tinyint(4) NOT NULL DEFAULT '0' COMMENT '来源.1:开放平台，2：微信小程序，3：产品的手机app',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `orders_order_id_unique` (`order_id`),
  KEY `orders_user_id_index` (`user_id`),
  KEY `orders_goods_id_index` (`goods_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Table structure for hu_password_resets
-- ----------------------------
DROP TABLE IF EXISTS `hu_password_resets`;
CREATE TABLE `hu_password_resets` (
  `email` varchar(191) NOT NULL,
  `token` varchar(191) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for hu_permissions
-- ----------------------------
DROP TABLE IF EXISTS `hu_permissions`;
CREATE TABLE `hu_permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) NOT NULL,
  `module` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '',
  `guard_name` varchar(191) NOT NULL,
  `show_name` varchar(191) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `module_name_guard_name` (`module`,`name`,`guard_name`),
  KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of hu_permissions
-- ----------------------------
BEGIN;
INSERT INTO `hu_permissions` VALUES (1, 'Admin@show', 'Admin', 'api_admin', '管理员详情', '2018-07-29 17:45:43', '2018-07-29 17:45:43');
INSERT INTO `hu_permissions` VALUES (2, 'Admin@index', 'Admin', 'api_admin', '管理员列表', '2018-07-29 18:11:19', '2018-07-29 18:11:23');
INSERT INTO `hu_permissions` VALUES (3, 'Admin@update', 'Admin', 'api_admin', '管理员修改', '2018-07-29 18:24:05', '2018-07-29 18:24:05');
INSERT INTO `hu_permissions` VALUES (4, 'Admin@store', 'Admin', 'api_admin', '管理员添加', '2018-07-29 18:26:23', '2018-07-29 18:26:23');
INSERT INTO `hu_permissions` VALUES (5, 'Admin@destroy', 'Admin', 'api_admin', '管理员删除', '2018-07-29 18:26:50', '2018-07-29 18:26:50');
INSERT INTO `hu_permissions` VALUES (6, 'Admin@currentAdminInfo', 'Admin', 'api_admin', '管理员信息详情', '2018-07-29 18:30:08', '2018-07-29 18:30:08');
INSERT INTO `hu_permissions` VALUES (7, 'Menu@index', 'Menu', 'api_admin', '菜单列表', '2018-07-29 18:32:17', '2018-07-29 18:32:17');
INSERT INTO `hu_permissions` VALUES (9, 'Menu@show', 'Menu', 'api_admin', '菜单详情', '2018-07-29 18:32:37', '2018-07-29 18:32:37');
INSERT INTO `hu_permissions` VALUES (10, 'Menu@update', 'Menu', 'api_admin', '菜单修改', '2018-07-29 18:32:52', '2018-07-29 18:32:52');
INSERT INTO `hu_permissions` VALUES (11, 'Menu@store', 'Menu', 'api_admin', '菜单添加', '2018-07-29 18:33:06', '2018-07-29 18:33:06');
INSERT INTO `hu_permissions` VALUES (12, 'Menu@destroy', 'Menu', 'api_admin', '菜单删除', '2018-07-29 18:33:17', '2018-07-29 18:33:17');
INSERT INTO `hu_permissions` VALUES (13, 'Role@index', 'Role', 'api_admin', '角色列表', '2018-07-29 18:34:05', '2018-07-29 18:34:05');
INSERT INTO `hu_permissions` VALUES (14, 'Role@show', 'Role', 'api_admin', '角色详情', '2018-07-29 18:34:14', '2018-07-29 18:34:14');
INSERT INTO `hu_permissions` VALUES (15, 'Role@update', 'Role', 'api_admin', '角色修改', '2018-07-29 18:34:25', '2018-07-29 18:34:25');
INSERT INTO `hu_permissions` VALUES (16, 'Role@store', 'Role', 'api_admin', '角色添加', '2018-07-29 18:34:38', '2018-07-29 18:34:38');
INSERT INTO `hu_permissions` VALUES (17, 'Role@destroy', 'Role', 'api_admin', '角色删除', '2018-07-29 18:34:49', '2018-07-29 18:34:49');
INSERT INTO `hu_permissions` VALUES (18, 'Role@assignPermissions', 'Role', 'api_admin', '角色分配权限', '2018-07-29 18:35:44', '2018-07-29 18:35:44');
INSERT INTO `hu_permissions` VALUES (19, 'Admin@assign', 'Admin', 'api_admin', '管理员分配角色', '2018-07-29 18:36:34', '2018-07-29 18:36:34');
INSERT INTO `hu_permissions` VALUES (20, 'Permission@index', 'Permission', 'api_admin', '权限节点列表', '2018-07-29 18:37:39', '2018-07-29 18:37:39');
INSERT INTO `hu_permissions` VALUES (21, 'Permission@show', 'Permission', 'api_admin', '权限节点详情', '2018-07-29 18:37:55', '2018-07-29 18:37:55');
INSERT INTO `hu_permissions` VALUES (22, 'Permission@store', 'Permission', 'api_admin', '权限节点添加', '2018-07-29 18:38:06', '2018-07-29 18:38:06');
INSERT INTO `hu_permissions` VALUES (23, 'Permission@update', 'Permission', 'api_admin', '权限节点修改', '2018-07-29 18:38:15', '2018-07-29 18:38:15');
INSERT INTO `hu_permissions` VALUES (24, 'Permission@destroy', 'Permission', 'api_admin', '权限节点删除', '2018-07-29 18:38:34', '2018-07-29 18:38:34');
INSERT INTO `hu_permissions` VALUES (25, 'Admin@resetPassword', 'Admin', 'api_admin', '管理员密码修改', '2018-07-29 20:44:31', '2018-07-29 20:44:31');
COMMIT;

-- ----------------------------
-- Table structure for hu_review_templates
-- ----------------------------
DROP TABLE IF EXISTS `hu_review_templates`;
CREATE TABLE `hu_review_templates` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0' COMMENT '提交用户id',
  `verifier_id` int(11) NOT NULL DEFAULT '0' COMMENT '审核人id',
  `title` varchar(191) NOT NULL DEFAULT '' COMMENT '模板标题',
  `sign` varchar(191) NOT NULL DEFAULT '' COMMENT '模板签名',
  `content` varchar(191) NOT NULL DEFAULT '' COMMENT '模板内容',
  `state` tinyint(4) NOT NULL DEFAULT '0' COMMENT '当前状态（0:未审核，1：已通过，2：已驳回）',
  `channel_group_id` int(11) NOT NULL DEFAULT '0' COMMENT '模板类型（1：验证码。2：订单通知。）',
  `only_channel_id` int(11) NOT NULL DEFAULT '0' COMMENT '模板等级（根据等级，走不通的通道）',
  `rejected_reason` varchar(191) NOT NULL DEFAULT '' COMMENT '驳回原因',
  `remark` varchar(191) NOT NULL DEFAULT '' COMMENT '管理员备注',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `review_templates_user_id_title_unique` (`title`,`user_id`) USING BTREE,
  KEY `review_templates_user_id_index` (`user_id`),
  KEY `review_templates_remark_index` (`remark`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of hu_review_templates
-- ----------------------------
BEGIN;
INSERT INTO `hu_review_templates` VALUES (1, 5, 5, '第一次提交', '迁移', '短信来了', 1, 2, 20, '', '', '2018-07-03 17:02:14', '2018-07-04 15:49:49');
INSERT INTO `hu_review_templates` VALUES (3, 6, 0, '第一次提交3', '迁移', 'dddddd', 0, 1, 0, '', '', NULL, NULL);
INSERT INTO `hu_review_templates` VALUES (4, 5, 0, '第一次提交4', '迁移', '短信来了', 0, 1, 0, '', '', '2018-07-03 17:14:32', '2018-07-03 17:14:32');
INSERT INTO `hu_review_templates` VALUES (5, 5, 0, '第一次提交6', '迁移', '短信来了', 0, 1, 0, '', '', '2018-07-03 17:38:10', '2018-07-03 17:38:10');
INSERT INTO `hu_review_templates` VALUES (6, 5, 0, '第一次提交8', '迁移', '短信来了', 0, 1, 0, '', '', '2018-07-03 17:38:43', '2018-07-03 17:38:43');
INSERT INTO `hu_review_templates` VALUES (7, 5, 0, '第一次提交3', '迁移', '短信来了', 0, 1, 0, '', '', '2018-07-04 12:48:17', '2018-07-04 13:23:38');
COMMIT;

-- ----------------------------
-- Table structure for hu_role_has_permissions
-- ----------------------------
DROP TABLE IF EXISTS `hu_role_has_permissions`;
CREATE TABLE `hu_role_has_permissions` (
  `permission_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `role_has_permissions_role_id_foreign` (`role_id`),
  CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `hu_permissions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `hu_roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of hu_role_has_permissions
-- ----------------------------
BEGIN;
INSERT INTO `hu_role_has_permissions` VALUES (1, 2);
INSERT INTO `hu_role_has_permissions` VALUES (2, 2);
INSERT INTO `hu_role_has_permissions` VALUES (3, 2);
INSERT INTO `hu_role_has_permissions` VALUES (6, 2);
INSERT INTO `hu_role_has_permissions` VALUES (7, 2);
INSERT INTO `hu_role_has_permissions` VALUES (9, 2);
INSERT INTO `hu_role_has_permissions` VALUES (10, 2);
INSERT INTO `hu_role_has_permissions` VALUES (11, 2);
INSERT INTO `hu_role_has_permissions` VALUES (12, 2);
INSERT INTO `hu_role_has_permissions` VALUES (13, 2);
INSERT INTO `hu_role_has_permissions` VALUES (14, 2);
INSERT INTO `hu_role_has_permissions` VALUES (20, 2);
INSERT INTO `hu_role_has_permissions` VALUES (21, 2);
COMMIT;

-- ----------------------------
-- Table structure for hu_roles
-- ----------------------------
DROP TABLE IF EXISTS `hu_roles`;
CREATE TABLE `hu_roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) NOT NULL,
  `guard_name` varchar(191) NOT NULL,
  `show_name` varchar(191) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_guard_name` (`name`,`guard_name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of hu_roles
-- ----------------------------
BEGIN;
INSERT INTO `hu_roles` VALUES (1, 'super_admin', 'api_admin', '后台超级管理员', '2018-07-29 18:43:01', '2018-07-29 18:43:01');
INSERT INTO `hu_roles` VALUES (2, 'admin', 'api_admin', '后台管理员', '2018-07-29 18:51:36', '2018-07-29 18:51:36');
COMMIT;

-- ----------------------------
-- Table structure for hu_sign_reports
-- ----------------------------
DROP TABLE IF EXISTS `hu_sign_reports`;
CREATE TABLE `hu_sign_reports` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sms_sign` varchar(191) COLLATE utf8mb4_general_ci NOT NULL COMMENT '签名',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `sms_sign` (`sms_sign`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Table structure for hu_slow_query_logs
-- ----------------------------
DROP TABLE IF EXISTS `hu_slow_query_logs`;
CREATE TABLE `hu_slow_query_logs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `exec` varchar(191) NOT NULL DEFAULT '',
  `expend_time` double(8,2) NOT NULL DEFAULT '0.00',
  `ori_sql` text NOT NULL,
  `sql_content` text NOT NULL,
  `is_fixed` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `slow_query_logs_exec_index` (`exec`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for hu_sms_replies
-- ----------------------------
DROP TABLE IF EXISTS `hu_sms_replies`;
CREATE TABLE `hu_sms_replies` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `mobile` varchar(191) COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '手机号码',
  `content` varchar(191) COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '回复的内容',
  `channel_id` int(11) NOT NULL COMMENT '通道id',
  `sub_code` int(11) NOT NULL COMMENT '扩展码',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `sms_replies_mobile_index` (`mobile`),
  KEY `sms_replies_channel_id_index` (`channel_id`),
  KEY `sms_replies_sub_code_index` (`sub_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Table structure for hu_sms_send_limits
-- ----------------------------
DROP TABLE IF EXISTS `hu_sms_send_limits`;
CREATE TABLE `hu_sms_send_limits` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL COMMENT '用户id',
  `mobile_day_limit` int(11) NOT NULL DEFAULT '20' COMMENT '同一手机号码一天20条, 为0则不限制',
  `mobile_minute_limit` int(11) NOT NULL DEFAULT '1' COMMENT '同一手机号一分钟1条，为0则不限制',
  `day_limit` int(11) NOT NULL DEFAULT '0' COMMENT '每天调用次数, 为0则不限制',
  `hour_limit` int(11) NOT NULL DEFAULT '0' COMMENT '每小时调用次数, 为0则不限制',
  `minute_limit` int(11) NOT NULL DEFAULT '0' COMMENT '每分钟时调用次数, 为0则不限制',
  `exceed_count` int(11) NOT NULL DEFAULT '0' COMMENT '超过限制次数（超过3次以内，则提示，3次和以上，则锁定账号）',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Table structure for hu_sms_send_records
-- ----------------------------
DROP TABLE IF EXISTS `hu_sms_send_records`;
CREATE TABLE `hu_sms_send_records` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `report_id` varchar(191) COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '通道返回的id，加上通道标识符组合唯一',
  `user_id` int(11) DEFAULT NULL,
  `mobile` varchar(191) COLLATE utf8mb4_general_ci NOT NULL COMMENT '收信人手机',
  `sms_sign` varchar(191) COLLATE utf8mb4_general_ci NOT NULL COMMENT '短信签名',
  `content` text COLLATE utf8mb4_general_ci,
  `state` tinyint(4) NOT NULL DEFAULT '0' COMMENT '提交成功，提交失败，接收成功，接收失败',
  `reason` varchar(191) COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '失败原因',
  `channel_id` int(11) NOT NULL COMMENT '通道表主键, 和状态status的成功组成唯一索引',
  `channel_group_id` int(11) NOT NULL COMMENT '短信发送类型, 通道分类表，分$t组名称',
  `try_count` tinyint(4) NOT NULL DEFAULT '0' COMMENT '重试次数',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `report_id_channel_id` (`report_id`,`channel_id`),
  KEY `mobile` (`mobile`),
  KEY `channel_id` (`channel_id`),
  KEY `channel_group_id` (`channel_group_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Table structure for hu_sms_user_lists
-- ----------------------------
DROP TABLE IF EXISTS `hu_sms_user_lists`;
CREATE TABLE `hu_sms_user_lists` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL COMMENT '用户id',
  `type` tinyint(4) NOT NULL COMMENT '列表类型，0：黑名单，1：白名单',
  `bind_key` tinyint(4) NOT NULL COMMENT '绑定类型，1：手机，2：ip',
  `bind_value` varchar(191) COLLATE utf8mb4_general_ci NOT NULL COMMENT '绑定的值，手机号码或ip地址',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `sms_user_lists_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Table structure for hu_tags
-- ----------------------------
DROP TABLE IF EXISTS `hu_tags`;
CREATE TABLE `hu_tags` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) NOT NULL DEFAULT '',
  `alias_name` varchar(191) NOT NULL DEFAULT '',
  `model_type` varchar(191) NOT NULL DEFAULT '',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `tags_name_unique` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for hu_user_funds
-- ----------------------------
DROP TABLE IF EXISTS `hu_user_funds`;
CREATE TABLE `hu_user_funds` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL COMMENT '用户id',
  `type` tinyint(4) NOT NULL COMMENT ' 流水的类型, 1：充值短信（转换为条数）。2：使用短信（消费条数）。3：推广返现。4：退款。5：提现。',
  `sms_record` bigint(20) NOT NULL DEFAULT '0' COMMENT '短信充值和消费记录，充值短信条数，消费短信条数',
  `cash_record` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '现金消费记录，返现，退款，提现',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_funds_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Table structure for hu_users
-- ----------------------------
DROP TABLE IF EXISTS `hu_users`;
CREATE TABLE `hu_users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL DEFAULT '0' COMMENT '父级id，仅支持一级。（0表示无上级）',
  `name` varchar(191) COLLATE utf8mb4_general_ci NOT NULL COMMENT '用户名',
  `email` varchar(191) COLLATE utf8mb4_general_ci NOT NULL COMMENT '邮箱',
  `mobile` varchar(191) COLLATE utf8mb4_general_ci NOT NULL COMMENT '手机号码',
  `password` varchar(191) COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '登入密码',
  `qq` varchar(191) COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT 'qq号码',
  `head_portrait` varchar(191) COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '头像地址',
  `real_name` varchar(191) COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '真实姓名',
  `type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '客户类型，1：普通账号。2：业务合作用户账号。3：内部销售账号。4：大客户账号',
  `state` tinyint(4) NOT NULL DEFAULT '1' COMMENT '用户当前状态,0：冻结，1：可用，2：锁定',
  `source` tinyint(4) NOT NULL DEFAULT '0' COMMENT '用户来源，来源,0：手动添加， 1：微信，2：开放平台',
  `promotion_code` varchar(191) COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '推广码，发展下线使用',
  `earn` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '用户当前收益',
  `total_number` int(11) NOT NULL DEFAULT '0' COMMENT '总购买量，每次购买累加。',
  `gift_number` int(11) NOT NULL DEFAULT '0' COMMENT '赠送数量',
  `use_number` int(11) NOT NULL DEFAULT '0' COMMENT '使用总量。redis同步',
  `residue_number` int(11) NOT NULL DEFAULT '0' COMMENT '剩余条数。redis同步',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `last_login_ip` char(15) COLLATE utf8mb4_general_ci NOT NULL DEFAULT '',
  `last_sync_time` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_name_unique` (`name`),
  UNIQUE KEY `users_email_unique` (`email`),
  UNIQUE KEY `users_mobile_unique` (`mobile`),
  UNIQUE KEY `users_promotion_code_unique` (`promotion_code`),
  KEY `users_last_login_ip` (`last_login_ip`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

SET FOREIGN_KEY_CHECKS = 1;
