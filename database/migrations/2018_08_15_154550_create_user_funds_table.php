<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserFundsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_funds', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_id')->index()->comment('用户id');
            $table->tinyInteger('type')->comment(' 流水的类型, 1：充值短信（转换为条数）。2：使用短信（消费条数）。3：推广返现。4：退款。5：提现。');
            $table->bigInteger('sms_record')->default(0)->comment('短信充值和消费记录，充值短信条数，消费短信条数');
            $table->decimal('cash_record',10, 2)->default(0.00)->comment('现金消费记录，返现，退款，提现');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_funds');
    }
}
