<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInsideMessageSystemsTable extends Migration
{
    /**
     * 系统消息系统消息群发时
     *
     * inside_messages_content表只有一条记录。
     * inside_messages也只有一条记录。记录管理员的模型，收件人模型，还有发送管理员的id。收件人id为0。
     * inside_messages_sys记录所有收到记录用户的id。
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inside_message_systems', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('customer_id')->index()->comment('推送给的客户id');
            $table->integer('message_id')->index()->comment('关联的消息id');
            $table->tinyInteger('state')->default(0)->comment('未读。已读');
            $table->timestamp('updated_at')->default(null);
            $table->timestamp('created_at')->default(null)->index();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inside_message_systems');
    }
}
