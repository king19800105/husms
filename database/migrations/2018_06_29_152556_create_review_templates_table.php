<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReviewTemplatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('review_templates', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->index()->nullable(false)->default(0)->comment('提交用户id');
            $table->integer('verifier_id')->nullable(false)->default(0)->comment('审核人id');
            $table->string('title')->nullable(false)->default('')->comment('模板标题');
            $table->string('sign')->nullable(false)->default('')->comment('模板签名');
            $table->string('content')->nullable(false)->default('')->comment('模板内容');
            $table->tinyInteger('state')->nullable(false)->default(0)->comment('当前状态（0:未审核，1：已通过，2：已驳回）');
            $table->integer('channel_group_id')->nullable(false)->default(0)->comment('所属通道组id');
            $table->integer('only_channel_id')->nullable(false)->default(0)->comment('指定唯一通道，0：根据通道组生效，其他id，根据设置的通道id生效');
            $table->string('rejected_reason')->nullable(false)->default('')->comment('驳回原因');
            $table->string('remark')->index()->nullable(false)->default('')->comment('管理员备注');
            $table->unique(['title', 'user_id']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('review_templates');
    }
}
