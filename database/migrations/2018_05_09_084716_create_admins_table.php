<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdminsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admins', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string('name')->unique()->nullable(false)->default('')->comment('用户名');
            $table->string('email')->unique()->nullable(false)->default('')->comment('邮箱');
            $table->string('mobile')->unique()->nullable(false)->default('')->comment('手机号码');
            $table->string('password')->nullable(false)->default('')->comment('管理员密码');
            $table->string('avatar')->nullable(false)->default('')->comment('头像地址');
            $table->string('producer')->nullable(false)->default('')->comment('创建人');
            $table->string('remark')->nullable(false)->default('')->comment('备注信息');
            $table->tinyInteger('state')->nullable(false)->default(1)->comment('用户状态，1：可用，2：冻结');
            $table->char('last_logged_ip', 15)->default('');
            $table->rememberToken();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admins');
    }
}
