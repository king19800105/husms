<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChannelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('channels', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->unique()->default('')->comment('通道名称');
            $table->string('access_type', 50)->comment('对接类型, cmpp，http等');
            $table->string('access_domain')->default('')->comment('接入域名');
            $table->string('access_ip')->default('')->comment('接入ip地址');
            $table->mediumInteger('access_port')->default(0)->comment('接入端口');
            $table->integer('channel_model_id')->default(0)->comment('通道模型id');
            $table->tinyInteger('is_sign_limit')->default(0)->comment('是否受到签名限制（0：无限制，1：受限）');
            $table->tinyInteger('state')->default(0)->comment('当前状态（是否可用）');
            $table->tinyInteger('weight')->default(0)->comment('显示的权重，根据权重来排名');
            $table->string('remark')->default('')->comment('通道备注信息');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('channels');
    }
}
