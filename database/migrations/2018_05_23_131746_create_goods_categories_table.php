<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGoodsCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('goods_categories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->unique()->comment('商品分类名称');
            $table->string('alias_name')->index()->default('')->comment('商品分类别名');
            $table->integer('parent_id')->default(0)->comment('分类父级id，顶级为0');
            $table->tinyInteger('show_type')->default(1)->comment('显示类型，1：全部端都显示，2:仅支持pc端，3：支持手机端');
            $table->tinyInteger('weight')->default(1)->comment('权重');
            $table->tinyInteger('state')->default(1)->comment('分类状态，1显示，0不显示');
            $table->string('img')->default('')->comment('分类图片url');
            $table->string('seo_title')->default('');
            $table->string('seo_keywords')->default('');
            $table->string('seo_description')->default('');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('goods_categories');
    }
}
