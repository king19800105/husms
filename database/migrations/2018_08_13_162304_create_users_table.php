<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('parent_id')->default(0)->comment('父级id，仅支持一级。（0表示无上级）');
            $table->string('name')->unique()->comment('用户名');
            $table->string('email')->unique()->comment('邮箱');
            $table->string('mobile')->unique()->comment('手机号码');
            $table->string('password')->default('')->comment('登入密码');
            $table->string('qq')->default('')->comment('qq号码');
            $table->string('head_portrait')->default('')->comment('头像地址');
            $table->string('real_name')->default('')->comment('真实姓名');
            $table->tinyInteger('type')->default(1)->comment('客户类型，1：普通账号。2：业务合作用户账号。3：内部销售账号。4：大客户账号');
            $table->tinyInteger('state')->default(1)->comment('用户当前状态,0：冻结，1：可用，2：锁定');
            $table->tinyInteger('source')->default(0)->comment('用户来源，来源,0：手动添加， 1：微信，2：开放平台');
            $table->string('promotion_code')->unique()->default('')->comment('推广码，发展下线使用');
            $table->decimal('earn', 10, 2)->default(0.00)->comment('用户当前收益');
            $table->integer('total_number')->default(0)->comment('总购买量，每次购买累加。');
            $table->integer('gift_number')->default(0)->comment('赠送数量');
            $table->integer('use_number')->default(0)->comment('使用总量。redis同步');
            $table->integer('residue_number')->default(0)->comment('剩余条数。redis同步');
            $table->char('last_login_ip', 15)->index()->default('')->comment('最后登入时间');
            $table->timestamp('last_sync_time')->comment('上次数据同步时间');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
