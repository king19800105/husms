<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSlowQueryLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('slow_query_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('exec')->index()->default('');
            $table->double('expend_time', 8, 2)->default(0.00);
            $table->text('ori_sql');
            $table->text('sql_content');
            $table->tinyInteger('is_fixed')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('slow_query_logs');
    }
}
