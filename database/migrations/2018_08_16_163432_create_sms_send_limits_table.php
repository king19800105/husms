<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSmsSendLimitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sms_send_limits', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_id')->unique()->comment('用户id');
            $table->integer('mobile_day_limit')->default(20)->comment('同一手机号码一天20条, 为0则不限制');
            $table->integer('mobile_minute_limit')->default(1)->comment('同一手机号一分钟1条，为0则不限制');
            $table->integer('day_limit')->default(0)->comment('每天调用次数, 为0则不限制');
            $table->integer('hour_limit')->default(0)->comment('每小时调用次数, 为0则不限制');
            $table->integer('minute_limit')->default(0)->comment('每分钟时调用次数, 为0则不限制');
            $table->integer('exceed_count')->default(0)->comment('超过限制次数（超过3次以内，则提示，3次和以上，则锁定账号）');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sms_send_limits');
    }
}
