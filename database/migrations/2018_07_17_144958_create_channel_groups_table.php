<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChannelGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('channel_groups', function (Blueprint $table) {
            $table->increments('id');
            $table->string('group_name')->unique()->comment('分组名称');
            $table->integer('parent_id')->comment('上级分组id');
            $table->tinyInteger('state')->comment('当前状态（是否可用）');
            $table->tinyInteger('weight')->comment('显示排序权重值');
            $table->string('remark')->comment('备注');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('channel_groups');
    }
}
