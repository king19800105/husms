<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('order_id')->unique()->comment('订单编号');
            $table->integer('user_id')->index()->comment('用户id');
            $table->integer('goods_id')->index()->comment('商品id');
            $table->string('goods_name')->comment('商品名称');
            $table->integer('goods_number')->default(1)->comment('购买个数，套餐个数');
            $table->tinyInteger('order_type')->default(1)->comment('订单类型，1：购买，2：赠送');
            $table->tinyInteger('state')->default(0)->comment('订单状态(0:未支付，1:已支付，2:申请退款，3:已退款, 4:已过期, 5.已取消)，如果申请退款失败，则返回已支付状态');
            $table->tinyInteger('pay_type')->default(0)->comment('支付方式, 1.支付宝，2.微信，3.银行转账，4...');
            $table->decimal('ori_price', 10, 2)->default(0.00)->comment('原价');
            $table->decimal('discount_price', 10, 2)->default(0.00)->comment('折扣金额');
            $table->decimal('actual_price', 10, 2)->default(0.00)->comment('实际支付金额，赠送的话为0');
            $table->timestamp('pay_time')->default(null)->comment('支付时间');
            $table->timestamp('decision_time')->default(null)->comment('申请退款时间');
            $table->timestamp('refund_time')->default(null)->comment('退款时间');
            $table->string('remark')->default('')->comment('备注信息');
            $table->tinyInteger('source')->default(0)->comment('来源.1:开放平台，2：微信小程序，3：产品的手机app');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
