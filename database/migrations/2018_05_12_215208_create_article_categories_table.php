<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticleCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('article_categories', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string('name')->unique()->comment('文章分类名称');
            $table->string('alias_name')->unique()->comment('文章分类别名');
            $table->integer('parent_id')->nullable(false)->default(0)->comment('父级id');
            $table->string('seo_keywords')->nullable(false)->default('')->comment('seo关键词');
            $table->string('seo_title')->nullable(false)->default('')->comment('seo标签');
            $table->string('seo_description')->nullable(false)->default('')->comment('seo描述');
            $table->tinyInteger('weight')->nullable(false)->default(0)->comment('排序权重');
            $table->string('img')->nullable(false)->default('')->comment('分类图片地址');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('article_categories');
    }
}
