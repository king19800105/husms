<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMenusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menus', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string('name')->comment('菜单名称');
            $table->string('slug')->nullable(false)->default('')->comment('菜单权限');
            $table->integer('parent_id')->nullable(false)->default(0)->comment('父级id');
            $table->string('url')->nullable(false)->default('')->comment('请求地址，如：/admin');
            $table->string('url_alias')->nullable(false)->default('')->comment('请求地址的别名，如：admin.index');
            $table->tinyInteger('weight')->nullable(false)->default(0)->comment('权重排序值。0~255');
            $table->string('type')->nullable(false)->default('backend')->comment('菜单类型');
            $table->string('icon')->nullable(false)->default('')->comment('图标');
            $table->string('light')->nullable(false)->default('')->comment('高亮名称');
            $table->string('description')->nullable(false)->default('')->comment('菜单描述');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menus');
    }
}
