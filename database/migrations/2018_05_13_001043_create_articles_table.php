<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articles', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string('title')->unique();
            $table->string('author')->default('');
            $table->text('content_markdown');
            $table->text('content_html');
            $table->string('intro')->default(''); // 如果手动设置了，就是手动设置的。如果没有手动设置，就取出字符串的前150个字符
            $table->integer('category_id')->unsigned();
            $table->string('img')->default('');
            $table->tinyInteger('weight')->default(0);
            $table->tinyInteger('is_top')->default(0); // 是否置顶
            $table->tinyInteger('is_recommend')->default(0); // 是否推荐
            $table->tinyInteger('is_hot')->default(0); // 是否热门
            $table->tinyInteger('state')->default(1); // 1：发布，2：审核
            $table->string('seo_title')->default('');
            $table->string('seo_keywords')->default('');
            $table->string('seo_description')->default('');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('articles');
    }
}
