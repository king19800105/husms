<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChannelChannelGroupTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('channel_channel_group', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('channel_id')->index()->comment('通道id');
            $table->integer('channel_group')->index()->comment('通道组id');
            $table->tinyInteger('rate')->comment('0：不可用，先被过滤掉。1~100,百分比分配比率');
            $table->decimal('price', 6, 5)->default(0.00000)->comment('该通道发送此类短信的价格');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('channel_channel_group');
    }
}
