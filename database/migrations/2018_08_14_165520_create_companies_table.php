<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('companies', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->index()->default(0)->comment('用户id');
            $table->string('company_name')->default('')->comment('公司名称');
            $table->tinyInteger('qualification_state')->default(0)->comment('审核状态.0：未提交。1：已通过。2：审核中。3：已驳回。');
            $table->string('license_uri')->default('')->comment('营业执照地址');
            $table->string('organization_uri')->default('')->comment('组织机构代码证');
            $table->string('id_card_front_uri')->default('')->comment('经办人身份证正面');
            $table->string('id_card_back_uri')->default('')->comment('经办人身份证反面');
            $table->string('tax_register_uri')->default('')->comment('税务登记证');
            $table->string('icp_uri')->default('')->comment('备案截图');
            $table->string('sms_app_uri')->default('')->comment('短信应用截图');
            $table->string('site_url')->default('')->comment('官网地址');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('companies');
    }
}
