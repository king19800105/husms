<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInsideMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inside_messages', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('sender_id')->index()->default(0)->comment('发起人id');
            $table->integer('recipient_id')->index()->default(0)->comment('接收人id，如果是系统群发，则为0');
            $table->string('sender_model')->default('')->comment('发送者模型');
            $table->string('recipient_model')->default('')->comment('接收者模型');
            $table->bigInteger('content_id')->comment('内部消息表id');
            $table->tinyInteger('state')->default(0)->comment('当前信息的状态。0：未查看，1：已查看');
            $table->tinyInteger('type')->default(1)->comment('1.站内系统通知(单向。管理员->会员)。2.业务沟通（双向。管理员和会员）。3.工单提价（双向。管理员和会员）');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inside_messages');
    }
}
