<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSmsRepliesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sms_replies', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('mobile')->index()->default('')->comment('手机号码');
            $table->string('content')->default('')->comment('回复的内容');
            $table->integer('channel_id')->index()->comment('通道id');
            $table->integer('sub_code')->index()->comment('扩展码');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sms_replies');
    }
}
