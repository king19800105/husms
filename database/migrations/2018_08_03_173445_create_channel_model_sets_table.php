<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChannelModelSetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('channel_model_sets', function (Blueprint $table) {
            $table->increments('id');
            $table->string('model_name')->default('')->comment('模型名称，1：三网通。2：仅移动。3：仅联通。4：仅电信。5：移动 + 联通。6：移动 + 电信。7：联通 + 电信');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('channel_model_sets');
    }
}
