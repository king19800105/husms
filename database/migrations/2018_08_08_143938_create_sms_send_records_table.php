<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSmsSendRecordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sms_send_records', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('report_id')->default('')->comment('通道返回的id，加上通道标识符组合唯一');
            $table->integer('user_id')->index()->comment('用户id');
            $table->string('mobile')->index()->comment('收信人手机');
            $table->string('sms_sign')->comment('短信签名');
            $table->text('content')->comment('短信内容');
            $table->tinyInteger('state')->default(0)->comment('提交成功，提交失败，接收成功，接收失败');
            $table->string('reason')->default('')->comment('失败原因');
            $table->integer('channel_id')->index()->comment('通道表主键, 和状态status的成功组成唯一索引');
            $table->integer('channel_group_id')->index()->comment('短信发送类型, 通道分类表，分$t组名称');
            $table->tinyInteger('try_count')->default(0)->comment('重试次数');
            $table->unique(['report_id', 'channel_id']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sms_send_records');
    }
}
