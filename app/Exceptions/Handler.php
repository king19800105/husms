<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Validation\ValidationException;
use Spatie\Permission\Exceptions\UnauthorizedException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\Routing\Exception\MethodNotAllowedException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        if ($request->wantsJson()) {
            return $this->setJsonExceptionResponse($exception);
        }

        return parent::render($request, $exception);
    }

    protected function setJsonExceptionResponse(Exception $exception)
    {
        $status = method_exists($exception, 'getStatusCode') ? $exception->getStatusCode() : 400;
        $message = $exception->getMessage() ?: __('message.common.errors');
        $data = class_basename(get_class($exception));

        if ($exception instanceof AuthenticationException || $exception instanceof UnauthorizedHttpException) {
            $status = 401;
            $message = __('exception.authentication');
        }

        if ($exception instanceof AuthorizationException || ($exception instanceof UnauthorizedException)) {
            $status = 403;
            $message = __('exception.authorization');
        }

        if ($exception instanceof ModelNotFoundException) {
            $status = 404;
            $modelClass = explode('\\', $exception->getModel());
            $message = end($modelClass) . ' ' . __('exception.not_find');
        }

        if ($exception instanceof NotFoundHttpException) {
            $status = 404;
            $message = 'Http ' . __('exception.not_find');
        }

        if ($exception instanceof ValidationException) {
            $status = 422;
            $message = __('exception.validation');
            $data = $exception->errors();
        }

        if ($exception instanceof MethodNotAllowedException) {
            $status = 405;
            $message = __('exception.not_allow');
        }

        return response_api($data, $message, $status);
    }
}
