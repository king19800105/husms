<?php
/**
 * Created by PhpStorm.
 * User: anthony
 * Date: 2018/8/13
 * Time: 下午5:43
 */

namespace App\Observers;


use App\Models\User;

class UserObserver
{
    public function creating(User $user)
    {
        $user->password = bcrypt($user->password);
    }
}