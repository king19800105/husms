<?php
/**
 * Created by PhpStorm.
 * User: king
 * Date: 2018/2/21
 * Time: 下午10:50
 */
namespace App\Observers;

use App\Models\Article;

class ArticleObserver
{
    // 关联删除设置
    public function deleting(Article $article)
    {
        $article->tags()->detach();
    }
}