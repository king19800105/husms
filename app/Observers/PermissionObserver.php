<?php
/**
 * Created by PhpStorm.
 * User: kb130
 * Date: 2018/5/10
 * Time: 16:34
 */

namespace App\Observers;


use App\Models\Permission;

class PermissionObserver
{
    public function creating(Permission $permission)
    {
        $permission->name = ucfirst($permission->name);
        $module = explode('@', $permission->name);
        $permission->module = $module[0] ?? '';
    }
}