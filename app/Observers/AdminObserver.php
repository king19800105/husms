<?php
/**
 * Created by PhpStorm.
 * User: king
 * Date: 2018/2/21
 * Time: 下午10:50
 */
namespace App\Observers;

use App\Models\Admin;

class AdminObserver
{
    public function creating(Admin $admin)
    {
        $admin->password = bcrypt($admin->password);
        $admin->producer = request()->user()->name;

        if (is_null($admin->remark)) {
            $admin->remark = '';
        }
    }

    public function updating(Admin $admin)
    {
        if ($admin->new_password) {
            $admin->password = bcrypt($admin->new_password);
            unset($admin->new_password);
        }
    }
}