<?php

namespace App\Http\Requests\Order;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'order_type'   => 'required|in:1,2',
            'state'        => 'required|in:0,1,2,3,4',
            'pay_type'     => 'required|in:1,2,3',
            'actual_price' => 'required|numeric|min:0',
            'remark'       => 'string|between:2,255',
        ];
    }

    public function attributes()
    {
        return [
            'order_type'   => __('message.order.order_type'),
            'state'        => __('message.order.state'),
            'pay_type'     => __('message.order.pay_type'),
            'actual_price' => __('message.order.actual_price'),
            'remark'       => __('message.order.remark'),
        ];
    }
}
