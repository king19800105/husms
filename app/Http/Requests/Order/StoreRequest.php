<?php

namespace App\Http\Requests\Order;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'order_id'     => 'required|integer|unique:orders',
            'user_id'      => 'required|integer',
            'goods_id'     => 'required|integer',
            'goods_name'   => 'required|string|between:2,255',
            'goods_number' => 'required|integer|between:1,99',
            'source'       => 'required|in:1,2,3,4,5',
        ];
    }


    public function attributes()
    {
        return [
            'order_id'     => __('message.order.order_id'),
            'user_id'      => __('message.order.user_id'),
            'goods_id'     => __('message.order.goods_id'),
            'goods_name'   => __('message.order.goods_name'),
            'goods_number' => __('message.order.goods_number'),
            'source'       => __('message.order.source'),
        ];
    }
}
