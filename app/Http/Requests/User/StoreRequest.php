<?php

namespace App\Http\Requests\User;

use App\Rules\ValidMobile;
use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'                  => 'bail|required|unique:users|alpha_dash|between:5,25',
            'password'              => 'required|min:6|alpha_dash|confirmed',
            'password_confirmation' => '',
            'email'                 => 'required|email|unique:users|max:60',
            'mobile'                => ['required', 'max:20', 'unique:users', new ValidMobile()],
            'qq'                    => 'integer',
            'head_portrait'         => 'string|url|between:5, 255',
            'real_name'             => 'string|between:2, 50',
        ];
    }

    public function attributes()
    {
        return [
            'name'                  => __('message.user.name'),
            'password'              => __('message.user.password'),
            'password_confirmation' => __('message.user.password_confirmation'),
            'email'                 => __('message.user.email'),
            'mobile'                => __('message.user.mobile'),
            'qq'                    => __('message.user.qq'),
            'head_portrait'         => __('message.user.head_portrait'),
            'real_name'             => __('message.user.real_name'),
        ];
    }
}
