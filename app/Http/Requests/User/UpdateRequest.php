<?php

namespace App\Http\Requests\User;


use App\Rules\ValidMobile;

class UpdateRequest extends StoreRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            'name'   => 'required|unique:users,name,' . $this->name .'|alpha_dash|between:5,25',
            'email'  => 'required|unique:users,email,' . $this->email .'|email|max:60',
            'mobile' => ['required', 'max:20', 'unique:users,mobile'. $this->mobile, new ValidMobile()],
            'type'   => 'required|in:1,2,3,4',
            'state'  => 'required|in:0,1,2',
        ]);
    }
}
