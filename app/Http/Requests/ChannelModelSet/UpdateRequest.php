<?php

namespace App\Http\Requests\ChannelModelSet;

class UpdateRequest extends StoreRequest
{
    public function rules()
    {
        return array_merge(parent::rules(), [
            'model_name' => 'required|unique:channel_model_sets,model_name,' . $this->model_name .'|integer',
        ]);
    }
}
