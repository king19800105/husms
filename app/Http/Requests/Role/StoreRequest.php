<?php

namespace App\Http\Requests\Role;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'       => 'bail|required|string|between:1,25',
            'show_name'  => 'string|between:1,25',
            'guard_name' => 'string|between:1,25',
        ];
    }

    public function attributes()
    {
        return [
            'name'       => __('message.role.name'),
            'guard_name' => __('message.role.guard_name'),
            'show_name'  => __('message.role.show_name'),
        ];
    }
}
