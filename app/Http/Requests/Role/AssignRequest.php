<?php

namespace App\Http\Requests\Role;

use App\Rules\ValidJsonArray;
use Illuminate\Foundation\Http\FormRequest;

class AssignRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id'             => 'required|integer|min:1',
            'permission_ids' => ['required', new ValidJsonArray()],
        ];
    }
}
