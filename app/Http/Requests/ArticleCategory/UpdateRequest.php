<?php

namespace App\Http\Requests\ArticleCategory;


class UpdateRequest extends StoreRequest
{
    public function rules()
    {
        return array_merge(parent::rules(), [
            'name'       => 'required|unique:article_categories,name,' . $this->article_category .'|string',
            'alias_name' => 'unique:article_categories,alias_name,' . $this->article_category .'|string',
        ]);
    }
}
