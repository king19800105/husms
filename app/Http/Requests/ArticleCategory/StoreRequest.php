<?php

namespace App\Http\Requests\ArticleCategory;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $mimes = config('common.fileupload.img_mimes');
        $fileMaxsize = config('common.fileupload.maxsize', '1024');

        return [
            'name'            => ['required', 'between:2,255', 'unique:article_categories'],
            'alias_name'      => 'string|between:2,255',
            'parent_id'       => 'required|integer|min:0',
            'seo_keywords'    => 'string|between:2,100',
            'seo_title'       => 'string|between:2,100',
            'seo_description' => 'string|between:2,180',
            'weight'          => 'integer|between:0,255',
            'img'         => 'image|mimes:' . $mimes . '|max:' . $fileMaxsize,
        ];
    }

    public function attributes()
    {
        return [
            'name'            => __('message.article_category.name'),
            'alias_name'      => __('message.article_category.alias_name'),
            'parent_id'       => __('message.article_category.parent_id'),
            'seo_keywords'    => __('message.article_category.seo_keywords'),
            'seo_title'       => __('message.article_category.seo_title'),
            'seo_description' => __('message.article_category.seo_description'),
            'weight'          => __('message.article_category.weight'),
            'img'             => __('message.article_category.img'),
        ];
    }
}
