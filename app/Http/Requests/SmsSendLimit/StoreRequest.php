<?php

namespace App\Http\Requests\SmsSendLimit;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id' => 'required|unique:sms_send_limits|integer|min:1',
        ];
    }

    public function attributes()
    {
        return [
            'user_id' => __('message.sms_send_limit.user_id'),
        ];
    }
}
