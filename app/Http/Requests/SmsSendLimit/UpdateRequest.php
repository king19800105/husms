<?php

namespace App\Http\Requests\SmsSendLimit;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'mobile_day_limit'    => 'integer|min:0',
            'mobile_minute_limit' => 'integer|min:0',
            'day_limit'           => 'integer|min:0',
            'hour_limit'          => 'integer|min:0',
            'minute_limit'        => 'integer|min:0',
        ];
    }
}
