<?php

namespace App\Http\Requests\Menu;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     * 'name', 'slug', 'parent_id', 'url', 'url_alias', 'weight', 'type', 'icon', 'light', 'description'
     * @return array
     */
    public function rules()
    {
        return [
            'name'        => 'bail|required|string|between:1,25',
            'slug'        => 'string|between:1,25',
            'parent_id'   => 'integer',
            'url'         => 'string',
            'url_alias'   => 'string',
            'weight'      => 'integer|between:0,255',
            'type'        => 'string',
            'icon'        => 'string',
            'light'       => 'string',
            'description' => 'string|max:150'
        ];
    }

    public function attributes()
    {
        return [
            'name'        => __('message.menu.name'),
            'slug'        => __('message.menu.slug'),
            'parent_id'   => __('message.menu.parent_id'),
            'url'         => __('message.menu.url'),
            'url_alias'   => __('message.menu.url_alias'),
            'weight'      => __('message.menu.weight'),
            'type'        => __('message.menu.type'),
            'icon'        => __('message.menu.icon'),
            'light'       => __('message.menu.light'),
            'description' => __('message.menu.description'),
        ];
    }
}
