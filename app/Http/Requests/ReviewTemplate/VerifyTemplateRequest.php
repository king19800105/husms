<?php
/**
 * Created by PhpStorm.
 * User: kb130
 * Date: 2018/7/4
 * Time: 14:53
 */

namespace App\Http\Requests\ReviewTemplate;


use Illuminate\Foundation\Http\FormRequest;

class VerifyTemplateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'id'               => 'bail|required|integer|min:1',
            'user_id'          => 'required|integer|min:1',
            'sign'             => 'required|string|between:2,8',
            'content'          => 'required|string|between:2,190',
            'state'            => 'required|in:0,1,2',
            'channel_group_id' => 'required|integer',
            'only_channel_id'  => 'required|integer',
            'rejected_reason'  => 'string|between:2,190',
            'remark'           => 'string|between:2,190',
        ];
    }

    public function attributes()
    {
        return [
            'id'               => __('message.review_template.id'),
            'user_id'          => __('message.review_template.user_id'),
            'sign'             => __('message.review_template.sign'),
            'content'          => __('message.review_template.content'),
            'state'            => __('message.review_template.state'),
            'channel_group_id' => __('message.review_template.channel_group_id'),
            'only_channel_id'  => __('message.review_template.only_channel_id'),
            'rejected_reason'  => __('message.review_template.rejected_reason'),
            'remark'           => __('message.review_template.remark'),
        ];
    }
}