<?php

namespace App\Http\Requests\ReviewTemplate;

class UpdateRequest extends StoreRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $userId = $this->user()->id;

        return array_merge(parent::rules(), [
            'id'    => 'bail|required|integer|min:1',
            'title' => 'bail|required|unique:review_templates,title,'.$this->id.',id,user_id,'.$userId.'|between:2,50',
        ]);
    }

    public function attributes()
    {
        return array_merge(parent::attributes(), [
            'id' => __('message.review_template.id'),
        ]);
    }

    public function messages()
    {
        return [
            'id.min'     => '用户非法操作',
            'id.integer' => '用户非法操作',
        ];
    }
}
