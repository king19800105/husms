<?php

namespace App\Http\Requests\ReviewTemplate;

use Illuminate\Foundation\Http\FormRequest;

/**
 * 用户保存
 * Class StoreRequest
 * @package App\Http\Requests\ReviewTemplate
 */
class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $userId = $this->user()->id;

        return [
            // review_templates 表 title唯一，并且排除id为null的数据，并且user_id 也不能和$userId相同。
            'title'           => 'bail|required|unique:review_templates,title,NULL,id,user_id,'.$userId.'|between:2,50',
            'sign'            => 'required|string|between:2,8',
            'content'         => 'required|string|between:2,190',
        ];
    }

    public function attributes()
    {
        return [
            'title'           => __('message.review_template.title'),
            'sign'            => __('message.review_template.sign'),
            'content'         => __('message.review_template.content'),
        ];
    }
}
