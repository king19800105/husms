<?php

namespace App\Http\Requests\UserFund;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id'     => 'required|integer|min:1',
            'type'        => 'required|in:1,2,3,4,5',
            'sms_record'  => 'required|integer',
            'cash_record' => 'required|numeric',
        ];
    }

    public function attributes()
    {
        return [
            'user_id'     => __('message.user_fund.name'),
            'type'        => __('message.user_fund.type'),
            'sms_record'  => __('message.user_fund.sms_record'),
            'cash_record' => __('message.user_fund.cash_record'),
        ];
    }
}
