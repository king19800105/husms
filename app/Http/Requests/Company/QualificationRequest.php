<?php
/**
 * Created by PhpStorm.
 * User: anthony
 * Date: 2018/8/14
 * Time: 下午5:40
 */

namespace App\Http\Requests\Company;


use Illuminate\Foundation\Http\FormRequest;

class QualificationRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'user_id'           => 'required|integer|unique:companies',
            'company_name'      => 'required|string|between:2,255',
            'license_uri'       => 'required|string|between:2,255',
            'organization_uri'  => 'required|string|between:2,255',
            'id_card_front_uri' => 'required|string|between:2,255',
            'id_card_back_uri'  => 'required|string|between:2,255',
            'tax_register_uri'  => 'required|string|between:2,255',
            'site_url'          => 'required|url',
            'icp_uri'           => 'required|string|between:2,255',
            'sms_app_uri'       => 'required|string|between:2,255',
        ];
    }
}