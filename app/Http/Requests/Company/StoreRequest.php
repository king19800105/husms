<?php

namespace App\Http\Requests\Company;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id'             => 'required|integer|unique:companies',
            'qualification_state' => 'required|unique:companies,qualification_state,' . $this->qualification_state .'|in:0,1,2,3',
        ];
    }

    public function attributes()
    {
        return [
            'user_id'             => __('message.company.user_id'),
            'qualification_state' => __('message.company.qualification_state'),
        ];
    }
}
