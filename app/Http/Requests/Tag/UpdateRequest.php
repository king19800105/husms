<?php

namespace App\Http\Requests\Tag;

class UpdateRequest extends StoreRequest
{
    public function rules()
    {
        return array_merge(parent::rules(), [
            'name' => 'required|unique:tags,name,' . $this->name .'|between:2,150',
        ]);
    }
}
