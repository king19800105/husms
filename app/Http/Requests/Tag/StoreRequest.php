<?php

namespace App\Http\Requests\Tag;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'       => 'required|unique:tags|between:2,150',
            'alias_name' => 'string|between:2,150',
            'model_type' => 'string|between:2,150',
        ];
    }

    public function attributes()
    {
        return [
            'name'       => __('message.tag.name'),
            'alias_name' => __('message.tag.alias_name'),
            'model_type' => __('message.tag.model_type'),
        ];
    }
}
