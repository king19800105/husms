<?php

namespace App\Http\Requests\Article;

class UpdateRequest extends StoreRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            'title' => 'required|unique:articles,title,' . $this->article .'|between:2,150',
        ]);
    }
}
