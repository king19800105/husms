<?php

namespace App\Http\Requests\Article;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $mimes = config('common.fileupload.img_mimes');
        $fileMaxsize = config('common.fileupload.maxsize', '1024');

        return [
            'title'            => 'required|unique:articles|between:2,150',
            'author'           => 'required|string|between:2,50',
            'content_markdown' => 'required|string|min:2',
            'content_html'     => 'string',
            'category_id'      => 'required|integer|min:1',
            'intro'            => 'string|between:2,150',
            'img'          => 'image|mimes:' . $mimes.'|max:' . $fileMaxsize,
            'seo_title'        => 'string|between:2,100',
            'seo_keywords'     => 'string|between:2,100',
            'seo_description'  => 'string|between:4,180',
            'weight'           => 'integer|between:1,255',
            'is_top'           => 'boolean',
            'is_recommend'     => 'boolean',
            'is_hot'           => 'boolean',
            'state'            => 'required|boolean',
            'tag_id'           => 'array',
        ];
    }

    public function attributes()
    {
        return [
            'title'            => __('message.article.title'),
            'author'           => __('message.article.author'),
            'content_markdown' => __('message.article.content_markdown'),
            'content_html'     => __('message.article.content_html'),
            'category_id'      => __('message.article.category_id'),
            'intro'            => __('message.article.intro'),
            'img'              => __('message.article.img'),
            'seo_title'        => __('message.article.seo_title'),
            'seo_keywords'     => __('message.article.seo_keywords'),
            'seo_description'  => __('message.article.seo_description'),
            'weight'           => __('message.article.weight'),
            'is_top'           => __('message.article.is_top'),
            'is_recommend'     => __('message.article.is_recommend'),
            'is_hot'           => __('message.article.is_hot'),
            'state'            => __('message.article.state'),
            'tag_id'           => __('message.article.tag_id'),
        ];
    }
}
