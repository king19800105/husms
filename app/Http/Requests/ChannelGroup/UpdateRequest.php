<?php

namespace App\Http\Requests\ChannelGroup;

class UpdateRequest extends StoreRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            'group_name' => 'required|unique:channel_groups,group_name,' . $this->group_name .'|string',
        ]);
    }
}
