<?php

namespace App\Http\Requests\ChannelGroup;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'group_name' => 'required|string|max:255',
            'parent_id'  => 'required|integer|min:0',
            'state'      => 'required|integer|min:0',
            'weight'     => 'integer|min:0|max:99',
            'remark'     => 'string|between:2,255',
        ];
    }

    public function attributes()
    {
        return [
            'group_name' => __('message.channel_group.group_name'),
            'parent_id'  => __('message.channel_group.parent_id'),
            'state'      => __('message.channel_group.state'),
            'weight'     => __('message.channel_group.weight'),
            'remark'     => __('message.channel_group.remark'),
        ];
    }
}
