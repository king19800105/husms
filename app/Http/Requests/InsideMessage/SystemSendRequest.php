<?php

namespace App\Http\Requests\InsideMessage;

class SystemSendRequest extends PointSendRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            'condition'    => 'required|json',
        ]);
    }

    public function attributes()
    {
        return array_merge(parent::attributes(), [
            'condition' => __('message.inner_message.condition'),
        ]);
    }
}
