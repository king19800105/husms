<?php

namespace App\Http\Requests\InsideMessage;

use Illuminate\Foundation\Http\FormRequest;

class PointSendRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'        => 'required|between:2,255',
            'content'      => 'required|string',
            'recipient_id' => 'required|integer',
            'contact_way'  => 'string|between:2,255',
        ];
    }

    public function attributes()
    {
        return [
            'recipient_id' => __('message.inner_message.recipient_id'),
            'title'        => __('message.inner_message.title'),
            'content'      => __('message.inner_message.content'),
            'contact_way'  => __('message.inner_message.contact_way'),
        ];
    }
}
