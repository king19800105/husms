<?php

namespace App\Http\Requests\GoodsCategory;


class UpdateRequest extends StoreRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            'name' => 'required|unique:goods_categories,name,' . $this->goods_category . '|between:2,255',
        ]);
    }
}
