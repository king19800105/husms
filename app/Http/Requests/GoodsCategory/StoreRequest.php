<?php

namespace App\Http\Requests\GoodsCategory;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $mimes = config('common.fileupload.img_mimes');
        $fileMaxsize = config('common.fileupload.maxsize', '1024');

        return [
            'name' => 'required|between:2,255|unique:goods_categories',
            'parent_id' => 'required|integer|min:0',
            'alias_name' => 'string|between:2,255',
            'img' => 'image|mimes:' . $mimes.'|max:' . $fileMaxsize,
            'show_type' => 'integer|between:1,255',
            'state' => 'boolean',
            'weight' => 'integer|between:1,255',
            'seo_title' => 'string|between:2,100',
            'seo_keywords' => 'string|between:2,100',
            'seo_description' => 'string|between:4,255',
        ];
    }

    public function attributes()
    {
        return [
            'name'            => __('message.goods_category.name'),
            'alias_name'      => __('message.goods_category.alias_name'),
            'parent_id'       => __('message.goods_category.parent_id'),
            'show_type'       => __('message.goods_category.show_type'),
            'seo_keywords'    => __('message.goods_category.seo_keywords'),
            'seo_title'       => __('message.goods_category.seo_title'),
            'state'       => __('message.goods_category.state'),
            'seo_description' => __('message.goods_category.seo_description'),
            'weight'          => __('message.goods_category.weight'),
            'img'         => __('message.goods_category.img'),
        ];
    }
}
