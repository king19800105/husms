<?php

namespace App\Http\Requests\SignReport;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'sms_sign' => 'required|string|unique:sign_reports|between:2, 8',
        ];
    }

    public function attributes()
    {
        return [
            'sms_sign' => __('message.sign_report.sms_sign'),
        ];
    }
}
