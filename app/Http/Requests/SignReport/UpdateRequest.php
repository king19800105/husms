<?php

namespace App\Http\Requests\SignReport;

class UpdateRequest extends StoreRequest
{
    public function rules()
    {
        return array_merge(parent::rules(), [
            'sms_sign' => 'required|unique:sign_reports,sms_sign,' . $this->sms_sign .'|between:2,8',
        ]);
    }
}
