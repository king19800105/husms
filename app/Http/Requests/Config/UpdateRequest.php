<?php

namespace App\Http\Requests\Config;

class UpdateRequest extends StoreRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            'name' => 'required|unique:configs,name,' . $this->config .'|string',
        ]);
    }
}
