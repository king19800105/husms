<?php

namespace App\Http\Requests\Config;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'      => 'required|unique:configs|string',
            'show_name' => 'string',
            'info'      => 'required|string',
        ];
    }

    public function attributes()
    {
        return [
            'name'      => __('message.config.name'),
            'show_name' => __('message.config.show_name'),
            'info'      => __('message.config.info'),
        ];
    }
}
