<?php

namespace App\Http\Requests\Admin;

use App\Rules\ValidMobile;
use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'     => 'bail|required|unique:admins|alpha_dash|between:5,25',
            'password' => 'required|min:6|alpha_dash|confirmed',
            'password_confirmation' => '',
            'email'    => 'required|email|unique:admins|max:60',
            'mobile'   => ['required', 'max:20', 'unique:admins', new ValidMobile()],
            'remark'   => 'max:255',
        ];
    }

    public function attributes()
    {
        return [
            'name'     => __('message.admin.name'),
            'password' => __('message.admin.password'),
            'email'    => __('message.admin.email'),
            'remark'   => __('message.admin.remark'),
            'mobile'   => __('message.admin.mobile'),
            'password_confirmation' => __('message.admin.password_confirmation'),
        ];
    }
}
