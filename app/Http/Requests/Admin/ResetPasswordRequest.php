<?php

namespace App\Http\Requests\Admin;

use App\Rules\ValidPasswordEqual;
use Illuminate\Foundation\Http\FormRequest;

class ResetPasswordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return 0 === strcmp(request()->user()->id, $this->id);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'password'     => ['required', 'min:6', 'alpha_dash', new ValidPasswordEqual()],
            'new_password' => 'required|min:6|alpha_dash|confirmed',
            'new_password_confirmation' => '',
        ];
    }

    public function attributes()
    {
        return [
            'password'                  => __('message.admin.password'),
            'new_password'              => __('message.admin.new_password'),
            'new_password_confirmation' => __('message.admin.new_password_confirmation'),
        ];
    }
}
