<?php

namespace App\Http\Requests\Admin;


use App\Rules\ValidMobile;

class UpdateRequest extends StoreRequest
{
    public function authorize()
    {
        return 0 === strcmp($this->admin, request()->user()->id);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email'  => 'required|email|unique:admins,email,' . $this->admin .'|max:60',
            'mobile' => ['required', 'max:20', 'unique:admins,mobile,' . $this->admin . ',', new ValidMobile()],
            'remark' => 'max:255',
            'state' => 'integer|in:0,1,2',
        ];
    }

    public function attributes()
    {
        return array_merge(parent::attributes(), [
            'state' => __('message.admin.state'),
        ]);
    }
}
