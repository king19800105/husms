<?php

namespace App\Http\Requests\SmsUserList;

use App\Rules\ValidIpOrMobile;
use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'type'       => 'required|in:0,1',
            'bind_key'   => 'required|in:1,2',
            'bind_value' => ['required', 'string', 'between:11,15', new ValidIpOrMobile()]
        ];
    }

    public function attributes()
    {
        return [
            'user_id'    => __('message.sms_user_list.type'),
            'type'       => __('message.sms_user_list.type'),
            'bind_key'   => __('message.sms_user_list.bind_key'),
            'bind_value' => __('message.sms_user_list.bind_value'),
        ];
    }
}
