<?php

namespace App\Http\Requests\SmsUserList;


class UpdateRequest extends StoreRequest
{
    public function rules()
    {
        return array_merge(parent::rules(), [
            'user_id' => 'required|integer|min:1',
        ]);
    }
}
