<?php

namespace App\Http\Requests\Channel;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'             => ['required', 'string', 'unique:channels', 'between:2,255'],
            'access_type'      => 'required|string|between:2,50',
            'access_ip'        => 'required|ip|between:2,255',
            'channel_model_id' => 'required|integer',
            'is_sign_limit'    => 'required|in:0,1',
            'state'            => 'required|in:0,1',
            'weight'           => 'required|integer',
            'access_domain'    => 'string|between:2,255',
            'access_port'      => 'integer',
            'remark'           => 'string|between:2,255',
        ];
    }

    public function attributes()
    {
        return [
            'name'             => __('message.channel.name'),
            'group_id'         => __('message.channel.group_id'),
            'access_type'      => __('message.channel.access_type'),
            'access_domain'    => __('message.channel.access_domain'),
            'access_ip'        => __('message.channel.access_ip'),
            'access_port'      => __('message.channel.access_port'),
            'channel_model_id' => __('message.channel.channel_model_id'),
            'price'            => __('message.channel.price'),
            'is_sign_limit'    => __('message.channel.is_sign_limit'),
            'state'            => __('message.channel.state'),
            'weight'           => __('message.channel.weight'),
            'remark'           => __('message.channel.remark'),
        ];
    }
}
