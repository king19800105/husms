<?php
/**
 * Created by PhpStorm.
 * User: anthony
 * Date: 2018/8/8
 * Time: 下午12:40
 */

namespace App\Http\Requests\Channel;


use Illuminate\Foundation\Http\FormRequest;

class AssignRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

        ];
    }

    public function attributes()
    {
        return [
        ];
    }
}