<?php

namespace App\Http\Requests\Channel;


class UpdateRequest extends StoreRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            'name' => 'required|string|unique:channels,name,' . $this->name .'|between:2,255',
        ]);
    }
}
