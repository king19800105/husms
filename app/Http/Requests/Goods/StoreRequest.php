<?php

namespace App\Http\Requests\Goods;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $mimes = config('common.fileupload.img_mimes');
        $fileMaxsize = config('common.fileupload.maxsize', '1024');

        return [
            'name'              => 'required|unique:goods|between:4,255',
            'alias_name'        => 'string|between:2,255',
            'goods_category_id' => 'required',
            'content_markdown'  => 'required|string|min:1',
            'content_html'      => 'string',
            'shop_price'        => 'numeric',
            'market_price'      => 'numeric',
            'inventory'         => 'integer',
            'seo_title'         => 'string|between:2,100',
            'seo_keywords'      => 'string|between:2,100',
            'seo_description'   => 'string|between:4,255',
            'weight'            => 'integer|between:1,99',
            'state'             => 'required|integer|boolean',
            'intro'             => 'string|between:2,255',
            'img'               => 'image|mimes:' . $mimes . '|max:' . $fileMaxsize,
            'goods_model_id'    => 'integer',
            'goods_no'          => 'alpha_dash|unique:goods',
            'is_hot'            => 'boolean',
            'is_recommend'      => 'boolean',
            'is_new'            => 'boolean',
            'is_discount'       => 'boolean',
        ];
    }

    public function attributes()
    {
        return [
            'name'              => __('message.goods.name'),
            'alias_name'        => __('message.goods.alias_name'),
            'goods_category_id' => __('message.goods.goods_category_id'),
            'is_hot'            => __('message.goods.is_hot'),
            'is_new'            => __('message.goods.is_new'),
            'is_recommend'      => __('message.goods.is_recommend'),
            'is_discount'       => __('message.goods.is_discount'),
            'goods_model_id'    => __('message.goods.goods_model_id'),
            'goods_no'          => __('message.goods.goods_no'),
            'shop_price'        => __('message.goods.shop_price'),
            'inventory'         => __('message.goods.inventory'),
            'intro'             => __('message.goods.intro'),
            'content_markdown'  => __('message.goods.content_markdown'),
            'content_html'      => __('message.goods.content_html'),
            'market_price'      => __('message.goods.market_price'),
            'weight'            => __('message.goods.weight'),
            'state'             => __('message.goods.state'),
            'img'               => __('message.goods.img'),
            'seo_title'         => __('message.goods.seo_title'),
            'seo_keywords'      => __('message.goods.seo_keywords'),
            'seo_description'   => __('message.goods.seo_description'),
        ];
    }
}
