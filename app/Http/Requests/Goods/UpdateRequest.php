<?php

namespace App\Http\Requests\Goods;

class UpdateRequest extends StoreRequest
{
    public function rules()
    {
        return array_merge(parent::rules(), [
            'name' => 'required|unique:goods,name,' . $this->good .'|between:4,255',
        ]);
    }
}
