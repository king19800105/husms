<?php

namespace App\Http\Controllers\Backend;

use App\Http\Requests\Goods\StoreRequest;
use App\Http\Requests\Goods\UpdateRequest;
use App\Http\Responses\Goods\IndexResponse;
use App\Http\Responses\Goods\ShowResponse;
use App\Services\GoodsService;
use App\Http\Controllers\Controller;

/**
 * Class GoodsController
 * @package App\Http\Controllers\Backend
 */
class GoodsController extends Controller
{
    protected $goodsService;

    public function __construct(GoodsService $goodsService)
    {
        $this->goodsService = $goodsService;
    }

    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Contracts\Support\Responsable
    */
    public function index()
    {
        $list = $this->goodsService->getGoodsList();

        return new IndexResponse($list);
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(StoreRequest $request)
    {
        $data = $request->validated();

        return response_api($this->goodsService->storeGoods($data));
    }

    /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Contracts\Support\Responsable
    */
    public function show($id)
    {
        $data = $this->goodsService->getGoods($id);

        return new ShowResponse($data);
    }

    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Foundation\Http\FormRequest  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(UpdateRequest $request, $id)
    {
        $data = $request->validated();
        $bool = $this->goodsService->updateGoods($id, $data);

        return response_api($bool);
    }

    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function destroy($id)
    {
        $bool = $this->goodsService->deleteGoods($id);

        return response_api($bool);
    }
}
