<?php

namespace App\Http\Controllers\Backend;

use App\Http\Requests\SmsSendLimit\StoreRequest;
use App\Http\Requests\SmsSendLimit\UpdateRequest;
use App\Http\Responses\SmsSendLimit\IndexResponse;
use App\Http\Responses\SmsSendLimit\ShowResponse;
use App\Services\SmsSendLimitService;
use App\Http\Controllers\Controller;

/**
 * Class SmsSendLimitController
 * @package App\Http\Controllers\Backend
 */
class SmsSendLimitController extends Controller
{
    protected $smsSendLimitService;

    public function __construct(SmsSendLimitService $smsSendLimitService)
    {
        $this->smsSendLimitService = $smsSendLimitService;
    }

    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Contracts\Support\Responsable
    */
    public function index()
    {
        $list = $this->smsSendLimitService->getSmsSendLimits();

        return new IndexResponse($list);
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(StoreRequest $request)
    {
        $data = $request->validated();

        return response_api($this->smsSendLimitService->storeSmsSendLimit($data));
    }

    /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Contracts\Support\Responsable
    */
    public function show($id)
    {
        $data = $this->smsSendLimitService->getSmsSendLimit($id);

        return new ShowResponse($data);
    }

    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Foundation\Http\FormRequest  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(UpdateRequest $request, $id)
    {
        $data = $request->validated();
        $bool = $this->smsSendLimitService->updateSmsSendLimit($id, $data);

        return response_api($bool);
    }

    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function destroy($id)
    {
        $bool = $this->smsSendLimitService->deleteSmsSendLimit($id);

        return response_api($bool);
    }
}
