<?php

namespace App\Http\Controllers\Backend;

use App\Http\Requests\Admin\AssignRequest;
use App\Http\Requests\Admin\ResetPasswordRequest;
use App\Http\Requests\Admin\StoreRequest;
use App\Http\Requests\Admin\UpdateRequest;
use App\Http\Responses\Admin\IndexResponse;
use App\Http\Responses\Admin\InfoResponse;
use App\Http\Responses\Admin\ShowResponse;
use App\Http\Responses\Goods\OperationIndexResponse;
use App\Http\Responses\Goods\OperationShowResponse;
use App\Http\Responses\Goods\QueryIndexResponse;
use App\Http\Responses\Goods\QueryShowResponse;
use App\Services\AdminService;
use App\Http\Controllers\Controller;
use App\Services\LogService;
use Illuminate\Http\Request;

/**
 * 所有日志操作的统一控制器
 *
 * Class AdminController
 * @package App\Http\Controllers\Backend
 */
class LogController extends Controller
{
    protected $logService;

    public function __construct(LogService $logService)
    {
        $this->logService = $logService;
    }

    public function slowQueryList()
    {
        $list = $this->logService->getSlowQueries();

        return new QueryIndexResponse($list);
    }

    public function showSlowQuery($id)
    {
        $data = $this->logService->getSlowQuery($id);

        return new QueryShowResponse($data);
    }

    public function operationList()
    {
        $list = $this->logService->getOperations();

        return new OperationIndexResponse($list);
    }

    public function showOperation($id)
    {
        $data = $this->logService->getOperation($id);

        return new OperationShowResponse($data);
    }

    public function fixedSlowQuery($id)
    {
        $res = $this->fixedSlowQuery($id);

        return response_api($res);
    }
}