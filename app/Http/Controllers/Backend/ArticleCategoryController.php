<?php

namespace App\Http\Controllers\Backend;

use App\Http\Requests\ArticleCategory\StoreRequest;
use App\Http\Requests\ArticleCategory\UpdateRequest;
use App\Http\Responses\ArticleCategory\IndexResponse;
use App\Http\Responses\ArticleCategory\ShowResponse;
use App\Services\ArticleCategoryService;
use App\Http\Controllers\Controller;

/**
 * Class ArticleCategoryController
 * @package App\Http\Controllers\Backend
 */
class ArticleCategoryController extends Controller
{
    protected $articleCategoryService;

    public function __construct(ArticleCategoryService $articleCategoryService)
    {
        $this->articleCategoryService = $articleCategoryService;
    }

    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Contracts\Support\Responsable
    */
    public function index()
    {
        $list = $this->articleCategoryService->getArticleCategories();

        return new IndexResponse($list);
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(StoreRequest $request)
    {
        $data = $request->validated();

        return response_api($this->articleCategoryService->storeArticleCategory($data));
    }

    /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Contracts\Support\Responsable
    */
    public function show($id)
    {
        $data = $this->articleCategoryService->getArticleCategory($id);

        return new ShowResponse($data);
    }

    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Foundation\Http\FormRequest  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(UpdateRequest $request, $id)
    {
        $data = $request->validated();
        $bool = $this->articleCategoryService->updateArticleCategory($id, $data);

        return response_api($bool);
    }

    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function destroy($id)
    {
        $bool = $this->articleCategoryService->deleteArticleCategory($id);

        return response_api($bool);
    }
}
