<?php

namespace App\Http\Controllers\Backend;

use App\Http\Requests\Permission\StoreRequest;
use App\Http\Requests\Permission\UpdateRequest;
use App\Http\Responses\Permission\GroupResponse;
use App\Http\Responses\Permission\IndexResponse;
use App\Http\Responses\Permission\ShowResponse;
use App\Services\PermissionService;
use App\Http\Controllers\Controller;

/**
 * 权限控制器
 *
 * Class PermissionController
 * @package App\Http\Controllers\Backend
 */
class PermissionController extends Controller
{
    protected $permissionService;

    public function __construct(PermissionService $permissionService)
    {
        $this->permissionService = $permissionService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Support\Responsable
     */
    public function index()
    {
        $list = $this->permissionService->getPermissions();

        return new IndexResponse($list);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreRequest $request)
    {
        $data = $request->validated();

        return response_api($this->permissionService->storePermission($data));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\Support\Responsable
     */
    public function show($id)
    {
        $data = $this->permissionService->getPermission($id);

        return new ShowResponse($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, $id)
    {
        $data = $request->validated();
        $bool = $this->permissionService->updatePermission($id, $data);

        return response_api($bool);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $bool = $this->permissionService->deletePermission($id);

        return response_api($bool);
    }

    public function showOnGroup()
    {
        $result = $this->permissionService->getPermissionsOnGroup();

        return new GroupResponse($result);
    }
}
