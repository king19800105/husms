<?php

namespace App\Http\Controllers\Backend;

use App\Http\Requests\SmsReply\StoreRequest;
use App\Http\Requests\SmsReply\UpdateRequest;
use App\Http\Responses\SmsReply\IndexResponse;
use App\Http\Responses\SmsReply\ShowResponse;
use App\Services\SmsReplyService;
use App\Http\Controllers\Controller;

/**
 * Class SmsReplyController
 * @package App\Http\Controllers\Backend
 */
class SmsReplyController extends Controller
{
    protected $smsReplyService;

    public function __construct(SmsReplyService $smsReplyService)
    {
        $this->smsReplyService = $smsReplyService;
    }

    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Contracts\Support\Responsable
    */
    public function index()
    {
        $list = $this->smsReplyService->getSmsReplies();

        return new IndexResponse($list);
    }

    /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Contracts\Support\Responsable
    */
    public function show($id)
    {
        $data = $this->smsReplyService->getSmsReply($id);

        return new ShowResponse($data);
    }
}
