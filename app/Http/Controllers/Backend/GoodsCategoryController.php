<?php

namespace App\Http\Controllers\Backend;

use App\Http\Requests\GoodsCategory\StoreRequest;
use App\Http\Requests\GoodsCategory\UpdateRequest;
use App\Http\Responses\GoodsCategory\IndexResponse;
use App\Http\Responses\GoodsCategory\ShowResponse;
use App\Services\GoodsCategoryService;
use App\Http\Controllers\Controller;

/**
 * Class GoodsCategoryController
 * @package App\Http\Controllers\Backend
 */
class GoodsCategoryController extends Controller
{
    protected $goodsCategoryService;

    public function __construct(GoodsCategoryService $goodsCategoryService)
    {
        $this->goodsCategoryService = $goodsCategoryService;
    }

    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Contracts\Support\Responsable
    */
    public function index()
    {
        $list = $this->goodsCategoryService->getGoodsCategories();

        return new IndexResponse($list);
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(StoreRequest $request)
    {
        $data = $request->validated();

        return response_api($this->goodsCategoryService->storeGoodsCategory($data));
    }

    /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Contracts\Support\Responsable
    */
    public function show($id)
    {
        $data = $this->goodsCategoryService->getGoodsCategory($id);

        return new ShowResponse($data);
    }

    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Foundation\Http\FormRequest  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(UpdateRequest $request, $id)
    {
        $data = $request->validated();
        $bool = $this->goodsCategoryService->updateGoodsCategory($id, $data);

        return response_api($bool);
    }

    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function destroy($id)
    {
        $bool = $this->goodsCategoryService->deleteGoodsCategory($id);

        return response_api($bool);
    }
}
