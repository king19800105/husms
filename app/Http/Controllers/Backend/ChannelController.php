<?php

namespace App\Http\Controllers\Backend;

use App\Http\Requests\Channel\AssignRequest;
use App\Http\Requests\Channel\StoreRequest;
use App\Http\Requests\Channel\UpdateRequest;
use App\Http\Responses\Channel\IndexResponse;
use App\Http\Responses\Channel\ShowResponse;
use App\Services\ChannelService;
use App\Http\Controllers\Controller;

/**
 * Class ChannelController
 * @package App\Http\Controllers\Backend
 */
class ChannelController extends Controller
{
    protected $channelService;

    public function __construct(ChannelService $channelService)
    {
        $this->channelService = $channelService;
    }

    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Contracts\Support\Responsable
    */
    public function index()
    {
        $list = $this->channelService->getChannels();

        return new IndexResponse($list);
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(StoreRequest $request)
    {
        $data = $request->validated();

        return response_api($this->channelService->storeChannel($data));
    }

    /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Contracts\Support\Responsable
    */
    public function show($id)
    {
        $data = $this->channelService->getChannel($id);

        return new ShowResponse($data);
    }

    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Foundation\Http\FormRequest  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(UpdateRequest $request, $id)
    {
        $data = $request->validated();
        $bool = $this->channelService->updateChannel($id, $data);

        return response_api($bool);
    }

    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function destroy($id)
    {
        $bool = $this->channelService->deleteChannel($id);

        return response_api($bool);
    }

    public function assign(AssignRequest $request)
    {

    }
}
