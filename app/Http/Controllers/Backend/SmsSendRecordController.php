<?php

namespace App\Http\Controllers\Backend;

use App\Http\Requests\SmsSendRecord\StoreRequest;
use App\Http\Requests\SmsSendRecord\UpdateRequest;
use App\Http\Responses\SmsSendRecord\IndexResponse;
use App\Http\Responses\SmsSendRecord\ShowResponse;
use App\Services\SmsSendRecordService;
use App\Http\Controllers\Controller;

/**
 * Class SmsSendRecordController
 * @package App\Http\Controllers\Backend
 */
class SmsSendRecordController extends Controller
{
    protected $smsSendRecordService;

    public function __construct(SmsSendRecordService $smsSendRecordService)
    {
        $this->smsSendRecordService = $smsSendRecordService;
    }

    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Contracts\Support\Responsable
    */
    public function index()
    {
        $list = $this->smsSendRecordService->getSmsSendRecords();

        return new IndexResponse($list);
    }

    /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Contracts\Support\Responsable
    */
    public function show($id)
    {
        $data = $this->smsSendRecordService->getSmsSendRecord($id);

        return new ShowResponse($data);
    }
}
