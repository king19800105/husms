<?php

namespace App\Http\Controllers\Backend;

use App\Http\Requests\SignReport\StoreRequest;
use App\Http\Requests\SignReport\UpdateRequest;
use App\Http\Responses\SignReport\IndexResponse;
use App\Http\Responses\SignReport\ShowResponse;
use App\Services\SignReportService;
use App\Http\Controllers\Controller;

/**
 * Class SignReportController
 * @package App\Http\Controllers\Backend
 */
class SignReportController extends Controller
{
    protected $signReportService;

    public function __construct(SignReportService $signReportService)
    {
        $this->signReportService = $signReportService;
    }

    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Contracts\Support\Responsable
    */
    public function index()
    {
        $list = $this->signReportService->getSignReports();

        return new IndexResponse($list);
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(StoreRequest $request)
    {
        $data = $request->validated();

        return response_api($this->signReportService->storeSignReport($data));
    }

    /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Contracts\Support\Responsable
    */
    public function show($id)
    {
        $data = $this->signReportService->getSignReport($id);

        return new ShowResponse($data);
    }

    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Foundation\Http\FormRequest  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(UpdateRequest $request, $id)
    {
        $data = $request->validated();
        $bool = $this->signReportService->updateSignReport($id, $data);

        return response_api($bool);
    }

    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function destroy($id)
    {
        $bool = $this->signReportService->deleteSignReport($id);

        return response_api($bool);
    }
}
