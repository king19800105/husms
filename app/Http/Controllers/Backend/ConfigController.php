<?php

namespace App\Http\Controllers\Backend;

use App\Http\Requests\Config\StoreRequest;
use App\Http\Requests\Config\UpdateRequest;
use App\Http\Responses\Config\IndexResponse;
use App\Http\Responses\Config\ShowResponse;
use App\Services\ConfigService;
use App\Http\Controllers\Controller;

/**
 * 配置信息控制器
 *
 * Class ConfigController
 * @package App\Http\Controllers\Backend
 */
class ConfigController extends Controller
{
    protected $configService;

    public function __construct(ConfigService $configService)
    {
        $this->configService = $configService;
    }

    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Contracts\Support\Responsable
    */
    public function index()
    {
        $list = $this->configService->getConfigs();

        return new IndexResponse($list);
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(StoreRequest $request)
    {
        $data = $request->validated();

        return response_api($this->configService->storeConfig($data));
    }

    /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Contracts\Support\Responsable
    */
    public function show($id)
    {
        $data = $this->configService->getConfig($id);

        return new ShowResponse($data);
    }

    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Foundation\Http\FormRequest  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(UpdateRequest $request, $id)
    {
        $data = $request->validated();
        $bool = $this->configService->updateConfig($id, $data);

        return response_api($bool);
    }

    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function destroy($id)
    {
        $bool = $this->configService->deleteConfig($id);

        return response_api($bool);
    }
}
