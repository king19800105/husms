<?php

namespace App\Http\Controllers\Backend;

use App\Http\Requests\Admin\AssignRequest;
use App\Http\Requests\Admin\ResetPasswordRequest;
use App\Http\Requests\Admin\StoreRequest;
use App\Http\Requests\Admin\UpdateRequest;
use App\Http\Responses\Admin\IndexResponse;
use App\Http\Responses\Admin\InfoResponse;
use App\Http\Responses\Admin\RoleResponse;
use App\Http\Responses\Admin\ShowResponse;
use App\Services\AdminService;
use App\Http\Controllers\Controller;
use App\Services\MenuService;
use Illuminate\Http\Request;

/**
 * 管理员控制器
 *
 * Class AdminController
 * @package App\Http\Controllers\Backend
 */
class AdminController extends Controller
{
    /**
     * @var AdminService
     */
    protected $adminService;

    protected $menuService;

    public function __construct(AdminService $adminService, MenuService $menuService)
    {
        $this->adminService = $adminService;
        $this->menuService = $menuService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Support\Responsable
     */
    public function index()
    {
        $list = $this->adminService->getAdmins();

        return new IndexResponse($list);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreRequest $request
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function store(StoreRequest $request)
    {
        $data = $request->validated();

        return response_api($this->adminService->storeAdmin($data));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\Support\Responsable
     */
    public function show($id)
    {
        $data = $this->adminService->getAdmin($id);

        return new ShowResponse($data);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param UpdateRequest $request
     * @param $id
     * @return bool
     */
    public function update(UpdateRequest $request, $id)
    {
        $data = $request->validated();
        $bool = $this->adminService->updateAdmin($id, $data);

        return response_api($bool);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $bool = $this->adminService->deleteAdmin($id);

        return response_api($bool);
    }

    /**
     * 修改密码操作
     *
     * @param ResetPasswordRequest $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function resetPassword(ResetPasswordRequest $request, $id)
    {
        $passwordData = $request->only('new_password');
        $bool = $this->adminService->updateAdmin($id, $passwordData);

        return response_api($bool);
    }

    /**
     * 获取当前用户基础信息
     *
     * @param Request $request
     * @return InfoResponse
     */
    public function currentAdminInfo(Request $request)
    {
        $admin = $request->user();
        // $permissions = $this->adminService->getPermissionRelatedInfo($admin);
        // $menus = $this->menuService->getMenuListByPermission($permissions);
        // $total = ['permissions' => $permissions, 'menus' => $menus, 'admin' => $admin];
        return new InfoResponse($admin);
    }

    /**
     * 分配角色或者权限
     *
     * @param AssignRequest $request
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function assign(AssignRequest $request)
    {
        $data = $request->validated();
        $result = $this->adminService->assignByType($data);

        return response_api($result);
    }

    public function getAdminRolesById($id)
    {
        $adminRoles = $data = $this->adminService->findAdminRolesById($id);

        return new RoleResponse($adminRoles);
    }

}