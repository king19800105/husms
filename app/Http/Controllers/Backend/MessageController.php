<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\InsideMessage\PointSendRequest;
use App\Http\Requests\InsideMessage\SystemSendRequest;
use App\Http\Responses\InsideMessage\IndexResponse;
use App\Http\Responses\InsideMessage\ShowResponse;
use App\Services\InsideMessageService;
use Illuminate\Contracts\Support\Responsable;

/**
 * 所有类型信息控制器
 *
 * Class MessageController
 * @package App\Http\Controllers\Backend
 */
class MessageController extends Controller
{
    protected $insideMessageService;

    public function __construct(InsideMessageService $insideMessageService)
    {
        $this->insideMessageService = $insideMessageService;
    }

    /**
     * 点对点单条发送
     * 用于管理员 -> 用户 或 用户 -> 管理员
     *
     * @param PointSendRequest $request
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function sendInsidePoint(PointSendRequest $request)
    {
        $data = $request->validated();
        $result = $this->insideMessageService->sendPointToPointMessage($data);

        return response_api($result);
    }

    /**
     * 系统批量群发站内信
     * 可以根据传递的条件，有选择性的进行推送。
     *
     * @param SystemSendRequest $request
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function sendInsideSystem(SystemSendRequest $request)
    {
        $data = $request->validated();
        $result = $this->insideMessageService->sendSystemInfoByCondition($data);

        return response_api($result);
    }

    /**
     * 取消点对点的某条信息
     *
     * @param $id
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function cancelInsideMessage($id)
    {
        $result = $this->insideMessageService->cancelPointToPointMessage($id);

        return response_api($result);
    }

    /**
     * 站内信息设置为已读, 并且获取数据详细数据
     *
     * @param $id
     * @return Responsable
     */
    public function readInsideMessage($id): Responsable
    {
        $result = [];
        $readOk = $this->insideMessageService->readMessageById($id);

        if ($readOk) {
            $result = $this->insideMessageService->getMessageById($id);
        }

        return new ShowResponse($result);
    }

    /**
     * 信息列表
     *
     * @param $type
     * @return Responsable
     */
    public function showInsideMessageList($type): Responsable
    {
        $typeValue = $this->insideMessageService->getRealMessageType($type);
        $result = $this->insideMessageService->getMessageListByType($typeValue);

        return new IndexResponse($result, $type);
    }
}
