<?php

namespace App\Http\Controllers\Backend;

use App\Http\Requests\SmsUserList\StoreRequest;
use App\Http\Requests\SmsUserList\UpdateRequest;
use App\Http\Responses\SmsUserList\IndexResponse;
use App\Http\Responses\SmsUserList\ShowResponse;
use App\Services\SmsUserListService;
use App\Http\Controllers\Controller;

/**
 * Class SmsUserListController
 * @package App\Http\Controllers\Backend
 */
class SmsUserListController extends Controller
{
    protected $smsUserListService;

    public function __construct(SmsUserListService $smsUserListService)
    {
        $this->smsUserListService = $smsUserListService;
    }

    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Contracts\Support\Responsable
    */
    public function index()
    {
        $list = $this->smsUserListService->getSmsUserLists();

        return new IndexResponse($list);
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(StoreRequest $request)
    {
        $data = $request->validated();

        return response_api($this->smsUserListService->storeSmsUserList($data));
    }

    /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Contracts\Support\Responsable
    */
    public function show($id)
    {
        $data = $this->smsUserListService->getSmsUserList($id);

        return new ShowResponse($data);
    }

    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Foundation\Http\FormRequest  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(UpdateRequest $request, $id)
    {
        $data = $request->validated();
        $bool = $this->smsUserListService->updateSmsUserList($id, $data);

        return response_api($bool);
    }

    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function destroy($id)
    {
        $bool = $this->smsUserListService->deleteSmsUserList($id);

        return response_api($bool);
    }
}
