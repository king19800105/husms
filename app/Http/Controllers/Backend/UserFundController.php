<?php

namespace App\Http\Controllers\Backend;

use App\Http\Requests\UserFund\StoreRequest;
use App\Http\Requests\UserFund\UpdateRequest;
use App\Http\Responses\UserFund\IndexResponse;
use App\Http\Responses\UserFund\ShowResponse;
use App\Services\UserFundService;
use App\Http\Controllers\Controller;

/**
 * Class UserFundController
 * @package App\Http\Controllers\Backend
 */
class UserFundController extends Controller
{
    protected $userFundService;

    public function __construct(UserFundService $userFundService)
    {
        $this->userFundService = $userFundService;
    }

    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Contracts\Support\Responsable
    */
    public function index()
    {
        $list = $this->userFundService->getUserFunds();

        return new IndexResponse($list);
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(StoreRequest $request)
    {
        $data = $request->validated();

        return response_api($this->userFundService->storeUserFund($data));
    }

    /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Contracts\Support\Responsable
    */
    public function show($id)
    {
        $data = $this->userFundService->getUserFund($id);

        return new ShowResponse($data);
    }

    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Foundation\Http\FormRequest  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(UpdateRequest $request, $id)
    {
        $data = $request->validated();
        $bool = $this->userFundService->updateUserFund($id, $data);

        return response_api($bool);
    }

    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function destroy($id)
    {
        $bool = $this->userFundService->deleteUserFund($id);

        return response_api($bool);
    }
}
