<?php

namespace App\Http\Controllers\Backend;

use App\Exceptions\ArriveLimitException;
use App\Http\Requests\ReviewTemplate\StoreRequest;
use App\Http\Requests\ReviewTemplate\UpdateRequest;
use App\Http\Requests\ReviewTemplate\VerifyTemplateRequest;
use App\Http\Responses\ReviewTemplate\IndexResponse;
use App\Http\Responses\ReviewTemplate\ShowResponse;
use App\Services\ReviewTemplateService;
use App\Http\Controllers\Controller;

/**
 * 短信模板审核
 *
 * Class ReviewTemplateController
 * @package App\Http\Controllers\Backend
 */
class ReviewTemplateController extends Controller
{
    protected $reviewTemplateService;

    public function __construct(ReviewTemplateService $reviewTemplateService)
    {
        $this->reviewTemplateService = $reviewTemplateService;
    }

    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Contracts\Support\Responsable
    */
    public function index()
    {
        $list = $this->reviewTemplateService->getReviewTemplates();

        return new IndexResponse($list);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param StoreRequest $request
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function store(StoreRequest $request)
    {
        throw_if(
            $this->reviewTemplateService->isToLimit(),
            new ArriveLimitException(__('exception.tpl_limit'))
        );

        $data = $request->validated();
        $result = $this->reviewTemplateService->storeReviewTemplate($data);

        return response_api($result);
    }

    /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Contracts\Support\Responsable
    */
    public function show($id)
    {
        $data = $this->reviewTemplateService->getReviewTemplate($id);

        return new ShowResponse($data);
    }

    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Foundation\Http\FormRequest  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(UpdateRequest $request, $id)
    {
        $data = $request->validated();
        $bool = $this->reviewTemplateService->updateReviewTemplate($id, $data);

        return response_api($bool);
    }

    /**
     * 审核模板
     *
     * @param VerifyTemplateRequest $verifyTemplateRequest
     * @param $id
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function verifyTemplate(VerifyTemplateRequest $verifyTemplateRequest, $id)
    {
        $data = $verifyTemplateRequest->validated();
        $bool = $this->reviewTemplateService->updateReviewTemplate($id, $data);

        return response_api($bool);
    }

    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function destroy($id)
    {
        $bool = $this->reviewTemplateService->deleteReviewTemplate($id);

        return response_api($bool);
    }
}
