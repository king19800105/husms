<?php

namespace App\Http\Controllers\Backend;

use App\Http\Requests\ChannelGroup\StoreRequest;
use App\Http\Requests\ChannelGroup\UpdateRequest;
use App\Http\Responses\ChannelGroup\IndexResponse;
use App\Http\Responses\ChannelGroup\ShowResponse;
use App\Services\ChannelGroupService;
use App\Http\Controllers\Controller;

/**
 * Class ChannelGroupController
 * @package App\Http\Controllers\Backend
 */
class ChannelGroupController extends Controller
{
    protected $channelGroupService;

    public function __construct(ChannelGroupService $channelGroupService)
    {
        $this->channelGroupService = $channelGroupService;
    }

    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Contracts\Support\Responsable
    */
    public function index()
    {
        $list = $this->channelGroupService->getChannelGroups();

        return new IndexResponse($list);
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(StoreRequest $request)
    {
        $data = $request->validated();

        return response_api($this->channelGroupService->storeChannelGroup($data));
    }

    /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Contracts\Support\Responsable
    */
    public function show($id)
    {
        $data = $this->channelGroupService->getChannelGroup($id);

        return new ShowResponse($data);
    }

    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Foundation\Http\FormRequest  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(UpdateRequest $request, $id)
    {
        $data = $request->validated();
        $bool = $this->channelGroupService->updateChannelGroup($id, $data);

        return response_api($bool);
    }

    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function destroy($id)
    {
        $bool = $this->channelGroupService->deleteChannelGroup($id);

        return response_api($bool);
    }
}
