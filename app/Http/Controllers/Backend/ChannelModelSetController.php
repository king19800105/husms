<?php

namespace App\Http\Controllers\Backend;

use App\Http\Requests\ChannelModelSet\StoreRequest;
use App\Http\Requests\ChannelModelSet\UpdateRequest;
use App\Http\Responses\ChannelModelSet\IndexResponse;
use App\Http\Responses\ChannelModelSet\ShowResponse;
use App\Services\ChannelModelSetService;
use App\Http\Controllers\Controller;

/**
 * Class ChannelModelSetController
 * @package App\Http\Controllers\Backend
 */
class ChannelModelSetController extends Controller
{
    protected $channelModelSetService;

    public function __construct(ChannelModelSetService $channelModelSetService)
    {
        $this->channelModelSetService = $channelModelSetService;
    }

    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Contracts\Support\Responsable
    */
    public function index()
    {
        $list = $this->channelModelSetService->getChannelModelSets();

        return new IndexResponse($list);
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(StoreRequest $request)
    {
        $data = $request->validated();

        return response_api($this->channelModelSetService->storeChannelModelSet($data));
    }

    /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Contracts\Support\Responsable
    */
    public function show($id)
    {
        $data = $this->channelModelSetService->getChannelModelSet($id);

        return new ShowResponse($data);
    }

    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Foundation\Http\FormRequest  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(UpdateRequest $request, $id)
    {
        $data = $request->validated();
        $bool = $this->channelModelSetService->updateChannelModelSet($id, $data);

        return response_api($bool);
    }

    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function destroy($id)
    {
        $bool = $this->channelModelSetService->deleteChannelModelSet($id);

        return response_api($bool);
    }
}
