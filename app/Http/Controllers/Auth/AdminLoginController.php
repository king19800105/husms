<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Phpno1\Passport\Traits\TokenAuthenticatesUsers;

class AdminLoginController extends Controller
{
    use TokenAuthenticatesUsers;

    protected $maxAttempts = 3;

    protected $decayMinutes = 60;

    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function guard()
    {
        return 'api_admin';
    }

    protected function authorization()
    {
        return [
            'grant_type'    => 'password',
            'client_id'     => 2,
            'client_secret' => 'IRjbX0rJkziAXEW4vQ2TpkfeyUNFzS814cSmiWBd',
            'scope'         => 'client-backend',
        ];
    }

    public function loginAuthorization()
    {
        $authInfo = $this->login();

        return response_api(json_decode($authInfo, true));
    }

    public function cancelAuthorization()
    {
        $this->logout();

        return response_api(__('auth.logout'));
    }

    protected function authenticated(Request $request, $user)
    {
        $user->update(['last_logged_ip' => $request->getClientIp()]);
    }
}
