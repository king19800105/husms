<?php

namespace App\Http\Responses\Permission;

use App\Models\Permission;
use Illuminate\Contracts\Support\Responsable;

class IndexResponse implements Responsable
{
    protected $result;

    public function __construct($result)
    {
        $this->result = $result;
    }

    public function toResponse($request)
    {
        return response_api($this->transform());
    }

    protected function transform()
    {
        $this->result->getCollection()->transform(function (Permission $permission) {
            return [
                'id'             => $permission->id,
                'name'           => $permission->name,
                'guard_name'     => $permission->guard_name,
                'show_name'      => $permission->show_name,
                'created_at'     => $permission->created_at->diffForHumans(),
                'updated_at'     => $permission->updated_at->diffForHumans(),
            ];
        });

        return $this->result;
    }
}