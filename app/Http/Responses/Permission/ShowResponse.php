<?php

namespace App\Http\Responses\Permission;

use Illuminate\Contracts\Support\Responsable;

class ShowResponse implements Responsable
{
    protected $result;

    public function __construct($result)
    {
        $this->result = $result;
    }

    public function toResponse($request)
    {
        return response_api($this->transform());
    }

    protected function transform()
    {
        return [
            'name'       => $this->result->name,
            'guard_name' => $this->result->guard_name,
            'show_name'  => $this->result->show_name,
            'updated_at' => $this->result->updated_at->format('Y-m-d H:i:s'),
            'created_at' => $this->result->created_at->format('Y-m-d H:i:s'),
        ];
    }
}