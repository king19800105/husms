<?php
/**
 * Created by PhpStorm.
 * User: anthony
 * Date: 2018/9/26
 * Time: 上午9:27
 */

namespace App\Http\Responses\Permission;


use App\Models\Permission;
use Illuminate\Contracts\Support\Responsable;

class GroupResponse implements Responsable
{

    protected $result;

    public function __construct($result)
    {
        $this->result = $result;
    }

    public function toResponse($request)
    {
        return response_api($this->transform());
    }

    protected function transform()
    {
        $this->result->transform(function ($item, $key) {
            $group = ['name' => $key];
            $group['child'] = $item->transform(function (Permission $permission) {
                return [
                    'id'         => $permission->id,
                    'name'       => $permission->name,
                    'guard_name' => $permission->guard_name,
                    'show_name'  => $permission->show_name,
                ];
            });

            return $group;
        });

        return array_values($this->result->toArray());
    }
}