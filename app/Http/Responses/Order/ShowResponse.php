<?php

namespace App\Http\Responses\Order;

use Illuminate\Contracts\Support\Responsable;

class ShowResponse implements Responsable
{
    protected $result;

    public function __construct($result)
    {
        $this->result = $result;
    }

    public function toResponse($request)
    {
        return response_api($this->transform());
    }

    protected function transform()
    {
        return [
            'order_id'       => $this->result->order_id,
            'user_id'        => $this->result->user_id,
            'goods_id'       => $this->result->goods_id,
            'goods_name'     => $this->result->goods_name,
            'goods_number'   => $this->result->goods_number,
            'order_type'     => $this->result->order_type,
            'state'          => $this->result->state,
            'pay_type'       => $this->result->pay_type,
            'ori_price'      => $this->result->ori_price,
            'discount_price' => $this->result->discount_price,
            'actual_price'   => $this->result->actual_price,
            'remark'         => $this->result->remark,
            'pay_time'       => $this->result->pay_time->format('Y-m-d H:i:s'),
            'source'         => $this->result->source,
            'decision_time'  => $this->result->decision_time->format('Y-m-d H:i:s'),
            'refund_time'    => $this->result->decision_time->format('Y-m-d H:i:s'),
            'created_at'     => $this->result->created_at->format('Y-m-d H:i:s'),
            'updated_at'     => $this->result->updated_at->format('Y-m-d H:i:s'),
        ];
    }
}