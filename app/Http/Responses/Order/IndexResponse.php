<?php

namespace App\Http\Responses\Order;

use App\Models\Order;
use Illuminate\Contracts\Support\Responsable;

class IndexResponse implements Responsable
{
    protected $result;

    public function __construct($result)
    {
        $this->result = $result;
    }

    public function toResponse($request)
    {
        return response_api($this->transform());
    }

    protected function transform()
    {
        $this->result->getCollection()->transform(function (Order $order) {
            return [
                'order_id'     => $order->order_id,
                'user_id'      => $order->user_id,
                'goods_name'   => $order->goods_name,
                'goods_number' => $order->goods_number,
                'order_type'   => $order->order_type,
                'state'        => $order->state,
                'created_at'   => $order->created_at->diffForHumans(),
            ];
        });

        return $this->result;
    }
}