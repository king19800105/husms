<?php

namespace App\Http\Responses\Article;

use Illuminate\Contracts\Support\Responsable;

class ShowResponse implements Responsable
{
    protected $result;

    public function __construct($result)
    {
        $this->result = $result;
    }

    public function toResponse($request)
    {
        return response_api($this->transform());
    }

    protected function transform()
    {
        return [
            'title'            => $this->result->title,
            'author'           => $this->result->author,
            'content_markdown' => $this->result->content_markdown,
            'content_html'     => $this->result->content_html,
            'category_id'      => $this->result->category_id,
            'intro'            => $this->result->intro,
            'img'          => $this->result->img,
            'seo_title'        => $this->result->seo_title,
            'seo_keywords'     => $this->result->seo_keywords,
            'seo_description'  => $this->result->seo_description,
            'weight'           => $this->result->weight,
            'is_top'           => $this->result->is_top,
            'is_recommend'     => $this->result->is_recommend,
            'is_hot'           => $this->result->is_hot,
            'state'            => $this->result->state,
            'tag_id'           => $this->result->tag_id,
            'created_at'       => $this->result->created_at->format('Y-m-d H:i:s'),
            'updated_at'       => $this->result->updated_at->format('Y-m-d H:i:s'),
        ];
    }
}