<?php

namespace App\Http\Responses\Article;

use App\Models\Article;
use Illuminate\Contracts\Support\Responsable;

class IndexResponse implements Responsable
{
    protected $result;

    public function __construct($result)
    {
        $this->result = $result;
    }

    public function toResponse($request)
    {
        return response_api($this->transform());
    }

    protected function transform()
    {
        $this->result->getCollection()->transform(function (Article $article) {
            return [
                'title'            => $article->title,
                'author'           => $article->author,
                'img'              => $article->img,
                'is_top'           => $article->is_top,
                'is_recommend'     => $article->is_recommend,
                'is_hot'           => $article->is_hot,
                'state'            => $article->state,
                'created_at'       => $article->created_at->diffForHumans(),
                'updated_at'       => $article->updated_at->diffForHumans(),
            ];
        });

        return $this->result;
    }
}