<?php

namespace App\Http\Responses\Goods;

use App\Models\Goods;
use Illuminate\Contracts\Support\Responsable;

class IndexResponse implements Responsable
{
    protected $result;

    public function __construct($result)
    {
        $this->result = $result;
    }

    public function toResponse($request)
    {
        return response_api($this->transform());
    }

    protected function transform()
    {
        $this->result->getCollection()->transform(function (Goods $goods) {
            return [
                'name'              => $goods->name,
                'alias_name'        => $goods->alias_name,
                'goods_category_id' => $goods->goods_category_id,
                'goods_model_id'    => $goods->goods_model_id,
                'inventory'         => $goods->inventory,
                'shop_price'        => $goods->shop_price,
                'state'             => $goods->state,
                'created_at'        => $goods->created_at->diffForHumans(),
                'updated_at'        => $goods->updated_at->diffForHumans(),
            ];
        });

        return $this->result;
    }
}