<?php

namespace App\Http\Responses\Goods;

use Illuminate\Contracts\Support\Responsable;

class ShowResponse implements Responsable
{
    protected $result;

    public function __construct($result)
    {
        $this->result = $result;
    }

    public function toResponse($request)
    {
        return response_api($this->transform());
    }

    protected function transform()
    {
        return [
            'name'              => $this->result->name,
            'alias_name'        => $this->result->alias_name,
            'goods_category_id' => $this->result->goods_category_id,
            'goods_no'          => $this->result->goods_no,
            'content_html'      => $this->result->content_html,
            'goods_model_id'    => $this->result->goods_model_id,
            'content_markdown'  => $this->result->content_markdown,
            'shop_price'        => $this->result->shop_price,
            'inventory'         => $this->result->inventory,
            'is_hot'            => $this->result->is_hot,
            'category_id'       => $this->result->category_id,
            'intro'             => $this->result->intro,
            'img'               => $this->result->img,
            'seo_title'         => $this->result->seo_title,
            'seo_keywords'      => $this->result->seo_keywords,
            'seo_description'   => $this->result->seo_description,
            'market_price'      => $this->result->market_price,
            'weight'            => $this->result->weight,
            'is_top'            => $this->result->is_top,
            'is_recommend'      => $this->result->is_recommend,
            'state'             => $this->result->state,
            'is_new'            => $this->result->is_new,
            'created_at'        => $this->result->created_at->format('Y-m-d H:i:s'),
            'updated_at'        => $this->result->updated_at->format('Y-m-d H:i:s'),
        ];
    }
}