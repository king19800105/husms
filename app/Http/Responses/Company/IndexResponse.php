<?php

namespace App\Http\Responses\Company;

use App\Models\Company;
use Illuminate\Contracts\Support\Responsable;

class IndexResponse implements Responsable
{
    protected $result;

    public function __construct($result)
    {
        $this->result = $result;
    }

    public function toResponse($request)
    {
        return response_api($this->transform());
    }

    protected function transform()
    {
        $this->result->getCollection()->transform(function (Company $company) {
            return [
                'user_id'             => $company->user_id,
                'company_name'        => $company->company_name,
                'qualification_state' => $company->qualification_state,
                'site_url'            => $company->site_url,
                'sms_app_uri'         => $company->sms_app_uri,
                'created_at'          => $company->created_at->diffForHumans(),
                'updated_at'          => $company->updated_at->diffForHumans(),
            ];
        });

        return $this->result;
    }
}