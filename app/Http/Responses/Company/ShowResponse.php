<?php

namespace App\Http\Responses\Company;

use Illuminate\Contracts\Support\Responsable;

class ShowResponse implements Responsable
{
    protected $result;

    public function __construct($result)
    {
        $this->result = $result;
    }

    public function toResponse($request)
    {
        return response_api($this->transform());
    }

    protected function transform()
    {
        return [
            'user_id'             => $this->result->user_id,
            'company_name'        => $this->result->company_name,
            'qualification_state' => $this->result->qualification_state,
            'license_uri'         => $this->result->license_uri,
            'organization_uri'    => $this->result->organization_uri,
            'id_card_front_uri'   => $this->result->id_card_front_uri,
            'id_card_back_uri'    => $this->result->id_card_back_uri,
            'tax_register_uri'    => $this->result->tax_register_uri,
            'site_url'            => $this->result->site_url,
            'icp_uri'             => $this->result->icp_uri,
            'sms_app_uri'         => $this->result->sms_app_uri,
            'updated_at'          => $this->result->updated_at->format('Y-m-d H:i:s'),
            'created_at'          => $this->result->created_at->format('Y-m-d H:i:s'),
        ];
    }
}