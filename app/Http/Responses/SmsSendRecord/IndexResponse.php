<?php

namespace App\Http\Responses\SmsSendRecord;

use App\Models\SmsSendRecord;
use Illuminate\Contracts\Support\Responsable;

class IndexResponse implements Responsable
{
    protected $result;

    public function __construct($result)
    {
        $this->result = $result;
    }

    public function toResponse($request)
    {
        return response_api($this->transform());
    }

    protected function transform()
    {
        $this->result->getCollection()->transform(function (SmsSendRecord $smsSendRecord) {
            return [
                'report_id'  => $smsSendRecord->report_id,
                'user_id'    => $smsSendRecord->user_id,
                'mobile'     => $smsSendRecord->mobile,
                'content'    => $smsSendRecord->content,
                'sms_sign'   => $smsSendRecord->sms_sign,
                'state'      => $smsSendRecord->state,
                'reason'     => $smsSendRecord->reason,
                'channel_id' => $smsSendRecord->channel_id,
                'created_at' => $smsSendRecord->created_at->diffForHumans(),
                'updated_at' => $smsSendRecord->updated_at->diffForHumans(),
            ];
        });

        return $this->result;
    }
}