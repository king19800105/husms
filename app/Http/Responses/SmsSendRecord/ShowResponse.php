<?php

namespace App\Http\Responses\SmsSendRecord;

use Illuminate\Contracts\Support\Responsable;

class ShowResponse implements Responsable
{
    protected $result;

    public function __construct($result)
    {
        $this->result = $result;
    }

    public function toResponse($request)
    {
        return response_api($this->transform());
    }

    protected function transform()
    {
        return [
            'report_id'        => $this->result->report_id,
            'user_id'          => $this->result->user_id,
            'mobile'           => $this->result->mobile,
            'content'          => $this->result->content,
            'sms_sign'         => $this->result->sms_sign,
            'state'            => $this->result->state,
            'reason'           => $this->result->reason,
            'channel_id'       => $this->result->channel_id,
            'channel_group_id' => $this->result->channel_group_id,
            'try_count'        => $this->result->try_count,
            'created_at'       => $this->result->created_at->format('Y-m-d H:i:s'),
            'updated_at'       => $this->result->updated_at->format('Y-m-d H:i:s'),
        ];
    }
}