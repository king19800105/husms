<?php

namespace App\Http\Responses\GoodsCategory;

use Illuminate\Contracts\Support\Responsable;

class ShowResponse implements Responsable
{
    protected $result;

    public function __construct($result)
    {
        $this->result = $result;
    }

    public function toResponse($request)
    {
        return response_api($this->transform());
    }

    protected function transform()
    {
        return [
            'name'            => $this->result->name,
            'alias_name'      => $this->result->alias_name,
            'parent_id'       => $this->result->parent_id,
            'seo_keywords'    => $this->result->seo_keywords,
            'seo_title'       => $this->result->seo_title,
            'seo_description' => $this->result->seo_description,
            'weight'          => $this->result->weight,
            'img'             => $this->result->img,
            'state'           => $this->result->state,
            'show_type'       => $this->result->show_type,
            'created_at'      => $this->result->created_at->format('Y-m-d H:i:s'),
            'updated_at'      => $this->result->updated_at->format('Y-m-d H:i:s'),
        ];
    }
}