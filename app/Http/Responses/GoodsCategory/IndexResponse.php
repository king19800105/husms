<?php

namespace App\Http\Responses\GoodsCategory;

use App\Models\GoodsCategory;
use Illuminate\Contracts\Support\Responsable;

class IndexResponse implements Responsable
{
    protected $result;

    public function __construct($result)
    {
        $this->result = $result;
    }

    public function toResponse($request)
    {
        return response_api($this->transform());
    }

    protected function transform()
    {
        $this->result->getCollection()->transform(function (GoodsCategory $goodsCategory) {
            return [
                'name'            => $goodsCategory->name,
                'alias_name'      => $goodsCategory->alias_name,
                'parent_id'       => $goodsCategory->parent_id,
                'state'           => $goodsCategory->state,
                'show_type'       => $goodsCategory->show_type,
                'created_at'      => $goodsCategory->created_at->diffForHumans(),
                'updated_at'      => $goodsCategory->updated_at->diffForHumans(),
            ];
        });

        return $this->result;
    }
}