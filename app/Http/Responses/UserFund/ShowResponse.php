<?php

namespace App\Http\Responses\UserFund;

use Illuminate\Contracts\Support\Responsable;

class ShowResponse implements Responsable
{
    protected $result;

    public function __construct($result)
    {
        $this->result = $result;
    }

    public function toResponse($request)
    {
        return response_api($this->transform());
    }

    protected function transform()
    {
        return [
            'user_id'     => $this->result->user_id,
            'type'        => $this->result->type,
            'sms_record'  => $this->result->sms_record,
            'cash_record' => $this->result->cash_record,
            'created_at'  => $this->result->created_at->format('Y-m-d H:i:s'),
            'updated_at'  => $this->result->updated_at->format('Y-m-d H:i:s'),
        ];
    }
}