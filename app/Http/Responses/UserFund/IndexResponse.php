<?php

namespace App\Http\Responses\UserFund;

use App\Models\UserFund;
use Illuminate\Contracts\Support\Responsable;

class IndexResponse implements Responsable
{
    protected $result;

    public function __construct($result)
    {
        $this->result = $result;
    }

    public function toResponse($request)
    {
        return response_api($this->transform());
    }

    protected function transform()
    {
        $this->result->getCollection()->transform(function (UserFund $userFund) {
            return [
                'user_id'     => $userFund->user_id,
                'type'        => $userFund->type,
                'sms_record'  => $userFund->sms_record,
                'cash_record' => $userFund->cash_record,
                'created_at'  => $userFund->created_at->diffForHumans(),
                'updated_at'  => $userFund->updated_at->diffForHumans(),
            ];
        });

        return $this->result;
    }
}