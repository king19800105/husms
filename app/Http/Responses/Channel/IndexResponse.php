<?php

namespace App\Http\Responses\Channel;

use App\Models\Channel;
use Illuminate\Contracts\Support\Responsable;

class IndexResponse implements Responsable
{
    protected $result;

    public function __construct($result)
    {
        $this->result = $result;
    }

    public function toResponse($request)
    {
        return response_api($this->transform());
    }

    protected function transform()
    {
        $this->result->getCollection()->transform(function (Channel $channel) {
            return [
                'name'             => $channel->name,
                'access_type'      => $channel->access_type,
                'access_domain'    => $channel->access_domain,
                'access_ip'        => $channel->access_ip,
                'access_port'      => $channel->access_port,
                'channel_model_id' => $channel->channel_model_id,
                'is_sign_limit'    => $channel->is_sign_limit,
                'state'            => $channel->state,
                'weight'           => $channel->weight,
                'remark'           => $channel->remark,
                'created_at'       => $channel->created_at->diffForHumans(),
                'updated_at'       => $channel->updated_at->diffForHumans(),
            ];
        });

        return $this->result;
    }
}