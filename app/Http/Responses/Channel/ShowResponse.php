<?php

namespace App\Http\Responses\Channel;

use Illuminate\Contracts\Support\Responsable;

class ShowResponse implements Responsable
{
    protected $result;

    public function __construct($result)
    {
        $this->result = $result;
    }

    public function toResponse($request)
    {
        return response_api($this->transform());
    }

    protected function transform()
    {
        return [
            'name'             => $this->result->name,
            'access_type'      => $this->result->access_type,
            'access_domain'    => $this->result->access_domain,
            'access_ip'        => $this->result->access_ip,
            'access_port'      => $this->result->access_port,
            'channel_model_id' => $this->result->channel_model_id,
            'is_sign_limit'    => $this->result->is_sign_limit,
            'state'            => $this->result->state,
            'weight'           => $this->result->weight,
            'remark'           => $this->result->remark,
            'updated_at'       => $this->result->updated_at->format('Y-m-d H:i:s'),
            'created_at'       => $this->result->created_at->format('Y-m-d H:i:s'),
        ];
    }
}