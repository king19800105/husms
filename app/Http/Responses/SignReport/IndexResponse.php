<?php

namespace App\Http\Responses\SignReport;

use App\Models\SignReport;
use Illuminate\Contracts\Support\Responsable;

class IndexResponse implements Responsable
{
    protected $result;

    public function __construct($result)
    {
        $this->result = $result;
    }

    public function toResponse($request)
    {
        return response_api($this->transform());
    }

    protected function transform()
    {
        $this->result->getCollection()->transform(function (SignReport $signReport) {
            return [
                'sms_sign'   => $signReport->sms_sign,
                'created_at' => $signReport->created_at->diffForHumans(),
                'updated_at' => $signReport->updated_at->diffForHumans(),
            ];
        });

        return $this->result;
    }
}