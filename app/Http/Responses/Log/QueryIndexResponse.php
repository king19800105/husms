<?php

namespace App\Http\Responses\Goods;

use App\Models\Goods;
use App\Models\SlowQueryLog;
use Illuminate\Contracts\Support\Responsable;

class QueryIndexResponse implements Responsable
{
    protected $result;

    public function __construct($result)
    {
        $this->result = $result;
    }

    public function toResponse($request)
    {
        return response_api($this->transform());
    }

    protected function transform()
    {
        $this->result->getCollection()->transform(function (SlowQueryLog $slowQueryLog) {
            return [
                'id'          => $slowQueryLog->id,
                'exec'        => $slowQueryLog->exec,
                'ori_sql'     => $slowQueryLog->ori_sql,
                'expend_time' => $slowQueryLog->expend_time,
                'is_fixed'    => $slowQueryLog->is_fixed,
                'created_at'  => $slowQueryLog->created_at->diffForHumans(),
                'updated_at'  => $slowQueryLog->updated_at->diffForHumans(),
            ];
        });

        return $this->result;
    }
}