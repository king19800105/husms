<?php

namespace App\Http\Responses\Goods;

use Illuminate\Contracts\Support\Responsable;

class QueryShowResponse implements Responsable
{
    protected $result;

    public function __construct($result)
    {
        $this->result = $result;
    }

    public function toResponse($request)
    {
        return response_api($this->transform());
    }

    protected function transform()
    {
        return [
            'exec'        => $this->result->exec,
            'sql_content' => $this->result->sql_content,
            'ori_sql'     => $this->result->ori_sql,
            'expend_time' => $this->result->expend_time,
            'is_fixed'    => $this->result->is_fixed,
            'created_at'  => $this->result->created_at->format('Y-m-d H:i:s'),
            'updated_at'  => $this->result->updated_at->format('Y-m-d H:i:s'),
        ];
    }
}