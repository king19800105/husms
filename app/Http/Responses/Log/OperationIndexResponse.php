<?php

namespace App\Http\Responses\Goods;

use App\Models\Goods;
use App\Models\OperationLog;
use App\Models\SlowQueryLog;
use Illuminate\Contracts\Support\Responsable;

class OperationIndexResponse implements Responsable
{
    protected $result;

    public function __construct($result)
    {
        $this->result = $result;
    }

    public function toResponse($request)
    {
        return response_api($this->transform());
    }

    protected function transform()
    {
        $this->result->getCollection()->transform(function (OperationLog $operationLog) {
            return [
                'id'         => $operationLog->id,
                'exec'       => $operationLog->exec,
                'name'       => $operationLog->name,
                'user_model' => $operationLog->user_model,
                'created_at' => $operationLog->created_at->diffForHumans(),
                'updated_at' => $operationLog->updated_at->diffForHumans(),
            ];
        });

        return $this->result;
    }
}