<?php

namespace App\Http\Responses\Goods;

use Illuminate\Contracts\Support\Responsable;

class OperationShowResponse implements Responsable
{
    protected $result;

    public function __construct($result)
    {
        $this->result = $result;
    }

    public function toResponse($request)
    {
        return response_api($this->transform());
    }

    protected function transform()
    {
        return [
            'user_id'       => $this->result->user_id,
            'exec'          => $this->result->exec,
            'name'          => $this->result->name,
            'user_model'    => $this->result->user_model,
            'params'        => $this->result->params,
            'response_data' => $this->result->response_data,
            'created_at'    => $this->result->created_at->diffForHumans(),
            'updated_at'    => $this->result->updated_at->diffForHumans(),
        ];
    }
}