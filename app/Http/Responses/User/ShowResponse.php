<?php

namespace App\Http\Responses\User;

use Illuminate\Contracts\Support\Responsable;

class ShowResponse implements Responsable
{
    protected $result;

    public function __construct($result)
    {
        $this->result = $result;
    }

    public function toResponse($request)
    {
        return response_api($this->transform());
    }

    protected function transform()
    {
        return [
            'name'           => $this->result->name,
            'parent_id'      => $this->result->parent_id,
            'email'          => $this->result->email,
            'mobile'         => $this->result->mobile,
            'qq'             => $this->result->qq,
            'head_portrait'  => $this->result->head_portrait,
            'real_name'      => $this->result->real_name,
            'type'           => $this->result->type,
            'state'          => $this->result->state,
            'source'         => $this->result->source,
            'promotion_code' => $this->result->promotion_code,
            'earn'           => $this->result->earn,
            'total_number'   => $this->result->total_number,
            'gift_number'    => $this->result->gift_number,
            'use_number'     => $this->result->use_number,
            'residue_number' => $this->result->residue_number,
            'last_login_ip'  => $this->result->last_login_ip,
            'last_sync_time' => $this->result->last_sync_time->format('Y-m-d H:i:s'),
            'created_at'     => $this->result->created_at->format('Y-m-d H:i:s'),
            'updated_at'     => $this->result->updated_at->format('Y-m-d H:i:s'),
        ];
    }
}