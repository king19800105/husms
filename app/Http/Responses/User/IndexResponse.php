<?php

namespace App\Http\Responses\User;

use App\Models\User;
use Illuminate\Contracts\Support\Responsable;

class IndexResponse implements Responsable
{
    protected $result;

    public function __construct($result)
    {
        $this->result = $result;
    }

    public function toResponse($request)
    {
        return response_api($this->transform());
    }

    protected function transform()
    {
        $this->result->getCollection()->transform(function (User $user) {
            return [
                'name'           => $user->name,
                'mobile'         => $user->mobile,
                'type'           => $user->type,
                'state'          => $user->state,
                'source'         => $user->source,
                'residue_number' => $user->residue_number,
                'earn'           => $user->earn,
                'created_at'     => $user->created_at->diffForHumans(),
                'updated_at'     => $user->updated_at->diffForHumans(),
            ];
        });

        return $this->result;
    }
}