<?php

namespace App\Http\Responses\Config;

use App\Models\Config;
use Illuminate\Contracts\Support\Responsable;

class IndexResponse implements Responsable
{
    protected $result;

    public function __construct($result)
    {
        $this->result = $result;
    }

    public function toResponse($request)
    {
        return response_api($this->transform());
    }

    protected function transform()
    {
        $this->result->getCollection()->transform(function (Config $config) {
            return [
                'name'       => $config->name,
                'show_name'  => $config->show_name,
                'info'       => $config->info,
                'created_at' => $config->created_at->diffForHumans(),
                'updated_at' => $config->updated_at->diffForHumans(),
            ];
        });

        return $this->result;
    }
}