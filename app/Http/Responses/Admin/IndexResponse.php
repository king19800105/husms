<?php

namespace App\Http\Responses\Admin;

use App\Models\Admin;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Contracts\Support\Responsable;
use Illuminate\Support\Facades\DB;

class IndexResponse implements Responsable
{
    protected $result;

    public function __construct(LengthAwarePaginator $result)
    {
        $this->result = $result;
    }

    public function toResponse($request)
    {
        return response_api($this->transform());
    }

    protected function transform()
    {
        $this->result->getCollection()->transform(function (Admin $admin) {
            return [
                'id'             => $admin->id,
                'name'           => $admin->name,
                'email'          => $admin->email,
                'mobile'         => $admin->mobile,
                'state'          => $admin->showState(),
                'last_logged_id' => $admin->last_logged_ip,
                'created_at'     => $admin->created_at->diffForHumans(),
            ];
        });
        return $this->result;
    }
}