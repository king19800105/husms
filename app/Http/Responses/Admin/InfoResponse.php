<?php

namespace App\Http\Responses\Admin;

use Illuminate\Contracts\Support\Responsable;

class InfoResponse implements Responsable
{
    protected $result;

    public function __construct($result)
    {
        $this->result = $result;
    }

    public function toResponse($request)
    {
        return response_api($this->transform());
    }

    protected function transform()
    {
        return [
            'id'             => $this->result->id,
            'name'           => $this->result->name,
            'mobile'         => $this->result->mobile,
            'created_at'     => $this->result->created_at->format('Y-m-d H:i:s'),
            'email'          => $this->result->email,
            'last_logged_ip' => $this->result->last_logged_ip,
            'remark'         => $this->result->remark,
            'roles'          => $this->result->roles,
        ];
    }
}