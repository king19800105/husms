<?php

namespace App\Http\Responses\Admin;

use App\Models\Admin;
use Illuminate\Contracts\Support\Responsable;

class ShowResponse implements Responsable
{
    protected $result;

    public function __construct(Admin $result)
    {
        $this->result = $result;
    }

    public function toResponse($request)
    {
        return response_api($this->transform());
    }

    protected function transform()
    {
        return [
            'name'           => $this->result->name,
            'email'          => $this->result->email,
            'mobile'         => $this->result->mobile,
            'remark'         => $this->result->remark,
            'state'          => $this->result->showState(),
            'producer'       => $this->result->producer,
            'last_logged_ip' => $this->result->last_logged_ip,
            'roles'          => $this->getRoleIds(),
            'updated_at'     => $this->result->updated_at->format('Y-m-d H:i:s'),
            'created_at'     => $this->result->created_at->format('Y-m-d H:i:s'),
        ];
    }

    protected function getRoleIds()
    {
        $data = $this->result->roles;

        if (!$data->isEmpty()) {
            return $data->pluck('id')->toArray();
        }

        return [];
    }
}