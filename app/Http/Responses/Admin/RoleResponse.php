<?php

namespace App\Http\Responses\Admin;

use Illuminate\Contracts\Support\Responsable;
use Illuminate\Support\Collection;

class RoleResponse implements Responsable
{
    protected $result;

    public function __construct(Collection $result)
    {
        $this->result = $result;
    }

    public function toResponse($request)
    {
        return response_api($this->transform());
    }

    protected function transform()
    {
        $this->result->transform(function ($role) {
            return [
                'id'        => $role->id,
                'name'      => $role->name,
                'show_name' => $role->show_name,
            ];
        });

        return $this->result;
    }
}