<?php

namespace App\Http\Responses\Role;

use Illuminate\Contracts\Support\Responsable;
use Illuminate\Support\Collection;

class PermissionResponse implements Responsable
{
    protected $result;

    public function __construct(Collection $result)
    {
        $this->result = $result;
    }

    public function toResponse($request)
    {
        return response_api($this->transform());
    }

    protected function transform()
    {
        $this->result->transform(function ($permission) {
            return [
                'id'        => $permission->id,
                'name'      => $permission->name,
                'module'    => $permission->module,
                'show_name' => $permission->show_name,
            ];
        });

        return $this->result;
    }
}