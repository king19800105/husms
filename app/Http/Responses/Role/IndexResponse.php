<?php

namespace App\Http\Responses\Role;

use App\Models\Role;
use Illuminate\Contracts\Support\Responsable;

class IndexResponse implements Responsable
{
    protected $result;

    public function __construct($result)
    {
        $this->result = $result;
    }

    public function toResponse($request)
    {
        return response_api($this->transform());
    }

    protected function transform()
    {
        $this->result->getCollection()->transform(function (Role $role) {
            return [
                'id'         => $role->id,
                'name'       => $role->name,
                'guard_name' => $role->guard_name,
                'show_name'  => $role->show_name,
                'created_at' => $role->created_at->diffForHumans(),
                'updated_at' => $role->updated_at->diffForHumans(),
            ];
        });

        return $this->result;
    }
}