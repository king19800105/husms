<?php

namespace App\Http\Responses\Role;

use Illuminate\Contracts\Support\Responsable;

class ShowResponse implements Responsable
{
    protected $result;

    public function __construct($result)
    {
        $this->result = $result;
    }

    public function toResponse($request)
    {
        return response_api($this->transform());
    }

    protected function transform()
    {
        return [
            'name'       => $this->result->name,
            'guard_name' => $this->result->guard_name,
            'show_name'  => $this->result->show_name,
            'permissions'=> $this->getPermissionIds(),
            'updated_at' => $this->result->updated_at->format('Y-m-d H:i:s'),
            'created_at' => $this->result->created_at->format('Y-m-d H:i:s'),
        ];
    }

    protected function getPermissionIds(): array
    {
        $data = $this->result->permissions;

        if (!$data->isEmpty()) {
            return $data->pluck('id')->toArray();
        }

        return [];
    }
}