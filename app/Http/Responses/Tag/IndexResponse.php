<?php

namespace App\Http\Responses\Tag;

use App\Models\Tag;
use Illuminate\Contracts\Support\Responsable;

class IndexResponse implements Responsable
{
    protected $result;

    public function __construct($result)
    {
        $this->result = $result;
    }

    public function toResponse($request)
    {
        return response_api($this->transform());
    }

    protected function transform()
    {
        $this->result->getCollection()->transform(function (Tag $tag) {
            return [
                'name'       => $tag->name,
                'alias_name' => $tag->alias_name,
                'model_type' => $tag->model_type,
                'created_at' => $tag->created_at->diffForHumans(),
                'updated_at' => $tag->updated_at->diffForHumans(),
            ];
        });

        return $this->result;
    }
}