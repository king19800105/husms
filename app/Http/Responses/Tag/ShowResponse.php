<?php

namespace App\Http\Responses\Tag;

use Illuminate\Contracts\Support\Responsable;

class ShowResponse implements Responsable
{
    protected $result;

    public function __construct($result)
    {
        $this->result = $result;
    }

    public function toResponse($request)
    {
        return response_api($this->transform());
    }

    protected function transform()
    {
        return [
            'name'       => $this->result->name,
            'alias_name' => $this->result->alias_name,
            'model_type' => $this->result->model_type,
            'created_at' => $this->result->created_at->format('Y-m-d H:i:s'),
            'updated_at' => $this->result->updated_at->format('Y-m-d H:i:s'),
        ];
    }
}