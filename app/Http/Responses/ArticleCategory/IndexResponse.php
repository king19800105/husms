<?php

namespace App\Http\Responses\ArticleCategory;

use App\Models\ArticleCategory;
use Illuminate\Contracts\Support\Responsable;

class IndexResponse implements Responsable
{
    protected $result;

    public function __construct($result)
    {
        $this->result = $result;
    }

    public function toResponse($request)
    {
        return response_api($this->transform());
    }

    protected function transform()
    {
        $this->result->getCollection()->transform(function (ArticleCategory $articleCategory) {
            return [
                'name'       => $articleCategory->name,
                'alias_name' => $articleCategory->alias_name,
                'img'    => $articleCategory->img,
                'created_at' => $articleCategory->created_at->diffForHumans(),
                'updated_at' => $articleCategory->updated_at->diffForHumans(),
            ];
        });

        return $this->result;
    }
}