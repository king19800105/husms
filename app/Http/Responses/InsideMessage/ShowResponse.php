<?php

namespace App\Http\Responses\InsideMessage;

use Illuminate\Contracts\Support\Responsable;

class ShowResponse implements Responsable
{
    protected $result;

    public function __construct($result)
    {
        $this->result = $result;
    }

    public function toResponse($request)
    {
        return response_api($this->transform());
    }

    protected function transform()
    {
        if (!empty($this->result)) {
            return [
                'title'       => $this->result->title,
                'contact_way' => $this->result->contact_way,
                'content'     => $this->result->content,
                'created_at'  => $this->result->created_at,
            ];
        }

        return [];
    }
}