<?php

namespace App\Http\Responses\ChannelGroup;

use App\Models\ChannelGroup;
use Illuminate\Contracts\Support\Responsable;

class IndexResponse implements Responsable
{
    protected $result;

    public function __construct($result)
    {
        $this->result = $result;
    }

    public function toResponse($request)
    {
        return response_api($this->transform());
    }

    protected function transform()
    {
        $this->result->getCollection()->transform(function (ChannelGroup $channelGroup) {
            return [
                'group_name' => $channelGroup->group_name,
                'parent_id'  => $channelGroup->parent_id,
                'state'      => $channelGroup->state,
                'weight'     => $channelGroup->weight,
                'remark'     => $channelGroup->remark,
                'created_at' => $channelGroup->created_at->diffForHumans(),
                'updated_at' => $channelGroup->updated_at->diffForHumans(),
            ];
        });

        return $this->result;
    }
}