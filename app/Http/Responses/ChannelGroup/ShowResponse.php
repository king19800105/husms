<?php

namespace App\Http\Responses\ChannelGroup;

use Illuminate\Contracts\Support\Responsable;

class ShowResponse implements Responsable
{
    protected $result;

    public function __construct($result)
    {
        $this->result = $result;
    }

    public function toResponse($request)
    {
        return response_api($this->transform());
    }

    protected function transform()
    {
        return [
            'group_name' => $this->result->group_name,
            'parent_id'  => $this->result->parent_id,
            'state'      => $this->result->state,
            'weight'     => $this->result->weight,
            'remark'     => $this->result->remark,
            'updated_at' => $this->result->updated_at->format('Y-m-d H:i:s'),
            'created_at' => $this->result->created_at->format('Y-m-d H:i:s'),
        ];
    }
}