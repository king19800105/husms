<?php

namespace App\Http\Responses\Menu;

use App\Models\Menu;
use Illuminate\Contracts\Support\Responsable;

class IndexResponse implements Responsable
{
    protected $result;

    public function __construct($result)
    {
        $this->result = is_array($result) ? collect($result) : $result;
    }

    public function toResponse($request)
    {
        return response_api($this->transform());
    }

    protected function transform()
    {
        $this->result->transform(function ($menu) {
            return [
                'id'         => $menu['id'],
                'name'       => $menu['name'],
                'slug'       => $menu['slug'],
                'light'      => $menu['light'],
                'icon'       => $menu['icon'],
                'url'        => $menu['url'],
                'url_alias'  => $menu['url_alias'],
                'type'       => $menu['type'],
                'child'      => $menu['child'],
                'created_at' => $menu['created_at'],
                'updated_at' => $menu['updated_at'],
            ];
        });

        return $this->result;
    }
}