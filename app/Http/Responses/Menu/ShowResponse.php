<?php

namespace App\Http\Responses\Menu;

use Illuminate\Contracts\Support\Responsable;

class ShowResponse implements Responsable
{
    protected $result;

    public function __construct($result)
    {
        $this->result = $result;
    }

    public function toResponse($request)
    {
        return response_api($this->transform());
    }

    protected function transform()
    {
        return [
            'name'        => $this->result->name,
            'slug'        => $this->result->slug,
            'parent_id'   => $this->result->parent_id,
            'weight'      => $this->result->weight,
            'url'         => $this->result->url,
            'url_alias'   => $this->result->url_alias,
            'type'        => $this->result->type,
            'icon'        => $this->result->icon,
            'light'       => $this->result->light,
            'description' => $this->result->description,
            'created_at'  => $this->result->created_at->format('Y-m-d H:i:s'),
            'updated_at'  => $this->result->updated_at->format('Y-m-d H:i:s'),
        ];
    }
}