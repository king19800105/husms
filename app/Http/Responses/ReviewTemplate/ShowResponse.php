<?php

namespace App\Http\Responses\ReviewTemplate;

use Illuminate\Contracts\Support\Responsable;

class ShowResponse implements Responsable
{
    protected $result;

    protected $isBackend;

    public function __construct($result, $isBackend = true)
    {
        $this->isBackend = $isBackend;
        $this->result = $result;
    }

    public function toResponse($request)
    {
        return response_api($this->transform());
    }

    protected function transform()
    {
        $baseShow = [
            'id'              => $this->result->id,
            'title'           => $this->result->title,
            'content'         => $this->result->content,
            'sign'            => $this->result->sign,
            'state'           => $this->result->state,
            'rejected_reason' => $this->result->rejected_reason,
            'created_at'      => $this->result->created_at->format('Y-m-d H:i:s'),
            'updated_at'      => $this->result->updated_at->format('Y-m-d H:i:s'),
        ];

        if ($this->isBackend) {
            return array_merge($baseShow, [
                'hash_id'          => $this->result->hash_id,
                'user_id'          => $this->result->user_id,
                'verifier_id'      => $this->result->verifier_id,
                'channel_group_id' => $this->result->channel_group_id,
                'only_channel_id'  => $this->result->only_channel_id,
                'remark'           => $this->result->remark,
            ]);
        }

        return $baseShow;
    }
}