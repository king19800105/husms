<?php

namespace App\Http\Responses\ReviewTemplate;

use App\Models\ReviewTemplate;
use Illuminate\Contracts\Support\Responsable;

class IndexResponse implements Responsable
{
    protected $result;

    public function __construct($result)
    {
        $this->result = $result;
    }

    public function toResponse($request)
    {
        return response_api($this->transform());
    }

    protected function transform()
    {
        $this->result->getCollection()->transform(function (ReviewTemplate $reviewTemplate) {
            return [
                'id'              => $reviewTemplate->id,
                'user_id'         => $reviewTemplate->user_id,
                'title'           => $reviewTemplate->title,
                'content'         => $reviewTemplate->content,
                'sign'            => $reviewTemplate->sign,
                'state'           => $reviewTemplate->state,
                'created_at'      => $reviewTemplate->created_at,
            ];
        });

        return $this->result;
    }
}