<?php

namespace App\Http\Responses\SmsUserList;

use Illuminate\Contracts\Support\Responsable;

class ShowResponse implements Responsable
{
    protected $result;

    public function __construct($result)
    {
        $this->result = $result;
    }

    public function toResponse($request)
    {
        return response_api($this->transform());
    }

    protected function transform()
    {
        return [
            'user_id'    => $this->result->user_id,
            'type'       => $this->result->type,
            'bind_key'   => $this->result->bind_key,
            'bind_value' => $this->result->bind_value,
            'created_at' => $this->result->created_at->format('Y-m-d H:i:s'),
            'updated_at' => $this->result->updated_at->format('Y-m-d H:i:s'),
        ];
    }
}