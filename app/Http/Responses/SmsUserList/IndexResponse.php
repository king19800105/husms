<?php

namespace App\Http\Responses\SmsUserList;

use App\Models\SmsUserList;
use Illuminate\Contracts\Support\Responsable;

class IndexResponse implements Responsable
{
    protected $result;

    public function __construct($result)
    {
        $this->result = $result;
    }

    public function toResponse($request)
    {
        return response_api($this->transform());
    }

    protected function transform()
    {
        $this->result->getCollection()->transform(function (SmsUserList $smsUserList) {
            return [
                'user_id'    => $smsUserList->user_id,
                'type'       => $smsUserList->type,
                'bind_key'   => $smsUserList->bind_key,
                'bind_value' => $smsUserList->bind_value,
                'created_at' => $smsUserList->created_at->diffForHumans(),
                'updated_at' => $smsUserList->updated_at->diffForHumans(),
            ];
        });

        return $this->result;
    }
}