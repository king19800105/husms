<?php

namespace App\Http\Responses\SmsSendLimit;

use App\Models\SmsSendLimit;
use Illuminate\Contracts\Support\Responsable;

class IndexResponse implements Responsable
{
    protected $result;

    public function __construct($result)
    {
        $this->result = $result;
    }

    public function toResponse($request)
    {
        return response_api($this->transform());
    }

    protected function transform()
    {
        $this->result->getCollection()->transform(function (SmsSendLimit $smsSendLimit) {
            return [
                'user_id'             => $smsSendLimit->user_id,
                'mobile_day_limit'    => $smsSendLimit->mobile_day_limit,
                'mobile_minute_limit' => $smsSendLimit->mobile_minute_limit,
                'day_limit'           => $smsSendLimit->day_limit,
                'hour_limit'          => $smsSendLimit->hour_limit,
                'minute_limit'        => $smsSendLimit->minute_limit,
                'exceed_count'        => $smsSendLimit->exceed_count,
                'created_at'          => $smsSendLimit->created_at->diffForHumans(),
                'updated_at'          => $smsSendLimit->updated_at->diffForHumans(),
            ];
        });

        return $this->result;
    }
}