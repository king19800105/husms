<?php

namespace App\Http\Responses\SmsSendLimit;

use Illuminate\Contracts\Support\Responsable;

class ShowResponse implements Responsable
{
    protected $result;

    public function __construct($result)
    {
        $this->result = $result;
    }

    public function toResponse($request)
    {
        return response_api($this->transform());
    }

    protected function transform()
    {
        return [
            'user_id'             => $this->result->user_id,
            'mobile_day_limit'    => $this->result->mobile_day_limit,
            'mobile_minute_limit' => $this->result->mobile_minute_limit,
            'day_limit'           => $this->result->day_limit,
            'hour_limit'          => $this->result->hour_limit,
            'minute_limit'        => $this->result->minute_limit,
            'exceed_count'        => $this->result->exceed_count,
            'created_at'          => $this->result->created_at->format('Y-m-d H:i:s'),
            'updated_at'          => $this->result->updated_at->format('Y-m-d H:i:s'),
        ];
    }
}