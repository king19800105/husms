<?php

namespace App\Http\Responses\SmsReply;

use App\Models\SmsReply;
use Illuminate\Contracts\Support\Responsable;

class IndexResponse implements Responsable
{
    protected $result;

    public function __construct($result)
    {
        $this->result = $result;
    }

    public function toResponse($request)
    {
        return response_api($this->transform());
    }

    protected function transform()
    {
        $this->result->getCollection()->transform(function (SmsReply $smsReply) {
            return [
                'mobile'     => $smsReply->mobile,
                'content'    => $smsReply->content,
                'channel_id' => $smsReply->channel_id,
                'sub_code'   => $smsReply->sub_code,
                'created_at' => $smsReply->created_at->diffForHumans(),
                'updated_at' => $smsReply->updated_at->diffForHumans(),
            ];
        });

        return $this->result;
    }
}