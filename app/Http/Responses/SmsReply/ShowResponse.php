<?php

namespace App\Http\Responses\SmsReply;

use Illuminate\Contracts\Support\Responsable;

class ShowResponse implements Responsable
{
    protected $result;

    public function __construct($result)
    {
        $this->result = $result;
    }

    public function toResponse($request)
    {
        return response_api($this->transform());
    }

    protected function transform()
    {
        return [
            'mobile'     => $this->result->mobile,
            'content'    => $this->result->content,
            'channel_id' => $this->result->channel_id,
            'sub_code'   => $this->result->mobile,
            'created_at' => $this->result->created_at->format('Y-m-d H:i:s'),
            'updated_at' => $this->result->updated_at->format('Y-m-d H:i:s'),
        ];
    }
}