<?php

namespace App\Http\Responses\ChannelModelSet;

use App\Models\ChannelModelSet;
use Illuminate\Contracts\Support\Responsable;

class IndexResponse implements Responsable
{
    protected $result;

    public function __construct($result)
    {
        $this->result = $result;
    }

    public function toResponse($request)
    {
        return response_api($this->transform());
    }

    protected function transform()
    {
        $this->result->getCollection()->transform(function (ChannelModelSet $channelModelSet) {
            return [
                'model_name' => $channelModelSet->model_name,
                'created_at' => $channelModelSet->created_at->diffForHumans(),
                'updated_at' => $channelModelSet->updated_at->diffForHumans(),
            ];
        });

        return $this->result;
    }
}