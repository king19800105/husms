<?php

namespace App\Http\Responses\ChannelModelSet;

use Illuminate\Contracts\Support\Responsable;

class ShowResponse implements Responsable
{
    protected $result;

    public function __construct($result)
    {
        $this->result = $result;
    }

    public function toResponse($request)
    {
        return response_api($this->transform());
    }

    protected function transform()
    {
        return [
            'model_name' => $this->result->model_name,
            'updated_at' => $this->result->updated_at->format('Y-m-d H:i:s'),
            'created_at' => $this->result->created_at->format('Y-m-d H:i:s'),
        ];
    }
}