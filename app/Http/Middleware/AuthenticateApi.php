<?php

namespace App\Http\Middleware;

use Illuminate\Auth\Middleware\Authenticate;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;

/**
 * api中间件验证
 *
 * Class AuthenticateApi
 * @package App\Http\Middleware
 */
class AuthenticateApi extends Authenticate
{
    protected function authenticate(array $guards)
    {
        $guard = 1 === count($guards) ? $guards[0] : 'api';

        if ($this->auth->guard($guard)->check()) {
            return $this->auth->shouldUse($guard);
        }

        throw new UnauthorizedHttpException('', 'Unauthenticated');
    }
}
