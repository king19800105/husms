<?php

namespace App\Services;

use App\Repository\Contracts\ChannelRepository;

class ChannelService
{
    /**
     * @var ChannelRepositoryEloquent
     */
    protected $channelRepository;

    public function __construct(ChannelRepository $channelRepository)
    {
        $this->channelRepository = $channelRepository;
    }

   /**
    * Get Channels  and paginate.
    *
    * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
    */
    public function getChannels()
    {
        return $this->channelRepository->paginate();
    }

   /**
    * Get one Channel  by primary key.
    *
    * @param $id
    * @return \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Eloquent\Collection|static[]|static|null
    */
    public function getChannel($id)
    {
        return $this->channelRepository->find($id);
    }

   /**
    * Create a new line on Channel.
    *
    * @param array $data
    * @return \Illuminate\Database\Eloquent\Model|$this
    */
    public function storeChannel($data)
    {
        return $this->channelRepository->create($data);
    }

   /**
    * Update Channel one line by primary key.
    *
    * @param $id
    * @param array $data
    * @return int
    */
    public function updateChannel($id, $data)
    {
        return $this->channelRepository->update($id, $data);
    }

   /**
    * Delete Channel one line by primary key.
    *
    * @param $id
    * @return mixed
    */
    public function deleteChannel($id)
    {
        return $this->channelRepository->deleteById($id);
    }

}