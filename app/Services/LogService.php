<?php
/**
 * Created by PhpStorm.
 * User: kb130
 * Date: 2018/5/24
 * Time: 17:09
 */

namespace App\Services;

use App\Repository\Contracts\OperationLogRepository;
use App\Repository\Contracts\SlowQueryLogRepository;

/**
 * 所有日志服务
 * 所有日志仓储集合
 *
 * Class LogService
 * @package App\Services
 */
class LogService
{
    protected $slowQueryLogRepository;

    protected $operationLogRepository;

    public function __construct(SlowQueryLogRepository $slowQueryLogRepository, OperationLogRepository $operationLogRepository)
    {
        $this->slowQueryLogRepository = $slowQueryLogRepository;
        $this->operationLogRepository = $operationLogRepository;
    }

    public function getSlowQueries()
    {
        return $this->slowQueryLogRepository->paginate();
    }

    public function getSlowQuery($id)
    {
        return $this->slowQueryLogRepository->find($id);
    }

    public function getOperations()
    {
        return $this->operationLogRepository->paginate();
    }

    public function getOperation($id)
    {
        return $this->slowQueryLogRepository->find($id);
    }

    public function fixedSlowQuery($id)
    {
        // todo... 执行修复操作。所有多个sql，一次性全部修复。
    }
}