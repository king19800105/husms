<?php

namespace App\Services;

use App\Repository\Contracts\UserFundRepository;

class UserFundService
{
    /**
     * @var UserFundRepositoryEloquent
     */
    protected $userFundRepository;

    public function __construct(UserFundRepository $userFundRepository)
    {
        $this->userFundRepository = $userFundRepository;
    }

   /**
    * Get UserFunds  and paginate.
    *
    * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
    */
    public function getUserFunds()
    {
        return $this->userFundRepository->paginate();
    }

   /**
    * Get one UserFund  by primary key.
    *
    * @param $id
    * @return \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Eloquent\Collection|static[]|static|null
    */
    public function getUserFund($id)
    {
        return $this->userFundRepository->find($id);
    }

   /**
    * Create a new line on UserFund.
    *
    * @param array $data
    * @return \Illuminate\Database\Eloquent\Model|$this
    */
    public function storeUserFund($data)
    {
        return $this->userFundRepository->create($data);
    }

   /**
    * Update UserFund one line by primary key.
    *
    * @param $id
    * @param array $data
    * @return int
    */
    public function updateUserFund($id, $data)
    {
        return $this->userFundRepository->update($id, $data);
    }

   /**
    * Delete UserFund one line by primary key.
    *
    * @param $id
    * @return mixed
    */
    public function deleteUserFund($id)
    {
        return $this->userFundRepository->deleteById($id);
    }

}