<?php

namespace App\Services;

use App\Repository\Contracts\CompanyRepository;

class CompanyService
{
    /**
     * @var CompanyRepositoryEloquent
     */
    protected $companyRepository;

    public function __construct(CompanyRepository $companyRepository)
    {
        $this->companyRepository = $companyRepository;
    }

   /**
    * Get Companies  and paginate.
    *
    * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
    */
    public function getCompanies()
    {
        return $this->companyRepository->paginate();
    }

   /**
    * Get one Company  by primary key.
    *
    * @param $id
    * @return \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Eloquent\Collection|static[]|static|null
    */
    public function getCompany($id)
    {
        return $this->companyRepository->find($id);
    }

   /**
    * Create a new line on Company.
    *
    * @param array $data
    * @return \Illuminate\Database\Eloquent\Model|$this
    */
    public function storeCompany($data)
    {
        return $this->companyRepository->create($data);
    }

   /**
    * Update Company one line by primary key.
    *
    * @param $id
    * @param array $data
    * @return int
    */
    public function updateCompany($id, $data)
    {
        return $this->companyRepository->update($id, $data);
    }

   /**
    * Delete Company one line by primary key.
    *
    * @param $id
    * @return mixed
    */
    public function deleteCompany($id)
    {
        return $this->companyRepository->deleteById($id);
    }

}