<?php

namespace App\Services;

use App\Repository\Contracts\SmsReplyRepository;

class SmsReplyService
{
    /**
     * @var SmsReplyRepositoryEloquent
     */
    protected $smsReplyRepository;

    public function __construct(SmsReplyRepository $smsReplyRepository)
    {
        $this->smsReplyRepository = $smsReplyRepository;
    }

   /**
    * Get SmsReplies  and paginate.
    *
    * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
    */
    public function getSmsReplies()
    {
        return $this->smsReplyRepository->paginate();
    }

   /**
    * Get one SmsReply  by primary key.
    *
    * @param $id
    * @return \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Eloquent\Collection|static[]|static|null
    */
    public function getSmsReply($id)
    {
        return $this->smsReplyRepository->find($id);
    }

   /**
    * Create a new line on SmsReply.
    *
    * @param array $data
    * @return \Illuminate\Database\Eloquent\Model|$this
    */
    public function storeSmsReply($data)
    {
        return $this->smsReplyRepository->create($data);
    }

   /**
    * Update SmsReply one line by primary key.
    *
    * @param $id
    * @param array $data
    * @return int
    */
    public function updateSmsReply($id, $data)
    {
        return $this->smsReplyRepository->update($id, $data);
    }

   /**
    * Delete SmsReply one line by primary key.
    *
    * @param $id
    * @return mixed
    */
    public function deleteSmsReply($id)
    {
        return $this->smsReplyRepository->deleteById($id);
    }

}