<?php

namespace App\Services;

use App\Repository\Contracts\OrderRepository;

class OrderService
{
    /**
     * @var OrderRepositoryEloquent
     */
    protected $orderRepository;

    public function __construct(OrderRepository $orderRepository)
    {
        $this->orderRepository = $orderRepository;
    }

   /**
    * Get Orders  and paginate.
    *
    * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
    */
    public function getOrders()
    {
        return $this->orderRepository->paginate();
    }

   /**
    * Get one Order  by primary key.
    *
    * @param $id
    * @return \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Eloquent\Collection|static[]|static|null
    */
    public function getOrder($id)
    {
        return $this->orderRepository->find($id);
    }

   /**
    * Create a new line on Order.
    *
    * @param array $data
    * @return \Illuminate\Database\Eloquent\Model|$this
    */
    public function storeOrder($data)
    {
        return $this->orderRepository->create($data);
    }

   /**
    * Update Order one line by primary key.
    *
    * @param $id
    * @param array $data
    * @return int
    */
    public function updateOrder($id, $data)
    {
        return $this->orderRepository->update($id, $data);
    }

   /**
    * Delete Order one line by primary key.
    *
    * @param $id
    * @return mixed
    */
    public function deleteOrder($id)
    {
        return $this->orderRepository->deleteById($id);
    }

}