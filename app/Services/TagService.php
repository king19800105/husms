<?php

namespace App\Services;

use App\Repository\Contracts\TagRepository;

class TagService
{
    /**
     * @var TagRepositoryEloquent
     */
    protected $tagRepository;

    public function __construct(TagRepository $tagRepository)
    {
        $this->tagRepository = $tagRepository;
    }

   /**
    * Get Tags  and paginate.
    *
    * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
    */
    public function getTags()
    {
        return $this->tagRepository->paginate();
    }

   /**
    * Get one Tag  by primary key.
    *
    * @param $id
    * @return \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Eloquent\Collection|static[]|static|null
    */
    public function getTag($id)
    {
        return $this->tagRepository->find($id);
    }

   /**
    * Create a new line on Tag.
    *
    * @param array $data
    * @return \Illuminate\Database\Eloquent\Model|$this
    */
    public function storeTag($data)
    {
        return $this->tagRepository->create($data);
    }

   /**
    * Update Tag one line by primary key.
    *
    * @param $id
    * @param array $data
    * @return int
    */
    public function updateTag($id, $data)
    {
        return $this->tagRepository->update($id, $data);
    }

   /**
    * Delete Tag one line by primary key.
    *
    * @param $id
    * @return mixed
    */
    public function deleteTag($id)
    {
        return $this->tagRepository->deleteById($id);
    }

}