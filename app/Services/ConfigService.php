<?php

namespace App\Services;

use App\Repository\Contracts\ConfigRepository;

class ConfigService
{
    /**
     * @var ConfigRepositoryEloquent
     */
    protected $configRepository;

    public function __construct(ConfigRepository $configRepository)
    {
        $this->configRepository = $configRepository;
    }

   /**
    * Get Configs  and paginate.
    *
    * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
    */
    public function getConfigs()
    {
        return $this->configRepository->paginate();
    }

   /**
    * Get one Config  by primary key.
    *
    * @param $id
    * @return \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Eloquent\Collection|static[]|static|null
    */
    public function getConfig($id)
    {
        return $this->configRepository->find($id);
    }

   /**
    * Create a new line on Config.
    *
    * @param array $data
    * @return \Illuminate\Database\Eloquent\Model|$this
    */
    public function storeConfig($data)
    {
        return $this->configRepository->create($data);
    }

   /**
    * Update Config one line by primary key.
    *
    * @param $id
    * @param array $data
    * @return int
    */
    public function updateConfig($id, $data)
    {
        return $this->configRepository->update($id, $data);
    }

   /**
    * Delete Config one line by primary key.
    *
    * @param $id
    * @return mixed
    */
    public function deleteConfig($id)
    {
        return $this->configRepository->deleteById($id);
    }

    public function parseConfig($key, $type)
    {

    }

}