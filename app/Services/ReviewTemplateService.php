<?php

namespace App\Services;

use App\Repository\Contracts\ReviewTemplateRepository;

/**
 * 模板审核服务
 *
 * Class ReviewTemplateService
 * @package App\Services
 */
class ReviewTemplateService
{
    /**
     * @var ReviewTemplateRepository
     */
    protected $reviewTemplateRepository;

    public function __construct(ReviewTemplateRepository $reviewTemplateRepository)
    {
        $this->reviewTemplateRepository = $reviewTemplateRepository;
    }

    public function getReviewTemplates()
    {
        return $this->reviewTemplateRepository->paginate();
    }

    public function getReviewTemplate($id)
    {
        return $this->reviewTemplateRepository->find($id);
    }

    public function storeReviewTemplate($data)
    {
        return $this->reviewTemplateRepository->create($data);
    }

    /**
     * 验证是否达到上限
     *
     * @return bool
     */
    public function isToLimit(): bool
    {
        $userId = optional(request()->user())->id;
        $count = $this->reviewTemplateRepository->findWhereCount(['user_id', $userId]);
        $limit = config('sms.template.limit');

        return $count > $limit;
    }

    /**
     * 后台管理员模板审核操作
     *
     * @param $id
     * @param $data
     * @return mixed
     */
    public function updateReviewTemplate($id, $data)
    {
        return $this->reviewTemplateRepository->update($id, $data);
    }

    /**
     * 删除模板
     *
     * @param $id
     * @return mixed
     */
    public function deleteReviewTemplate($id)
    {
        return $this->reviewTemplateRepository->deleteById($id);
    }
}