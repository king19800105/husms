<?php

namespace App\Services;

use App\Repository\Contracts\ChannelGroupRepository;

class ChannelGroupService
{
    /**
     * @var ChannelGroupRepositoryEloquent
     */
    protected $channelGroupRepository;

    public function __construct(ChannelGroupRepository $channelGroupRepository)
    {
        $this->channelGroupRepository = $channelGroupRepository;
    }

   /**
    * Get ChannelGroups  and paginate.
    *
    * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
    */
    public function getChannelGroups()
    {
        return $this->channelGroupRepository->paginate();
    }

   /**
    * Get one ChannelGroup  by primary key.
    *
    * @param $id
    * @return \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Eloquent\Collection|static[]|static|null
    */
    public function getChannelGroup($id)
    {
        return $this->channelGroupRepository->find($id);
    }

   /**
    * Create a new line on ChannelGroup.
    *
    * @param array $data
    * @return \Illuminate\Database\Eloquent\Model|$this
    */
    public function storeChannelGroup($data)
    {
        return $this->channelGroupRepository->create($data);
    }

   /**
    * Update ChannelGroup one line by primary key.
    *
    * @param $id
    * @param array $data
    * @return int
    */
    public function updateChannelGroup($id, $data)
    {
        return $this->channelGroupRepository->update($id, $data);
    }

   /**
    * Delete ChannelGroup one line by primary key.
    *
    * @param $id
    * @return mixed
    */
    public function deleteChannelGroup($id)
    {
        return $this->channelGroupRepository->deleteById($id);
    }

}