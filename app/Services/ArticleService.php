<?php

namespace App\Services;

use App\Repository\Contracts\ArticleRepository;

class ArticleService
{
    /**
     * @var ArticleRepository
     */
    protected $articleRepository;

    public function __construct(ArticleRepository $articleRepository)
    {
        $this->articleRepository = $articleRepository;
    }

   /**
    * Get Articles  and paginate.
    *
    * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
    */
    public function getArticles()
    {
        return $this->articleRepository->paginate();
    }

   /**
    * Get one Article  by primary key.
    *
    * @param $id
    * @return \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Eloquent\Collection|static[]|static|null
    */
    public function getArticle($id)
    {
        return $this->articleRepository->find($id);
    }

   /**
    * Create a new line on Article.
    *
    * @param array $data
    * @return \Illuminate\Database\Eloquent\Model|$this
    */
    public function storeArticle($data)
    {
        return $this->articleRepository->create($data);
    }

   /**
    * Update Article one line by primary key.
    *
    * @param $id
    * @param array $data
    * @return int
    */
    public function updateArticle($id, $data)
    {
        return $this->articleRepository->update($id, $data);
    }

   /**
    * Delete Article one line by primary key.
    *
    * @param $id
    * @return mixed
    */
    public function deleteArticle($id)
    {
        return $this->articleRepository->deleteById($id);
    }

}