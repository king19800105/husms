<?php

namespace App\Services;

use App\Repository\Contracts\ArticleCategoryRepository;

class ArticleCategoryService
{
    /**
     * @var ArticleCategoryRepositoryEloquent
     */
    protected $articleCategoryRepository;

    public function __construct(ArticleCategoryRepository $articleCategoryRepository)
    {
        $this->articleCategoryRepository = $articleCategoryRepository;
    }

   /**
    * Get ArticleCategories  and paginate.
    *
    * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
    */
    public function getArticleCategories()
    {
        return $this->articleCategoryRepository->paginate();
    }

   /**
    * Get one ArticleCategory  by primary key.
    *
    * @param $id
    * @return \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Eloquent\Collection|static[]|static|null
    */
    public function getArticleCategory($id)
    {
        return $this->articleCategoryRepository->find($id);
    }

   /**
    * Create a new line on ArticleCategory.
    *
    * @param array $data
    * @return \Illuminate\Database\Eloquent\Model|$this
    */
    public function storeArticleCategory($data)
    {
        return $this->articleCategoryRepository->create($data);
    }

   /**
    * Update ArticleCategory one line by primary key.
    *
    * @param $id
    * @param array $data
    * @return int
    */
    public function updateArticleCategory($id, $data)
    {
        return $this->articleCategoryRepository->update($id, $data);
    }

   /**
    * Delete ArticleCategory one line by primary key.
    *
    * @param $id
    * @return mixed
    */
    public function deleteArticleCategory($id)
    {
        return $this->articleCategoryRepository->deleteById($id);
    }

}