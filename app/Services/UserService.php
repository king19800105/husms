<?php

namespace App\Services;

use App\Repository\Contracts\UserRepository;

class UserService
{
    /**
     * @var UserRepositoryEloquent
     */
    protected $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

   /**
    * Get Users  and paginate.
    *
    * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
    */
    public function getUsers()
    {
        return $this->userRepository->paginate();
    }

   /**
    * Get one User  by primary key.
    *
    * @param $id
    * @return \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Eloquent\Collection|static[]|static|null
    */
    public function getUser($id)
    {
        return $this->userRepository->find($id);
    }

   /**
    * Create a new line on User.
    *
    * @param array $data
    * @return \Illuminate\Database\Eloquent\Model|$this
    */
    public function storeUser($data)
    {
        return $this->userRepository->create($data);
    }

   /**
    * Update User one line by primary key.
    *
    * @param $id
    * @param array $data
    * @return int
    */
    public function updateUser($id, $data)
    {
        return $this->userRepository->update($id, $data);
    }

   /**
    * Delete User one line by primary key.
    *
    * @param $id
    * @return mixed
    */
    public function deleteUser($id)
    {
        return $this->userRepository->deleteById($id);
    }

}