<?php

namespace App\Services;

use App\Repository\Contracts\SmsSendRecordRepository;

class SmsSendRecordService
{
    /**
     * @var SmsSendRecordRepositoryEloquent
     */
    protected $smsSendRecordRepository;

    public function __construct(SmsSendRecordRepository $smsSendRecordRepository)
    {
        $this->smsSendRecordRepository = $smsSendRecordRepository;
    }

   /**
    * Get SmsSendRecords  and paginate.
    *
    * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
    */
    public function getSmsSendRecords()
    {
        return $this->smsSendRecordRepository->paginate();
    }

   /**
    * Get one SmsSendRecord  by primary key.
    *
    * @param $id
    * @return \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Eloquent\Collection|static[]|static|null
    */
    public function getSmsSendRecord($id)
    {
        return $this->smsSendRecordRepository->find($id);
    }

   /**
    * Create a new line on SmsSendRecord.
    *
    * @param array $data
    * @return \Illuminate\Database\Eloquent\Model|$this
    */
    public function storeSmsSendRecord($data)
    {
        return $this->smsSendRecordRepository->create($data);
    }

   /**
    * Update SmsSendRecord one line by primary key.
    *
    * @param $id
    * @param array $data
    * @return int
    */
    public function updateSmsSendRecord($id, $data)
    {
        return $this->smsSendRecordRepository->update($id, $data);
    }

   /**
    * Delete SmsSendRecord one line by primary key.
    *
    * @param $id
    * @return mixed
    */
    public function deleteSmsSendRecord($id)
    {
        return $this->smsSendRecordRepository->deleteById($id);
    }

}