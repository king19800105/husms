<?php

namespace App\Services;

use App\Repository\Contracts\SignReportRepository;

class SignReportService
{
    /**
     * @var SignReportRepositoryEloquent
     */
    protected $signReportRepository;

    public function __construct(SignReportRepository $signReportRepository)
    {
        $this->signReportRepository = $signReportRepository;
    }

   /**
    * Get SignReports  and paginate.
    *
    * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
    */
    public function getSignReports()
    {
        return $this->signReportRepository->paginate();
    }

   /**
    * Get one SignReport  by primary key.
    *
    * @param $id
    * @return \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Eloquent\Collection|static[]|static|null
    */
    public function getSignReport($id)
    {
        return $this->signReportRepository->find($id);
    }

   /**
    * Create a new line on SignReport.
    *
    * @param array $data
    * @return \Illuminate\Database\Eloquent\Model|$this
    */
    public function storeSignReport($data)
    {
        return $this->signReportRepository->create($data);
    }

   /**
    * Update SignReport one line by primary key.
    *
    * @param $id
    * @param array $data
    * @return int
    */
    public function updateSignReport($id, $data)
    {
        return $this->signReportRepository->update($id, $data);
    }

   /**
    * Delete SignReport one line by primary key.
    *
    * @param $id
    * @return mixed
    */
    public function deleteSignReport($id)
    {
        return $this->signReportRepository->deleteById($id);
    }

}