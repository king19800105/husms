<?php

namespace App\Services;

use App\Repository\Contracts\ChannelModelSetRepository;

class ChannelModelSetService
{
    /**
     * @var ChannelModelSetRepositoryEloquent
     */
    protected $channelModelSetRepository;

    public function __construct(ChannelModelSetRepository $channelModelSetRepository)
    {
        $this->channelModelSetRepository = $channelModelSetRepository;
    }

   /**
    * Get ChannelModelSets  and paginate.
    *
    * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
    */
    public function getChannelModelSets()
    {
        return $this->channelModelSetRepository->paginate();
    }

   /**
    * Get one ChannelModelSet  by primary key.
    *
    * @param $id
    * @return \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Eloquent\Collection|static[]|static|null
    */
    public function getChannelModelSet($id)
    {
        return $this->channelModelSetRepository->find($id);
    }

   /**
    * Create a new line on ChannelModelSet.
    *
    * @param array $data
    * @return \Illuminate\Database\Eloquent\Model|$this
    */
    public function storeChannelModelSet($data)
    {
        return $this->channelModelSetRepository->create($data);
    }

   /**
    * Update ChannelModelSet one line by primary key.
    *
    * @param $id
    * @param array $data
    * @return int
    */
    public function updateChannelModelSet($id, $data)
    {
        return $this->channelModelSetRepository->update($id, $data);
    }

   /**
    * Delete ChannelModelSet one line by primary key.
    *
    * @param $id
    * @return mixed
    */
    public function deleteChannelModelSet($id)
    {
        return $this->channelModelSetRepository->deleteById($id);
    }

}