<?php

namespace App\Services;

use App\Repository\Contracts\PermissionRepository;

class PermissionService
{
    /**
     * @var PermissionRepositoryEloquent
     */
    protected $permissionRepository;

    public function __construct(PermissionRepository $permissionRepository)
    {
        $this->permissionRepository = $permissionRepository;
    }

   /**
    * Get Permissions  and paginate.
    *
    * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
    */
    public function getPermissions()
    {
        return $this->permissionRepository->paginate();
    }

   /**
    * Get one Permission  by primary key.
    *
    * @param $id
    * @return \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Eloquent\Collection|static[]|static|null
    */
    public function getPermission($id)
    {
        return $this->permissionRepository->find($id);
    }

   /**
    * Create a new line on Permission.
    *
    * @param array $data
    * @return \Illuminate\Database\Eloquent\Model|$this
    */
    public function storePermission($data)
    {
        return $this->permissionRepository->create($data);
    }

   /**
    * Update Permission one line by primary key.
    *
    * @param $id
    * @param array $data
    * @return int
    */
    public function updatePermission($id, $data)
    {
        return $this->permissionRepository->update($id, $data);
    }

   /**
    * Delete Permission one line by primary key.
    *
    * @param $id
    * @return mixed
    */
    public function deletePermission($id)
    {
        return $this->permissionRepository->deleteById($id);
    }

    public function getPermissionsOnGroup()
    {
        return $this->permissionRepository->all()->groupBy('module');
    }

}