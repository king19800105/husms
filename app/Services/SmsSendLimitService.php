<?php

namespace App\Services;

use App\Repository\Contracts\SmsSendLimitRepository;

class SmsSendLimitService
{
    /**
     * @var SmsSendLimitRepositoryEloquent
     */
    protected $smsSendLimitRepository;

    public function __construct(SmsSendLimitRepository $smsSendLimitRepository)
    {
        $this->smsSendLimitRepository = $smsSendLimitRepository;
    }

   /**
    * Get SmsSendLimits  and paginate.
    *
    * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
    */
    public function getSmsSendLimits()
    {
        return $this->smsSendLimitRepository->paginate();
    }

   /**
    * Get one SmsSendLimit  by primary key.
    *
    * @param $id
    * @return \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Eloquent\Collection|static[]|static|null
    */
    public function getSmsSendLimit($id)
    {
        return $this->smsSendLimitRepository->find($id);
    }

   /**
    * Create a new line on SmsSendLimit.
    *
    * @param array $data
    * @return \Illuminate\Database\Eloquent\Model|$this
    */
    public function storeSmsSendLimit($data)
    {
        return $this->smsSendLimitRepository->create($data);
    }

   /**
    * Update SmsSendLimit one line by primary key.
    *
    * @param $id
    * @param array $data
    * @return int
    */
    public function updateSmsSendLimit($id, $data)
    {
        return $this->smsSendLimitRepository->update($id, $data);
    }

   /**
    * Delete SmsSendLimit one line by primary key.
    *
    * @param $id
    * @return mixed
    */
    public function deleteSmsSendLimit($id)
    {
        return $this->smsSendLimitRepository->deleteById($id);
    }

}