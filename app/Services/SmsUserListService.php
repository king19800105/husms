<?php

namespace App\Services;

use App\Repository\Contracts\SmsUserListRepository;

class SmsUserListService
{
    /**
     * @var SmsUserListRepositoryEloquent
     */
    protected $smsUserListRepository;

    public function __construct(SmsUserListRepository $smsUserListRepository)
    {
        $this->smsUserListRepository = $smsUserListRepository;
    }

   /**
    * Get SmsUserLists  and paginate.
    *
    * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
    */
    public function getSmsUserLists()
    {
        return $this->smsUserListRepository->paginate();
    }

   /**
    * Get one SmsUserList  by primary key.
    *
    * @param $id
    * @return \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Eloquent\Collection|static[]|static|null
    */
    public function getSmsUserList($id)
    {
        return $this->smsUserListRepository->find($id);
    }

   /**
    * Create a new line on SmsUserList.
    *
    * @param array $data
    * @return \Illuminate\Database\Eloquent\Model|$this
    */
    public function storeSmsUserList($data)
    {
        return $this->smsUserListRepository->create($data);
    }

   /**
    * Update SmsUserList one line by primary key.
    *
    * @param $id
    * @param array $data
    * @return int
    */
    public function updateSmsUserList($id, $data)
    {
        return $this->smsUserListRepository->update($id, $data);
    }

   /**
    * Delete SmsUserList one line by primary key.
    *
    * @param $id
    * @return mixed
    */
    public function deleteSmsUserList($id)
    {
        return $this->smsUserListRepository->deleteById($id);
    }

}