<?php

namespace App\Services;

use App\Repository\Contracts\GoodsCategoryRepository;

class GoodsCategoryService
{
    /**
     * @var GoodsCategoryRepositoryEloquent
     */
    protected $goodsCategoryRepository;

    public function __construct(GoodsCategoryRepository $goodsCategoryRepository)
    {
        $this->goodsCategoryRepository = $goodsCategoryRepository;
    }

   /**
    * Get GoodsCategories  and paginate.
    *
    * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
    */
    public function getGoodsCategories()
    {
        return $this->goodsCategoryRepository->paginate();
    }

   /**
    * Get one GoodsCategory  by primary key.
    *
    * @param $id
    * @return \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Eloquent\Collection|static[]|static|null
    */
    public function getGoodsCategory($id)
    {
        return $this->goodsCategoryRepository->find($id);
    }

   /**
    * Create a new line on GoodsCategory.
    *
    * @param array $data
    * @return \Illuminate\Database\Eloquent\Model|$this
    */
    public function storeGoodsCategory($data)
    {
        return $this->goodsCategoryRepository->create($data);
    }

   /**
    * Update GoodsCategory one line by primary key.
    *
    * @param $id
    * @param array $data
    * @return int
    */
    public function updateGoodsCategory($id, $data)
    {
        return $this->goodsCategoryRepository->update($id, $data);
    }

   /**
    * Delete GoodsCategory one line by primary key.
    *
    * @param $id
    * @return mixed
    */
    public function deleteGoodsCategory($id)
    {
        return $this->goodsCategoryRepository->deleteById($id);
    }

}