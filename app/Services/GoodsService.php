<?php

namespace App\Services;

use App\Repository\Contracts\GoodsRepository;

class GoodsService
{
    /**
     * @var GoodsRepositoryEloquent
     */
    protected $goodsRepository;

    public function __construct(GoodsRepository $goodsRepository)
    {
        $this->goodsRepository = $goodsRepository;
    }

   /**
    * Get Goods  and paginate.
    *
    * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
    */
    public function getGoodsList()
    {
        return $this->goodsRepository->paginate();
    }

   /**
    * Get one Goods  by primary key.
    *
    * @param $id
    * @return \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Eloquent\Collection|static[]|static|null
    */
    public function getGoods($id)
    {
        return $this->goodsRepository->find($id);
    }

   /**
    * Create a new line on Goods.
    *
    * @param array $data
    * @return \Illuminate\Database\Eloquent\Model|$this
    */
    public function storeGoods($data)
    {
        return $this->goodsRepository->create($data);
    }

   /**
    * Update Goods one line by primary key.
    *
    * @param $id
    * @param array $data
    * @return int
    */
    public function updateGoods($id, $data)
    {
        return $this->goodsRepository->update($id, $data);
    }

   /**
    * Delete Goods one line by primary key.
    *
    * @param $id
    * @return mixed
    */
    public function deleteGoods($id)
    {
        return $this->goodsRepository->deleteById($id);
    }

}