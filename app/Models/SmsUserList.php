<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SmsUserList extends Model
{
    protected $fillable = ['user_id', 'type', 'bind_key', 'bind_value'];
}
