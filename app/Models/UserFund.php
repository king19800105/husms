<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserFund extends Model
{
    protected $fillable = ['user_id', 'type', 'sms_record', 'cash_record'];
}
