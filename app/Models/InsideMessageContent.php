<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InsideMessageContent extends Model
{
    protected $fillable = [
        'title', 'content', 'contact_way',
    ];

    public function insideMessage()
    {
        return $this->hasOne(InsideMessage::class, 'content_id', 'id');
    }
}
