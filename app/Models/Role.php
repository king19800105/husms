<?php

namespace App\Models;

use Spatie\Permission\Models\Role as Model;

class Role extends Model
{
    protected $fillable = ['name', 'guard_name', 'show_name'];
}
