<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InsideMessage extends Model
{
    protected $fillable = [
        'sender_id', 'sender_model', 'recipient_id', 'recipient_model', 'type', 'content_id', 'state',
    ];

    /**
     * 一对一关联inside_message_content表
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function insideMessageContent()
    {
        return $this->belongsTo(InsideMessageContent::class, 'content_id');
    }

    /**
     * 一对多关联inside_message_system表
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function insideMessageSystems() {
        return $this->hasMany(InsideMessageSystem::class, 'message_id');
    }
}
