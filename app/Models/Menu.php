<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    protected $fillable = [
        'name', 'slug', 'parent_id', 'url', 'url_alias', 'weight', 'type', 'icon', 'light', 'description'
    ];
}
