<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Channel extends Model
{
    protected $fillable = [
        'name',
        'access_type',
        'access_domain',
        'access_ip',
        'access_port',
        'channel_model_id',
        'is_sign_limit',
        'state',
        'weight',
        'remark',
    ];

    /**
     * 关联到通道分组
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function channelGroups()
    {
        return $this->belongsToMany('App\Models\ChannelGroup')->withTimestamps();
    }

    /**
     * 关联到报备签名
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function signReports()
    {
        return $this->belongsToMany('App\Models\SignReport')->withTimestamps();
    }
}
