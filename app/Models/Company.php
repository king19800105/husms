<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $fillable = [
        'user_id', 'company_name', 'qualification_state', 'license_uri', 'organization_uri', 'id_card_front_uri', 'id_card_back_uri', 'tax_register_uri', 'site_url', 'icp', 'sms_app_uri',
    ];
}
