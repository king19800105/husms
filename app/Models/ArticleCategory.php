<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ArticleCategory extends Model
{
    protected $fillable = [
        'name','alias_name', 'parent_id', 'seo_keywords', 'seo_title', 'seo_description', 'weight', 'img',
    ];

    /**
     * 关联到文章表模型，多对一
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function articles()
    {
        return $this->hasMany('App\Models\Article', 'category_id');
    }
}
