<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ChannelGroup extends Model
{
    protected $fillable = [
        'group_name', 'parent_id', 'state', 'weight', 'remark'
    ];

    /**
     * 关联到通道
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function channels()
    {
        return $this->belongsToMany('App\Models\Channel')->withTimestamps();
    }
}
