<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SlowQueryLog extends Model
{
    // todo... 当点击is_fixed，则修复说有关于这个sql的数据
    protected $fillable = ['exec', 'expend_time', 'ori_sql', 'sql_content', 'is_fixed'];
}