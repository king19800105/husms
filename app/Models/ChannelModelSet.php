<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ChannelModelSet extends Model
{
    protected $fillable = [
        'model_name',
    ];
}
