<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = ['order_id', 'user_id', 'goods_id', 'goods_name', 'goods_number', 'order_type', 'state', 'pay_type', 'ori_price', 'discount_price', 'actual_price', 'remark', 'pay_time', 'source', 'decision_time', 'refund_time'];
}
