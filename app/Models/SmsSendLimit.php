<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SmsSendLimit extends Model
{
    protected $fillable = [
        'user_id', 'mobile_day_limit', 'mobile_minute_limit', 'day_limit', 'hour_limit', 'minute_limit', 'exceed_count'
    ];
}
