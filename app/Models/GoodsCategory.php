<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GoodsCategory extends Model
{
    protected $fillable = [
        'name', 'alias_name', 'parent_id', 'show_type', 'weight', 'state', 'img', 'seo_title', 'seo_keywords', 'seo_description'
    ];

    /**
     * 关联到商品表模型，多对一
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function goods()
    {
        return $this->hasMany(Goods::class);
    }
}
