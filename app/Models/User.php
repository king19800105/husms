<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Model;
use Laravel\Passport\HasApiTokens;

class User extends Model
{
    use HasApiTokens, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'parent_id',
        'name',
        'email',
        'mobile',
        'password',
        'qq',
        'head_portrait',
        'real_name',
        'type',
        'state',
        'source',
        'promotion_code',
        'earn',
        'total_number',
        'gift_number',
        'use_number',
        'residue_number',
        'last_login_ip',
        'last_sync_time',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];
}
