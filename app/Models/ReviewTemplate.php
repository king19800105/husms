<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ReviewTemplate extends Model
{
    protected $fillable = ['title', 'sign', 'content', 'state', 'channel_group_id', 'only_channel_id', 'rejected_reason', 'remark'];
}
