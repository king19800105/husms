<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Goods extends Model
{
    protected $fillable = [
        'name', 'alias_name', 'goods_category_id', 'is_hot', 'is_new', 'is_recommend', 'is_discount', 'goods_model_id',
        'goods_no', 'shop_price', 'inventory', 'intro', 'content_markdown', 'content_html', 'market_price', 'weight',
        'state', 'img', 'seo_title', 'seo_keywords', 'seo_description'
    ];

    /**
     * 关联到商品分类表
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function goodsCategory()
    {
        return $this->belongsTo('App\Models\GoodsCategory', 'goods_category_id');
    }
}