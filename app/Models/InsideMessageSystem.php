<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InsideMessageSystem extends Model
{
    protected $fillable = [
        'customer_id', 'message_id', 'state',
    ];

    /**
     * 关联InsideMessage模型
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function insideMessage() {
        return $this->belongsTo(InsideMessage::class, 'message_id');
    }


    public function insideMessageContents()
    {
        return $this->insideMessage()
            ->with('insideMessageContent')
            ->where('recipient_id', '0')
            ->where('state', '<>', '2');
    }
}
