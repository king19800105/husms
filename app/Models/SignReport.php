<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SignReport extends Model
{
    protected $fillable = ['sms_sign'];

    /**
     * 关联到通道
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function channels()
    {
        return $this->belongsToMany('App\Models\Channel')->withTimestamps();
    }
}
