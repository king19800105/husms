<?php

namespace App\Repository\Filters\Admin;

use Anthony\Structure\Filters\{
    IOrder
};

class CreatedAt implements IOrder
{
    public function order($entity, $direction)
    {
        return $entity->orderBy('created_at', $direction);
    }
}