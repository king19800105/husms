<?php

namespace App\Repository\Filters\Admin;

use Anthony\Structure\Filters\{
    AbstractFilter
};

class Mobile extends AbstractFilter
{
    protected function mappings()
    {
        return [
            'mobile' => 'mobile'
        ];
    }

    public function filter($entity, $value)
    {
        return $entity->where('mobile', 'like', $value . '%');
    }
}