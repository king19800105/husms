<?php

namespace App\Repository\Filters\Admin;

use Anthony\Structure\Filters\{
    AbstractFilter
};

class CreatedAtEnd extends AbstractFilter
{
    protected function mappings()
    {
        return [
            'created_at_end' => 'created_at'
        ];
    }

    public function filter($entity, $value)
    {
        return $entity->where($this->resolveFilterValue('created_at_end'), '<=', $value);
    }
}