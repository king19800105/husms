<?php
/**
 * Created by PhpStorm.
 * User: anthony
 * Date: 2018/10/23
 * Time: 11:42 AM
 */

namespace App\Repository\Filters\Admin;


use Anthony\Structure\Filters\AbstractFilter;

class LastLoggedIP extends AbstractFilter
{
    protected function mappings()
    {
        return [
            'last_logged_ip' => 'last_logged_ip'
        ];
    }

    public function filter($entity, $value)
    {
        return $entity->where('last_logged_ip', 'like', $value . '%');
    }
}