<?php

namespace App\Repository\Filters\Admin;

use Anthony\Structure\Filters\{
    AbstractFilter,
    IOrder
};

class Name extends AbstractFilter implements IOrder
{
    protected function mappings()
    {
        return [
            'name' => 'name'
        ];
    }

    public function filter($entity, $value)
    {
        return $entity->where('name', 'like', $value . '%');
    }

    public function order($entity, $direction)
    {
        return $entity->orderBy('name', $this->resolveOrderDirection($direction));
    }
}