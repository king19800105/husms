<?php

namespace App\Repository\Filters\Admin;

use Anthony\Structure\Filters\{
    AbstractFilter,
    IOrder
};

class State extends AbstractFilter implements IOrder
{
    protected function mappings()
    {
        return [
            'state' => 'state'
        ];
    }

    public function filter($entity, $value)
    {
        return $entity->where('state', $value);
    }

    public function order($entity, $direction)
    {
        return $entity->orderBy('state', $this->resolveOrderDirection($direction));
    }
}