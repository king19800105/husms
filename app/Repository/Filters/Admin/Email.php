<?php

namespace App\Repository\Filters\Admin;

use Anthony\Structure\Filters\{
    AbstractFilter
};

class Email extends AbstractFilter
{
    protected function mappings()
    {
        return [
            'email' => 'email'
        ];
    }

    public function filter($entity, $value)
    {
        return $entity->where('email', 'like', $value . '%');
    }
}