<?php

namespace App\Repository\Eloquent;

use App\Models\ReviewTemplate;
use App\Repository\Contracts\ReviewTemplateRepository;
use Anthony\Structure\Eloquent\AbstractRepository;

class ReviewTemplateRepositoryEloquent extends AbstractRepository implements ReviewTemplateRepository
{
    protected $filters = [
        // filter and sort settings
    ];

    public function entity()
    {
        return ReviewTemplate::class;
    }
}