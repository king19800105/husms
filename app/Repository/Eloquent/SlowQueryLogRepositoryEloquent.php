<?php

namespace App\Repository\Eloquent;

use App\Models\SlowQueryLog;
use App\Repository\Contracts\SlowQueryLogRepository;
use Anthony\Structure\Eloquent\AbstractRepository;

class SlowQueryLogRepositoryEloquent extends AbstractRepository implements SlowQueryLogRepository
{
    protected $filters = [
        // filter and sort settings
    ];

    public function entity()
    {
        return SlowQueryLog::class;
    }
}