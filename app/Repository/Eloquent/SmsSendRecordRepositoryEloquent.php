<?php

namespace App\Repository\Eloquent;

use App\Models\SmsSendRecord;
use App\Repository\Contracts\SmsSendRecordRepository;
use Anthony\Structure\Eloquent\AbstractRepository;

class SmsSendRecordRepositoryEloquent extends AbstractRepository implements SmsSendRecordRepository
{
    protected $filters = [
        // filter and sort settings
    ];

    public function entity()
    {
        return SmsSendRecord::class;
    }
}