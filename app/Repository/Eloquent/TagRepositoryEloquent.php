<?php

namespace App\Repository\Eloquent;

use App\Models\Tag;
use App\Repository\Contracts\TagRepository;
use Anthony\Structure\Eloquent\AbstractRepository;

class TagRepositoryEloquent extends AbstractRepository implements TagRepository
{
    protected $filters = [
        // filter and sort settings
    ];

    public function entity()
    {
        return Tag::class;
    }
}