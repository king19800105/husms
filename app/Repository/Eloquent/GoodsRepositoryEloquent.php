<?php

namespace App\Repository\Eloquent;

use App\Models\Goods;
use App\Repository\Contracts\GoodsRepository;
use Anthony\Structure\Eloquent\AbstractRepository;

class GoodsRepositoryEloquent extends AbstractRepository implements GoodsRepository
{
    protected $filters = [
        // filter and sort settings
    ];

    public function entity()
    {
        return Goods::class;
    }
}