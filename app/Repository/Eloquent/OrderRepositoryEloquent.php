<?php

namespace App\Repository\Eloquent;

use App\Models\Order;
use App\Repository\Contracts\OrderRepository;
use Anthony\Structure\Eloquent\AbstractRepository;

class OrderRepositoryEloquent extends AbstractRepository implements OrderRepository
{
    protected $filters = [
        // filter and sort settings
    ];

    public function entity()
    {
        return Order::class;
    }
}