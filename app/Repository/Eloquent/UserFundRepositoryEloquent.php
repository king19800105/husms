<?php

namespace App\Repository\Eloquent;

use App\Models\UserFund;
use App\Repository\Contracts\UserFundRepository;
use Anthony\Structure\Eloquent\AbstractRepository;

class UserFundRepositoryEloquent extends AbstractRepository implements UserFundRepository
{
    protected $filters = [
        // filter and sort settings
    ];

    public function entity()
    {
        return UserFund::class;
    }
}