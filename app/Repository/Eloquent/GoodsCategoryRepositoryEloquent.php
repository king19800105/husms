<?php

namespace App\Repository\Eloquent;

use App\Models\GoodsCategory;
use App\Repository\Contracts\GoodsCategoryRepository;
use Anthony\Structure\Eloquent\AbstractRepository;

class GoodsCategoryRepositoryEloquent extends AbstractRepository implements GoodsCategoryRepository
{
    protected $filters = [
        // filter and sort settings
    ];

    public function entity()
    {
        return GoodsCategory::class;
    }
}