<?php

namespace App\Repository\Eloquent;

use App\Models\Menu;
use App\Repository\Contracts\MenuRepository;
use Anthony\Structure\Eloquent\AbstractRepository;
use App\Repository\Criterias\ByWeight;

class MenuRepositoryEloquent extends AbstractRepository implements MenuRepository
{
    protected $filters = [
        // filter and sort settings
    ];

    public function entity()
    {
        return Menu::class;
    }

    /**
     * 获取所有菜单列表
     *
     * @return \Illuminate\Support\Collection
     */
    public function getAllMenuList()
    {
        return $this
            ->pushCriteria(new ByWeight())
            ->all();
    }
}