<?php

namespace App\Repository\Eloquent;

use App\Models\Role;
use App\Repository\Contracts\RoleRepository;
use Anthony\Structure\Eloquent\AbstractRepository;

class RoleRepositoryEloquent extends AbstractRepository implements RoleRepository
{
    protected $filters = [
        // filter and sort settings
    ];

    public function entity()
    {
        return Role::class;
    }

    /**
     * 给角色分配权限
     *
     * @param $id
     * @param array $permissionIds
     * @return mixed
     */
    public function assign($id, array $permissionIds)
    {
        return $this->find($id)->permissions()->sync($permissionIds);
    }
}