<?php

namespace App\Repository\Eloquent;

use App\Models\Channel;
use App\Repository\Contracts\ChannelRepository;
use Anthony\Structure\Eloquent\AbstractRepository;

class ChannelRepositoryEloquent extends AbstractRepository implements ChannelRepository
{
    protected $filters = [
        // filter and sort settings
    ];

    public function entity()
    {
        return Channel::class;
    }
}