<?php

namespace App\Repository\Eloquent;

use App\Models\ArticleCategory;
use App\Repository\Contracts\ArticleCategoryRepository;
use Anthony\Structure\Eloquent\AbstractRepository;

class ArticleCategoryRepositoryEloquent extends AbstractRepository implements ArticleCategoryRepository
{
    protected $filters = [
        // filter and sort settings
    ];

    public function entity()
    {
        return ArticleCategory::class;
    }
}