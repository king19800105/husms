<?php

namespace App\Repository\Eloquent;

use App\Models\Company;
use App\Repository\Contracts\CompanyRepository;
use Anthony\Structure\Eloquent\AbstractRepository;

class CompanyRepositoryEloquent extends AbstractRepository implements CompanyRepository
{
    protected $filters = [
        // filter and sort settings
    ];

    public function entity()
    {
        return Company::class;
    }
}