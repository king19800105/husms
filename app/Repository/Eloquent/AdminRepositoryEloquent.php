<?php

namespace App\Repository\Eloquent;

use Anthony\Structure\Criterias\ByCreateTime;
use App\Models\Admin;
use App\Repository\Contracts\AdminRepository;
use Anthony\Structure\Criterias\FilterRequest;
use Anthony\Structure\Eloquent\AbstractRepository;
use App\Repository\Filters\Admin\{
    CreatedAt, CreatedAtEnd, CreatedAtStart, LastLoggedIP, State, Email, Mobile, Name
};

class AdminRepositoryEloquent extends AbstractRepository implements AdminRepository
{
    protected $filters = [
        // filter and sort settings
        'name'             => Name::class,
        'mobile'           => Mobile::class,
        'email'            => Email::class,
        'state'            => State::class,
        'last_logged_ip'   => LastLoggedIP::class,
        'created_at_start' => CreatedAtStart::class,
        'created_at_end'   => CreatedAtEnd::class,
        'created_at'       => CreatedAt::class,
    ];

    public function entity()
    {
        return Admin::class;
    }

    public function boot()
    {
        $this->pushCriteria(new FilterRequest($this->filters));
    }

    public function findAdminsByPage()
    {
        return $this
            ->pushCriteria(new ByCreateTime())
            ->paginate();
    }

    public function assignToRoles($id, array $roleIds)
    {
        return $this->find($id)->roles()->sync($roleIds);
    }

    public function assignToPermission($id, $permissionId)
    {
        return $this->find($id)->permissions()->sync($permissionId);
    }
}