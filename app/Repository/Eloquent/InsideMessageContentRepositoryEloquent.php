<?php

namespace App\Repository\Eloquent;

use App\Models\InsideMessage;
use App\Models\InsideMessageContent;
use App\Repository\Contracts\InsideMessageContentRepository;
use Anthony\Structure\Eloquent\AbstractRepository;

class InsideMessageContentRepositoryEloquent extends AbstractRepository implements InsideMessageContentRepository
{
    protected $filters = [
        // filter and sort settings
    ];

    public function entity()
    {
        return InsideMessageContent::class;
    }

    /**
     * 创建记录
     *
     * @param array $message
     * @param array $content
     * @return InsideMessage
     */
    public function saveContentAssociateInfo(array $message, array $content): InsideMessage
    {
         return $this->create($message)
             ->insideMessage()
             ->create($content);
    }
}