<?php

namespace App\Repository\Eloquent;

use App\Models\ChannelGroup;
use App\Repository\Contracts\ChannelGroupRepository;
use Anthony\Structure\Eloquent\AbstractRepository;

class ChannelGroupRepositoryEloquent extends AbstractRepository implements ChannelGroupRepository
{
    protected $filters = [
        // filter and sort settings
    ];

    public function entity()
    {
        return ChannelGroup::class;
    }
}