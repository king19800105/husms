<?php

namespace App\Repository\Eloquent;

use App\Models\Permission;
use App\Repository\Contracts\PermissionRepository;
use Anthony\Structure\Eloquent\AbstractRepository;

class PermissionRepositoryEloquent extends AbstractRepository implements PermissionRepository
{
    protected $filters = [
        // filter and sort settings
    ];

    public function entity()
    {
        return Permission::class;
    }
}