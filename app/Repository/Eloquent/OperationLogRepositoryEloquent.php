<?php

namespace App\Repository\Eloquent;

use App\Models\OperationLog;
use App\Repository\Contracts\OperationLogRepository;
use Anthony\Structure\Eloquent\AbstractRepository;

class OperationLogRepositoryEloquent extends AbstractRepository implements OperationLogRepository
{
    protected $filters = [
        // filter and sort settings
    ];

    public function entity()
    {
        return OperationLog::class;
    }
}