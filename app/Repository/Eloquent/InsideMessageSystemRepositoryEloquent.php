<?php

namespace App\Repository\Eloquent;

use Anthony\Structure\Criterias\ByCreateTime;
use Anthony\Structure\Criterias\EagerLoad;
use App\Models\InsideMessageSystem;
use App\Repository\Contracts\InsideMessageSystemRepository;
use Anthony\Structure\Eloquent\AbstractRepository;

class InsideMessageSystemRepositoryEloquent extends AbstractRepository implements InsideMessageSystemRepository
{
    protected $filters = [
        // filter and sort settings
    ];

    public function entity()
    {
        return InsideMessageSystem::class;
    }

    public function findMessagesByUserId($uid)
    {
        return $this->withCriteria(
            new EagerLoad(['insideMessageContents']),
            new ByCreateTime()
        )
            ->where('customer_id', $uid)
            ->paginate();
    }
}