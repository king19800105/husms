<?php

namespace App\Repository\Eloquent;

use App\Models\ChannelModelSet;
use App\Repository\Contracts\ChannelModelSetRepository;
use Anthony\Structure\Eloquent\AbstractRepository;

class ChannelModelSetRepositoryEloquent extends AbstractRepository implements ChannelModelSetRepository
{
    protected $filters = [
        // filter and sort settings
    ];

    public function entity()
    {
        return ChannelModelSet::class;
    }
}