<?php

namespace App\Repository\Eloquent;

use Anthony\Structure\Criterias\ByCreateTime;
use Anthony\Structure\Criterias\EagerLoad;
use App\Models\InsideMessage;
use App\Repository\Contracts\InsideMessageRepository;
use Anthony\Structure\Eloquent\AbstractRepository;
use App\Repository\Criterias\NoneCancelMessage;

class InsideMessageRepositoryEloquent extends AbstractRepository implements InsideMessageRepository
{
    protected $filters = [
        // filter and sort settings
    ];

    public function entity()
    {
        return InsideMessage::class;
    }

    /**
     * 查询出站内信详细
     *
     * @param $id
     * @return mixed
     */
    public function findMessageDetailInfoByContentId($id)
    {
        return $this->withCriteria(
            new NoneCancelMessage()
        )
            ->find($id)
            ->insideMessageContent;
    }

    public function findMessagesByUserId($uid)
    {
        return $this->withCriteria(
            new EagerLoad(['insideMessageContent']),
            new NoneCancelMessage(),
            new ByCreateTime()
        )
            ->where('recipient_id', $uid)
            ->paginate();
    }
}