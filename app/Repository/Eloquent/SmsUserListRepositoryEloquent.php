<?php

namespace App\Repository\Eloquent;

use App\Models\SmsUserList;
use App\Repository\Contracts\SmsUserListRepository;
use Anthony\Structure\Eloquent\AbstractRepository;

class SmsUserListRepositoryEloquent extends AbstractRepository implements SmsUserListRepository
{
    protected $filters = [
        // filter and sort settings
    ];

    public function entity()
    {
        return SmsUserList::class;
    }
}