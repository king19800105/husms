<?php

namespace App\Repository\Eloquent;

use App\Models\SignReport;
use App\Repository\Contracts\SignReportRepository;
use Anthony\Structure\Eloquent\AbstractRepository;

class SignReportRepositoryEloquent extends AbstractRepository implements SignReportRepository
{
    protected $filters = [
        // filter and sort settings
    ];

    public function entity()
    {
        return SignReport::class;
    }
}