<?php

namespace App\Repository\Eloquent;

use App\Models\User;
use App\Repository\Contracts\UserRepository;
use Anthony\Structure\Eloquent\AbstractRepository;

class UserRepositoryEloquent extends AbstractRepository implements UserRepository
{
    protected $filters = [
        // filter and sort settings
    ];

    public function entity()
    {
        return User::class;
    }

    /**
     * 根据条件获取id数组
     *
     * @param array $condition
     * @return array
     */
    public function findSystemMessageUserIds(array $condition, $batchNum): array
    {
        $ids = [];
        $this->entity->when(!empty($condition), function($query) use ($condition) {
            return $query->where($condition);
        })->chunk($batchNum, function ($users) use (&$ids) {
            foreach ($users as $user) {
                $ids[] = $user['id'];
            }
        });

        return $ids;
    }
}