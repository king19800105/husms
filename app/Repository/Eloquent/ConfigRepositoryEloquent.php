<?php

namespace App\Repository\Eloquent;

use App\Models\Config;
use App\Repository\Contracts\ConfigRepository;
use Anthony\Structure\Eloquent\AbstractRepository;

class ConfigRepositoryEloquent extends AbstractRepository implements ConfigRepository
{
    protected $filters = [
        // filter and sort settings
    ];

    public function entity()
    {
        return Config::class;
    }
}