<?php

namespace App\Repository\Eloquent;

use App\Models\SmsSendLimit;
use App\Repository\Contracts\SmsSendLimitRepository;
use Anthony\Structure\Eloquent\AbstractRepository;

class SmsSendLimitRepositoryEloquent extends AbstractRepository implements SmsSendLimitRepository
{
    protected $filters = [
        // filter and sort settings
    ];

    public function entity()
    {
        return SmsSendLimit::class;
    }
}