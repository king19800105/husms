<?php

namespace App\Repository\Eloquent;

use App\Models\SmsReply;
use App\Repository\Contracts\SmsReplyRepository;
use Anthony\Structure\Eloquent\AbstractRepository;

class SmsReplyRepositoryEloquent extends AbstractRepository implements SmsReplyRepository
{
    protected $filters = [
        // filter and sort settings
    ];

    public function entity()
    {
        return SmsReply::class;
    }
}