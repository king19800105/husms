<?php

namespace App\Repository\Eloquent;

use App\Models\Article;
use App\Repository\Contracts\ArticleRepository;
use Anthony\Structure\Eloquent\AbstractRepository;

class ArticleRepositoryEloquent extends AbstractRepository implements ArticleRepository
{
    protected $filters = [
        // filter and sort settings
    ];

    public function entity()
    {
        return Article::class;
    }
}