<?php

namespace App\Repository\Contracts;

interface RoleRepository
{
    public function assign($id, array $permissionIds);
}