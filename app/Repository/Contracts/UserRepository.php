<?php

namespace App\Repository\Contracts;

interface UserRepository
{
    public function findSystemMessageUserIds(array $condition, $batchNum): array;
}