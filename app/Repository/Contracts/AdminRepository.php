<?php

namespace App\Repository\Contracts;

interface AdminRepository
{
    public function findAdminsByPage();

    public function assignToRoles($id, array $roleIds);

    public function assignToPermission($id, $permissionId);
}