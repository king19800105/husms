<?php

namespace App\Repository\Contracts;

interface InsideMessageSystemRepository
{
    public function findMessagesByUserId($uid);
}