<?php

namespace App\Repository\Contracts;

interface InsideMessageRepository
{
    public function findMessageDetailInfoByContentId($id);

    public function findMessagesByUserId($uid);
}