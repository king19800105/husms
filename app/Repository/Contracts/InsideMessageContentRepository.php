<?php

namespace App\Repository\Contracts;

interface InsideMessageContentRepository
{
    public function saveContentAssociateInfo(array $message, array $content);
}