<?php
/**
 * Created by PhpStorm.
 * User: anthony
 * Date: 2018/7/29
 * Time: 下午10:31
 */

namespace App\Repository\Criterias;


use Anthony\Structure\Criterias\ICriteria;

class ByWeight implements ICriteria
{
    public function apply($entity)
    {
        return $entity->orderBy('weight', 'desc');
    }
}