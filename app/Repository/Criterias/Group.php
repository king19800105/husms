<?php
/**
 * Created by PhpStorm.
 * User: anthony
 * Date: 2018/7/29
 * Time: 下午10:31
 */

namespace App\Repository\Criterias;


use Anthony\Structure\Criterias\ICriteria;

class Group implements ICriteria
{
    protected $groupBy;

    public function __construct($groupBy)
    {
        $this->groupBy = $groupBy;
    }

    public function apply($entity)
    {
        return $entity->groupBy($this->groupBy);
    }
}