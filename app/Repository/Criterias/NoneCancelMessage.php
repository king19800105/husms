<?php

namespace App\Repository\Criterias;

use Anthony\Structure\Criterias\ICriteria;

class NoneCancelMessage implements ICriteria
{
    public function apply($entity)
    {
        return $entity->where('state', '<>', '2');
    }
}