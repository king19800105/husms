<?php

namespace App\Repository\Criterias;

use Anthony\Structure\Criterias\ICriteria;

class StateSet implements ICriteria
{
    public function apply($entity)
    {
        return $entity->where('state', '1');
    }
}