<?php

namespace App\Listeners\Auth;

use Laravel\Passport\Events\RefreshTokenCreated;

class PruneOldTokens
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(RefreshTokenCreated $event)
    {
        \DB::table('oauth_refresh_tokens')
            ->where('access_token_id', '<>', $event->accessTokenId)
            ->where('revoked', true)->delete();
    }
}
