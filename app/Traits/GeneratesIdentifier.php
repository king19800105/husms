<?php
/**
 * Created by PhpStorm.
 * User: king
 * Date: 2018/4/29
 * Time: 下午5:27
 */

namespace App\Traits;


trait GeneratesIdentifier
{
    public function generateIdentifier($id)
    {
        return hash('sha256', $id . config('app.key'));
    }
}