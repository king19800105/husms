<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class ValidUnique implements Rule
{
    protected $condition;
    protected $message;

    /**
     * ValidUnique constructor.
     * @param $condition
     * @param string $message
     */
    public function __construct($condition, $message = '')
    {
        $this->condition = $condition;
        $this->message = $message;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {

    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->message;
    }
}
