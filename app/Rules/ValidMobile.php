<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class ValidMobile implements Rule
{
    protected const REGEX = '/^(\+?86-?)?(18|15|13)[0-9]{9}$|^(010|02\d{1}|0[3-9]\d{2})-\d{7,9}(-\d+)?$|^\d+-\d+-\d+$/';
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return 1 === preg_match(static::REGEX, $value);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('validation.custom.valid_mobile');
    }
}
