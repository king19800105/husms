<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class ValidJsonArray implements Rule
{
    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if (is_string($value)) {
            $value = json_decode($value, true);
        }

        return is_array($value);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('validation.custom.valid_json_array');
    }
}
