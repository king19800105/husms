<?php

namespace App\Providers;

use App\Models\Admin;
use App\Models\Article;
use App\Models\Permission;
use App\Models\ReviewTemplate;
use App\Models\User;
use App\Observers\AdminObserver;
use App\Observers\ArticleObserver;
use App\Observers\PermissionObserver;
use App\Observers\ReviewTemplateObserver;
use App\Observers\UserObserver;
use Illuminate\Support\ServiceProvider;

class EloquentEventServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        Admin::observe(AdminObserver::class);
        Permission::observe(PermissionObserver::class);
        Article::observe(ArticleObserver::class);
        ReviewTemplate::observe(ReviewTemplateObserver::class);
        User::observe(UserObserver::class);
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
