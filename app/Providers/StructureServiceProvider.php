<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class StructureServiceProvider extends ServiceProvider
{
   /**
    * Bootstrap the application services.
    *
    * @return void
    */
    public function boot()
    {

    }

   /**
    * Register the application services.
    *
    * @return void
    */
    public function register()
    {
        $this->app->bind(\App\Repository\Contracts\AdminRepository::class, \App\Repository\Eloquent\AdminRepositoryEloquent::class);
        $this->app->bind(\App\Repository\Contracts\PermissionRepository::class, \App\Repository\Eloquent\PermissionRepositoryEloquent::class);
        $this->app->bind(\App\Repository\Contracts\RoleRepository::class, \App\Repository\Eloquent\RoleRepositoryEloquent::class);
        $this->app->bind(\App\Repository\Contracts\MenuRepository::class, \App\Repository\Eloquent\MenuRepositoryEloquent::class);
        $this->app->bind(\App\Repository\Contracts\ConfigRepository::class, \App\Repository\Eloquent\ConfigRepositoryEloquent::class);
        $this->app->bind(\App\Repository\Contracts\ArticleCategoryRepository::class, \App\Repository\Eloquent\ArticleCategoryRepositoryEloquent::class);
        $this->app->bind(\App\Repository\Contracts\ArticleRepository::class, \App\Repository\Eloquent\ArticleRepositoryEloquent::class);
        $this->app->bind(\App\Repository\Contracts\TagRepository::class, \App\Repository\Eloquent\TagRepositoryEloquent::class);
        $this->app->bind(\App\Repository\Contracts\GoodsCategoryRepository::class, \App\Repository\Eloquent\GoodsCategoryRepositoryEloquent::class);
        $this->app->bind(\App\Repository\Contracts\GoodsRepository::class, \App\Repository\Eloquent\GoodsRepositoryEloquent::class);
        $this->app->bind(\App\Repository\Contracts\SlowQueryLogRepository::class, \App\Repository\Eloquent\SlowQueryLogRepositoryEloquent::class);
        $this->app->bind(\App\Repository\Contracts\OperationLogRepository::class, \App\Repository\Eloquent\OperationLogRepositoryEloquent::class);
        $this->app->bind(\App\Repository\Contracts\InsideMessageRepository::class, \App\Repository\Eloquent\InsideMessageRepositoryEloquent::class);
        $this->app->bind(\App\Repository\Contracts\InsideMessageContentRepository::class, \App\Repository\Eloquent\InsideMessageContentRepositoryEloquent::class);
        $this->app->bind(\App\Repository\Contracts\InsideMessageSystemRepository::class, \App\Repository\Eloquent\InsideMessageSystemRepositoryEloquent::class);
        $this->app->bind(\App\Repository\Contracts\UserRepository::class, \App\Repository\Eloquent\UserRepositoryEloquent::class);
        $this->app->bind(\App\Repository\Contracts\ReviewTemplateRepository::class, \App\Repository\Eloquent\ReviewTemplateRepositoryEloquent::class);
        $this->app->bind(\App\Repository\Contracts\ChannelGroupRepository::class, \App\Repository\Eloquent\ChannelGroupRepositoryEloquent::class);
        $this->app->bind(\App\Repository\Contracts\ChannelModelSetRepository::class, \App\Repository\Eloquent\ChannelModelSetRepositoryEloquent::class);
        $this->app->bind(\App\Repository\Contracts\ChannelRepository::class, \App\Repository\Eloquent\ChannelRepositoryEloquent::class);
        $this->app->bind(\App\Repository\Contracts\SignReportRepository::class, \App\Repository\Eloquent\SignReportRepositoryEloquent::class);
        $this->app->bind(\App\Repository\Contracts\SmsSendRecordRepository::class, \App\Repository\Eloquent\SmsSendRecordRepositoryEloquent::class);
        $this->app->bind(\App\Repository\Contracts\SmsReplyRepository::class, \App\Repository\Eloquent\SmsReplyRepositoryEloquent::class);
        $this->app->bind(\App\Repository\Contracts\UserRepository::class, \App\Repository\Eloquent\UserRepositoryEloquent::class);
        $this->app->bind(\App\Repository\Contracts\CompanyRepository::class, \App\Repository\Eloquent\CompanyRepositoryEloquent::class);
        $this->app->bind(\App\Repository\Contracts\UserFundRepository::class, \App\Repository\Eloquent\UserFundRepositoryEloquent::class);
        $this->app->bind(\App\Repository\Contracts\OrderRepository::class, \App\Repository\Eloquent\OrderRepositoryEloquent::class);
        $this->app->bind(\App\Repository\Contracts\SmsSendLimitRepository::class, \App\Repository\Eloquent\SmsSendLimitRepositoryEloquent::class);
        $this->app->bind(\App\Repository\Contracts\SmsUserListRepository::class, \App\Repository\Eloquent\SmsUserListRepositoryEloquent::class);
        //end-binding
    }
}