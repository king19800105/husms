<?php

namespace App\Providers;

use Carbon\Carbon;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Laravel\Passport\Passport;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
        $this->initPassportSetting();
    }

    protected function initPassportSetting()
    {
        $passportConfig = config(
            'common.passport',
            ['expire' => 1, 'refresh' => 2, 'scope' => []]
        );

        Passport::routes();

        Passport::tokensExpireIn(Carbon::now()->addDays($passportConfig['expire']));

        Passport::refreshTokensExpireIn(Carbon::now()->addDays($passportConfig['refresh']));

        Passport::tokensCan($passportConfig['scope']);
    }
}
