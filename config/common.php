<?php
/**
 * Created by PhpStorm.
 * User: king
 * Date: 2018/2/20
 * Time: 下午3:57
 */

return [
    'passport' => [
        'expire'  => 30,
        'refresh' => 60,
        'scope'   => [
            'client-backend'      => 'Client Backend',
            'client-wechat-small' => 'WeChat Small',
        ]
    ],
    'fileupload' => [
        'img_mimes' => 'gif,jpeg,jpe,jpg,bmp,png',
        'maxsize'   => '1024',
    ],
];