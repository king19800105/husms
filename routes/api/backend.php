<?php
/**
 * Created by PhpStorm.
 * User: kb130
 * Date: 2018/4/12
 * Time: 14:33
 */

/**
 * 后台业务操作
 */
// todo... 添加 rbac 中间件
Route::group(['prefix' => 'backend', 'namespace'=>'Backend', 'middleware'=>['auth.api:api_admin', 'scope:client-backend', 'log.slow_query', 'log.operation', 'rbac']], function ($router) {
    // 用户管理
    $router->resource('user', 'UserController');

    // 企业资质管理
    $router->post('company/qualification', 'Qualification@CompanyController')->name('company.qualification');
    $router->resource('company', 'CompanyController');

    // 用户资金和短信消费流
    $router->resource('user-fund', 'UserFundController');

    // 用户订单
    $router->resource('order', 'OrderController');

    // 短信发送限制
    $router->resource('sms-send-limit', 'SmsSendLimitController');

    // 用户黑白名单
    $router->resource('sms-user-list', 'SmsUserListController');

    // 控制台
    $router->resource('dash', 'DashController');

    // 管理员模块
    $router->get('admin/roles/{id}', 'AdminController@getAdminRolesById')->name('admin.getAdminRolesById');
    $router->get('admin/info', 'AdminController@currentAdminInfo')->name('admin.currentAdminInfo');
    $router->post('admin/reset-password/{id}', 'AdminController@resetPassword')->name('admin.resetPassword');
    $router->post('admin/assign', 'AdminController@assign')->name('admin.assign');
    $router->resource('admin', 'AdminController');

    // 角色管理模块
    $router->get('role/permissions/{id}', 'RoleController@getPermissionsByRoleId')->name('role.getPermissionsByRoleId');
    $router->post('role/assign', 'RoleController@assign')->name('role.assign');
    $router->resource('role', 'RoleController');

    // 权限管理模块
    $router->get('permission/show-group', 'PermissionController@showOnGroup')->name('permission.showOnGroup');
    $router->resource('permission', 'PermissionController');

    // 菜单管理模块
    $router->resource('menu', 'MenuController');

    // 配置信息模型
    $router->resource('config', 'ConfigController');

    // 文章分类管理
    $router->resource('article-category', 'ArticleCategoryController');

    // 文章管理
    $router->resource('article', 'ArticleController');

    // 标签管理
    $router->resource('tag', 'TagController');

    // 商品分类管理
    $router->resource('goods-category', 'GoodsCategoryController');

    // 商品管理
    $router->resource('goods', 'GoodsController');

    // 模板审核
    $router->put('review-tpl/verify/{id}', 'ReviewTemplateController@verifyTemplate');
    $router->resource('review-tpl', 'ReviewTemplateController');

    // 通道管理
    $router->post('channel/assign', 'ChannelController@assign')->name('channel.assign');
    $router->resource('channel', 'ChannelController');

    // 通道模型管理
    $router->resource('channel-model-set', 'ChannelModelSetController');

    // 通道组管理
    $router->resource('channel-group', 'ChannelGroupController');

    // 签名管理
    $router->resource('sign-report', 'SignReportController');

    // 发送记录管理
    $router->resource('sms-send-record', 'SmsSendRecordController');

    // 上行接收记录管理
    $router->resource('sms-reply', 'SmsReplyController');

    // 信息推送操作
    $router->post('message/inside-point', 'MessageController@sendInsidePoint'); // 内部点对点发送
    $router->put('message/inside-point-cancel/{id}', 'MessageController@cancelInsideMessage')->where('id', '[0-9]+');   // 取消点对点的发送状态
    $router->post('message/inside-system', 'MessageController@sendInsideSystem');   // 系统推送群发
    $router->get('message/inside-read/{id}', 'MessageController@readInsideMessage')->where('id', '[0-9]+');   // 站内信标记已读，并且返回数据。
    $router->get('message/inside-list/{type}', 'MessageController@showInsideMessageList')->where('type', '[single|system]+');   // 根据类型显示消息列表

    // 日志管理
    $router->get('log/slow-queries', 'LogController@getSlowQueryList')->name('log.getSlowQueryList');     // 慢查询列表
    $router->get('log/show-query/{id}', 'LogController@showSlowQuery')->name('log.showSlowQuery');        // 慢查询详情
    $router->get('log/operations', 'LogController@operationList')->name('log.operationList');             // 用户操作列表
    $router->get('log/show-operation/{id}', 'LogController@showOperation')->name('log.showOperation');    // 用户操作详情
    $router->post('log/fix-slow-query/{id}', 'LogController@fixedSlowQuery')->name('log.fixedSlowQuery'); // 修复慢查询后的变更
});