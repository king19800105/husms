<?php
/**
 * Created by PhpStorm.
 * User: kb130
 * Date: 2018/4/12
 * Time: 14:33
 */

/**
 * backend模块
 */
 Route::group(['prefix' => 'backend', 'middleware'=>[]], function ($router) {
     // 授权登入和登出操作
    $router->group(['namespace'=>'Auth'], function ($router) {
        $router->post('login', 'AdminLoginController@loginAuthorization')->name('admin.login');
        $router->get('logout', 'AdminLoginController@cancelAuthorization')->name('admin.logout');
    });
 });

