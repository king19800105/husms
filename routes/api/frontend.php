<?php
/**
 * Created by PhpStorm.
 * User: kb130
 * Date: 2018/4/12
 * Time: 14:33
 */

/**
 * 后台业务操作
 */
// todo... 添加 rbac 中间件
Route::group(['prefix' => 'frontend', 'namespace'=>'Frontend'], function ($router) {
    // 用户管理
    $router->resource('my-test', 'MyTestController');
});