# 互信云技术服务平台

### 快速开发流程

1. php artisan anthony:entity GoodsCategory --resource
2. 数据表设置字段
3. php artisan migrate
4. Model中配置允许字段 protected $fillable = [...]
5. route路由配置
6. 自动填充控制器 php artisan anthony:fill-c GoodsCategory
7. request的create和update验证规则编写和字段显示设置
8. 编写本地化文件，把字段对应中文显示
9. 编写response信息
10.调试接口
